<?php

	$page_title = 'Belano.rs - Login page';
	include('head.php');

	include('header.php');

?>
    
    <div class="main-container" id="page-booking">

    	<div class="container-fluid">
    		<div class="row">

    			<div class="col-6" id="container-left">

                    <ul id="left-submenu">
                        <li><a href="#">Regularne korisnike</a></li>
                        <li><a href="#">Partnere</a></li>
                        <li><a href="#">Vlasnike</a></li>
                    </ul>

                    <div id="left-content">

                        <h4 class="title-medium">Login</h4>
                        <p class="text-medium">Welcome back.</br>
                        Please login in to your account</p>

                        <button type="button" class="btn btn-login" id="btn-login-google" >With Google</button>
                        <button type="button" class="btn btn-login" id="btn-login-google" >With Facebook</button>

                        <form action="#" id="form-login" class="my-5">
                            
                            <div class="input-big">
                                <label for="login-email">Name or Email</label>
                                <input type="text" name="login-email" class="form-control" id="login-email">
                            </div>
                            <div class="input-big">
                                <label for="login-email">Password</label>
                                <input type="text" name="login-password" class="form-control" id="login-password">
                            </div>

                            <div class="form-row flex-row justify-content-between my-2">
                                <label>
                                    <input type="checkbox" name="login-remember" id="login-remember" value="remember-me"> Remember me
                                </label>
                                <a href="#" class="btn-link">Forgot password?</a>
                            </div>

                            <div class="form-row my-4">
                                <button type="button" class="btn btn-primary outline">Login</button>
                                <button type="button" class="btn btn-info outline disabled">Sign-up</button>
                            </div>
                            
                            <label>
                                <input type="checkbox" name="login-agreement" id="login-agreement" class=""> Do you agree withe Privte Police / Terms
                            </label>

                        </form>

                    </div>

                    
    			</div>

    			<div class="col-6" id="container-right">
    				
    				<div class="main-image">
    					<div class="main-image-price">
    						<p>25 €</p>
    						<span class="apartman-status">Available</span>
    					</div>
    					<img src="img/apartman_main_image.jpg" alt="Naziv apartmana">
    				</div>

    				<ul class="other-images p-0 m-0">
    					<li>
    						<a href="#" class="thumb-item">
    						<img src="http://placehold.it/150x150" alt="">
    						</a>
    					</li>
    					<li>
    						<a href="#" class="thumb-item">
    						<img src="http://placehold.it/150x150" alt="">
    						</a>
    					</li>
    					<li>
    						<a href="#" class="thumb-item">
    						<img src="http://placehold.it/150x150" alt="">
    						</a>
    					</li>
    					<li>
    						<a href="#" class="thumb-item">
    						<img src="http://placehold.it/150x150" alt="">
    						</a>
    					</li>
    					<li>
    						<a href="#" class="thumb-item">
    						<img src="http://placehold.it/150x150" alt="">
    						</a>
    					</li>
    					<li>
    						<a href="#" class="thumb-item">
    						<img src="http://placehold.it/150x150" alt="">
    						</a>
    					</li>
    				</ul>

    				<div class="split-apart-info">
    					<h3>Frida apartman</h3>
    				</div>

    			</div>

    		</div>
    	</div>

    </div>

<?php include('footer.php'); ?>