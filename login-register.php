<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Belano.rs - Login/Register Account</title>

    <link rel="stylesheet" href="fonts/productsans.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.css">

    <!-- Material Design for Bootstrap -->
    <link rel="stylesheet" href="css/mdb.min.css">

    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/jquery.min.js"></script>

    <!-- Google Sing-in -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id" content="514439232405-u920cnd9j2e471k92iqjqq2p6ja0rnkd.apps.googleusercontent.com">

    <!-- FontAwesome Icons -->
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700&amp;subset=latin-ext" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- Leave those next 4 lines if you care about users using IE8 -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-login-register">

<?php include('header.php'); ?>

<div id="preloader">
    <div class="preloader-inner">
        <img src="img/logo-header-light.png" alt="Belano.rs">
    </div>
</div>

<div class="main-container">

    <div class="container-fluid">

        <div class="row">

            <!-- Login / Register form -->
            <div class="col-6" id="container-left">

                <div id="inner-left" class="with-top-row">

                    <div class="left-top-fixed">

                        <ul class="nav tabs justify-content-between" id="tabs-login-register" role="tablist">
                            <li class="nav-item">
                                <a href="#access-users" data-toggle="tab" class="nav-link active">Regularne korisnike</a>
                            </li>
                            <li class="nav-item">
                                <a href="#access-partners" data-toggle="tab" class="nav-link">Partnere</a>
                            </li>
                            <li class="nav-item">
                                <a href="login-owners" class="nav-link">Vlasnike</a>
                            </li>
                        </ul>

                    </div>

                    <div class="inner-left-content d-flex h-100 bg-white">

                        <div class="tab-content">

                            <!-- Regular users -->
                            <div class="tab-pane fade active" id="access-users" role="tabpanel">

                                <div id="login-regular-wrapper" class="access-wrapper w-100">

                                    <h4 class="page-title mb-3">Login</h4>
                                    <p class="medium-text text-lightblue mb-0">Welcome back.</p>
                                    <p class="medium-text text-lightblue">Please login in to your account</p>

                                    <div class="my-5">
                                        <div class="g-signin2 btn btn-white with-shadow m-0 rounded" data-onsuccess="onSignIn"></div>
                                        <a href="#" class="btn btn-white rounded with-shadow ml-3" id="btn-login-reg-facebook">With Facebook</a>
                                    </div>

                                    <!-- Form: Login -->
                                    <form action="" id="form-login-regular" class="w-100">

                                        <div class="form-label-group mb-0">
                                            <input type="text" name="login-reg-username" id="login-reg-username" class="form-control with-shadow" required="required" placeholder="Name or E-mail">
                                            <label for="login-reg-username" class="active">Name or E-mail</label>
                                        </div>

                                        <div class="form-label-group mb-0">
                                            <input type="password" name="login-reg-password" id="login-reg-password" class="form-control with-shadow validate" required="required" placeholder="Password">
                                            <label for="login-reg-password" class="active">Password</label>
                                            <span toggle="#login-reg-password" class="fas fa-eye-slash toggle-password"></span>
                                        </div>

                                        <div class="d-flex justify-content-between mt-3">
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="login-regular-remember" name="login-regular-remember">
                                                <label class="form-check-label" for="login-regular-remember">Remember me</label>
                                            </div>
                                            <a class="btn-link" href="/lost-password">Forget password</a>
                                        </div>

                                        <div class="mt-4">
                                            <button type="submit" class="btn btn-outline-primary rounded no-shadow m-0" id="btn-login-regular" disabled>Login</button>
                                            <a href="#" class="btn btn-outline-light rounded no-shadow btn-toggle-div" data-open-div="#register-reg-wrapper" data-close-div="#login-regular-wrapper" id="btn-open-register-regular">Sign-up</a>
                                        </div>

                                        <div class="mt-2 p-0 form-check">
                                            <input type="checkbox" class="form-check-input" id="login-regular-privacy" name="login-regular-privacy">
                                            <label class="form-check-label" for="login-regular-privacy">Do you agree withe Privte Police / Terms</label>
                                        </div>

                                    </form>

                                </div>

                                <div id="register-reg-wrapper" class="access-wrapper hidden">

                                    <h4 class="page-title mb-3">Register</h4>
                                    <p class="medium-text text-lightblue mb-0">Welcome back.</p>
                                    <p class="medium-text text-lightblue">Please register in to your account</p>

                                    <div class="my-5">
                                        <div class="g-signin2 btn btn-white with-shadow m-0 rounded" data-onsuccess="onSignIn"></div>
                                        <a href="#" class="btn btn-white rounded with-shadow ml-3" id="btn-login-reg-facebook">With Facebook</a>
                                    </div>

                                    <!-- Form: Register -->
                                    <form action="" id="form-register-regular" class="w-100">

                                        <div class="form-label-group mb-0">
                                            <input type="text" name="register-reg-username" id="register-reg-username" class="form-control with-shadow" required="required" placeholder="Fullname">
                                            <label for="register-reg-username" class="active">Fullname *</label>
                                        </div>

                                        <div class="form-label-group mb-0">
                                            <input type="text" name="register-reg-email" id="register-reg-email" class="form-control with-shadow" required="required" placeholder="E-mail address">
                                            <label for="register-reg-email" class="active">E-mail address *</label>
                                        </div>

                                        <div class="form-label-group mb-0">
                                            <input type="password" name="login-reg-password" id="login-reg-password" class="form-control with-shadow validate" required="required" placeholder="Password">
                                            <label for="login-reg-password" class="active">Password *</label>
                                            <span toggle="#login-reg-password" class="fas fa-eye-slash toggle-password"></span>
                                        </div>

                                        <div class="form-label-group mb-0">
                                            <input type="password" name="login-reg-password2" id="login-reg-password2" class="form-control with-shadow validate" required="required" placeholder="Repeat password">
                                            <label for="login-reg-password" class="active">Repeat password *</label>
                                            <span toggle="#login-reg-password" class="fas fa-eye-slash toggle-password"></span>
                                        </div>

                                        <div class="d-flex justify-content-start mt-3">
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="register-reg-remember" name="register-reg-remember">
                                                <label class="form-check-label" for="register-reg-remember">Remember me</label>
                                            </div>
                                        </div>

                                        <div class="mt-4">
                                            <button type="submit" class="btn btn-outline-primary rounded no-shadow m-0" id="btn-register-regular" disabled>Register</button>
                                            <a href="#" class="btn btn-outline-light rounded no-shadow btn-toggle-div" data-open-div="#login-regular-wrapper" data-close-div="#register-reg-wrapper">Login</a>
                                        </div>

                                        <div class="mt-2 p-0 form-check">
                                            <input type="checkbox" class="form-check-input" id="register-reg-privacy"
                                                   name="register-reg-privacy">
                                            <label class="form-check-label" for="register-reg-privacy">Do you agree withe Privte Police / Terms</label>
                                        </div>

                                    </form>

                                </div>

                            </div>

                            <div class="tab-pane fade" id="access-partners" role="tabpanel">

                                <!-- Partners -->
                                <div id="login-partners-wrapper" class="access-wrapper w-100">

                                    <h4 class="page-title mb-3">Login</h4>
                                    <p class="medium-text text-lightblue mb-0">Welcome back. Milos.</p>
                                    <p class="medium-text text-lightblue">Please login in to your account</p>

                                    <div class="my-5">
                                        <div class="g-signin2 btn btn-white with-shadow m-0 rounded" data-onsuccess="onSignIn"></div>
                                        <a href="#" class="btn btn-white rounded with-shadow ml-3" id="btn-login-reg-facebook">With Facebook</a>
                                    </div>

                                    <!-- Form: Login -->
                                    <form action="" id="form-login-regular" class="w-100">

                                        <div class="form-label-group mb-0">
                                            <input type="text" name="login-par-username" id="login-par-username" class="form-control with-shadow" required="required" placeholder="Name or E-mail">
                                            <label for="login-par-username" class="active">Name or E-mail</label>
                                        </div>

                                        <div class="form-label-group mb-0">
                                            <input type="password" name="login-par-password" id="login-par-password" class="form-control with-shadow validate" required="required" placeholder="Password">
                                            <label for="login-par-password" class="active">Password</label>
                                            <span toggle="#login-par-password" class="fas fa-eye-slash toggle-password"></span>
                                        </div>

                                        <div class="d-flex justify-content-between mt-3">
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="login-regular-remember" name="login-regular-remember">
                                                <label class="form-check-label" for="login-regular-remember">Remember me</label>
                                            </div>
                                            <a class="btn-link" href="/lost-password">Forget password</a>
                                        </div>

                                        <div class="mt-4">
                                            <button type="submit" class="btn btn-outline-primary rounded no-shadow m-0" id="btn-login-regular" disabled>Login</button>
                                            <a href="#" class="btn btn-outline-light rounded no-shadow btn-toggle-div" data-open-div="#register-partners-wrapper" data-close-div="#login-partners-wrapper" id="btn-open-register-par">Sign-up</a>
                                        </div>

                                        <div class="mt-2 p-0 form-check">
                                            <input type="checkbox" class="form-check-input" id="login-regular-privacy" name="login-regular-privacy">
                                            <label class="form-check-label" for="login-regular-privacy">Do you agree withe Privte Police / Terms</label>
                                        </div>

                                    </form>

                                </div>

                                <div id="register-partners-wrapper" class="access-wrapper hidden">

                                    <h4 class="page-title mb-3">Register</h4>
                                    <p class="medium-text text-lightblue mb-0">Be our partner.</p>
                                    <p class="medium-text text-lightblue">Please register your account</p>

                                    <div class="my-5">
                                        <div class="g-signin2 btn btn-white with-shadow m-0 rounded" data-onsuccess="onSignIn"></div>
                                        <a href="#" class="btn btn-white rounded with-shadow ml-3" id="btn-login-par-facebook">With Facebook</a>
                                    </div>

                                    <!-- Form: Register (partners) -->
                                    <form action="" id="form-register-par" class="w-100">

                                        <div class="form-label-group mb-0">
                                            <input type="text" name="register-par-username" id="register-par-username" class="form-control with-shadow" required="required" placeholder="Fullname">
                                            <label for="register-par-username" class="active">Fullname *</label>
                                        </div>

                                        <div class="form-label-group mb-0">
                                            <input type="text" name="register-par-email" id="register-par-email" class="form-control with-shadow" required="required" placeholder="E-mail address">
                                            <label for="register-par-email" class="active">E-mail address *</label>
                                        </div>

                                        <div class="form-label-group mb-0">
                                            <input type="password" name="login-par-password" id="login-par-password" class="form-control with-shadow validate" required="required" placeholder="Password">
                                            <label for="login-par-password" class="active">Password *</label>
                                            <span toggle="#login-par-password" class="fas fa-eye-slash toggle-password"></span>
                                        </div>

                                        <div class="form-label-group mb-0">
                                            <input type="password" name="login-par-password2" id="login-par-password2" class="form-control with-shadow validate" required="required" placeholder="Repeat password">
                                            <label for="login-par-password" class="active">Repeat password *</label>
                                            <span toggle="#login-par-password" class="fas fa-eye-slash toggle-password"></span>
                                        </div>

                                        <div class="d-flex justify-content-start mt-3">
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="register-par-remember" name="register-par-remember">
                                                <label class="form-check-label" for="register-par-remember">Remember me</label>
                                            </div>
                                        </div>

                                        <div class="mt-4">
                                            <button type="submit" class="btn btn-outline-primary rounded no-shadow m-0" id="btn-register-par" disabled>Register</button>
                                            <a href="#" class="btn btn-outline-light rounded no-shadow btn-toggle-div" data-open-div="#login-partners-wrapper" data-close-div="#register-partners-wrapper">Login</a>
                                        </div>

                                        <div class="mt-2 p-0 form-check">
                                            <input type="checkbox" class="form-check-input" id="register-par-privacy"
                                                   name="register-par-privacy">
                                            <label class="form-check-label" for="register-par-privacy">Do you agree withe Privte Police / Terms</label>
                                        </div>

                                    </form>

                                </div>
                                
                            </div>
                            <div class="tab-pane fade" id="access-owners" role="tabpanel">Owners</div>

                        </div>

                    </div>

                </div>

            </div>

            <!-- Single Apart. Promo -->
            <div class="col-6 d-none d-sm-none d-md-block d-lg-block" id="container-right">
                <?php include('templates/single-apartment-inner.php'); ?>
            </div>

        </div>

    </div>

</div>

<script>

    $(document).ready(function(){

        // Google Sign-in (Regular)
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
        }

        preloader();

        // Login (regular) form
        $('#form-login-regular').submit(function(e) {
            e.preventDefault();

            // TODO validate
            // Submit form

            var loginData = $(this).serialize();
            alert(loginData);
        });

        // Login (regular) privacy check
        $('input[name="login-regular-privacy"]').change(function () {
            if( $(this).prop("checked") == true ) {
                $('#btn-login-regular').removeAttr('disabled');
            }
            else {
                $('#btn-login-regular').attr('disabled', true);
            }
        });

        $('.btn-toggle-div').click(function(e){
            e.preventDefault();

            var openDiv = $(this).data('open-div');
            var closeDiv = $(this).data('close-div');

            $(openDiv).removeClass('hidden');
            $(closeDiv).addClass('hidden');


        })

        // Register (regular) form
        $('#form-register-regular').submit(function(e) {
            e.preventDefault();

            // TODO validate
            // Submit form

            var registerData = $(this).serialize();
            alert(registerData);
        });

        // Register (regular) privacy check
        $('input[name="register-reg-privacy"]').change(function () {
            if( $(this).prop("checked") == true ) {
                $('#btn-register-regular').removeAttr('disabled');
            }
            else {
                $('#btn-register-regular').attr('disabled', true);
            }
        });

        // Register (partners) privacy check
        $('input[name="register-par-privacy"]').change(function () {
            if( $(this).prop("checked") == true ) {
                $('#btn-register-par').removeAttr('disabled');
            }
            else {
                $('#btn-register-par').attr('disabled', true);
            }
        });

        // Login with Google (regular)
        $('#btn-login-reg-google').click(function(e) {
            e.preventDefault();
            alert('init google login');
        });

        // Login with Facebook (regular)
        $('#btn-login-reg-facebook').click(function(e) {
            e.preventDefault();
            alert('init facebook login');
        });

    })

</script>

<?php include('bottom-includes.php'); ?>