<!DOCTYPE html>
    <html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- TODO: Ogg for social networks -->
        <!-- TODO: FavIcons -->

        <title>Belano.rs - Homepage</title>

        <link rel="stylesheet" href="fonts/productsans.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-reboot.css">
        <link rel="stylesheet" href="css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="css/slick.css">

        <!-- Material Design for Bootstarp -->
        <link rel="stylesheet" href="css/mdb.min.css">

        <link rel="stylesheet" href="css/styles.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/jquery.min.js"></script>

        <!-- Material design Icons -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- Leave those next 4 lines if you care about users using IE8 -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>


<body>

    <?php include('templates/page-preloader.php'); ?>

    <?php include('header.php'); ?>

<div class="main-container" id="page-homepage">

    <div id="overlay" class="search-overlay"></div>

    <!-- Main apartmants slider -->
    <ul id="main-slider" class="" role="listbox">

        <div id="main-slider-holder">

            <li class="main-slider-item">

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 main-slider-info ms-left">

                    <div class="ms-heading">
                        <div class="ms-price-wrap">
                            <p class="medium-label">Pay per night</p>
                            <div class="ms-price">25 €</div>
                        </div>
                        <h1 class="ms-title text-blue">twin violet</h1>
                    </div>

                    <div class="ms-info">
                        <ul class="ms-rating">
                            <li class="rated-star"><i class="fa fa-star"></i></li>
                            <li class="rated-star"><i class="fa fa-star"></i></li>
                            <li class="rated-star"><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                        </ul>
                        <p class="mb-4 ms-area">Nova Karaburma</p>
                        <p class="ms-desc text-blue">Belano apartments offer pleasant accommodation in Belgrade throughout the year.</p>

                        <a href="#" class="ms-more-btn btn-viewmore">View more <span></span></a>

                        <ul class="feature-icons mt-3">
                            <li><i class="material-icons">local_parking</i></li>
                            <li><i class="material-icons">phone</i></li>
                            <li><i class="material-icons">pool</i></li>
                            <li><i class="material-icons">wifi</i></li>
                        </ul>

                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-8 ms-right">
                    <div class="ms-image">
                        <div class="image-overlay"></div>
                        <img src="img/main-slider-image.jpg" alt="Apartment Title" />
                    </div>
                </div>

            </li>

            <li class="main-slider-item">

                <div class="col-12 col-sm-12 col-md-12 col-lg-4 main-slider-info ms-left">

                    <div class="ms-heading">
                        <div class="ms-price-wrap">
                            <p class="medium-label">Pay per night</p>
                            <div class="ms-price">25 €</div>
                        </div>
                        <h1 class="ms-title text-blue">Margareta</h1>
                    </div>

                    <div class="ms-info">
                        <ul class="ms-rating">
                            <li class="rated-star"><i class="fa fa-star"></i></li>
                            <li class="rated-star"><i class="fa fa-star"></i></li>
                            <li class="rated-star"><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                        </ul>
                        <p class="mb-4 ms-area">Nova Karaburma</p>
                        <p class="ms-desc text-blue">Belano apartments offer pleasant accommodation in Belgrade throughout the year.</p>
                        <a href="#" class="ms-more-btn btn-viewmore">View more <span></span></a>
                        <ul class="feature-icons mt-3">
                            <li><i class="material-icons">local_parking</i></li>
                            <li><i class="material-icons">navigation</i></li>
                            <li><i class="material-icons">phone</i></li>
                            <li><i class="material-icons">pool</i></li>
                            <li><i class="material-icons">wifi</i></li>
                        </ul>
                    </div>
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-8 ms-right">
                    <div class="ms-image">
                        <div class="image-overlay"></div>
                        <img src="img/main-slider-image.jpg" alt="Apartment Title" />
                    </div>
                </div>

            </li>

        </div>

        <div class="col-md-8 offset-md-4">
            <div class="main-slider-controls">

                <a href="#" class="slider-prev" id="main-slider-prev">
                    <i class="fa fa-chevron-left" aria-hidden="true"></i>
                    <p>Last</p>
                    <span>Margareta</span>
                </a>

                <div class="slider-dots">
                    <a href="#" class="ms-prev dots-prev">01</a>
                    <span class="dots-line"></span>
                    <a href="" class="ms-next dots-next">02</a>
                </div>

                <a href="#" class="slider-next" id="main-slider-next">
                    <p>Next</p>
                    <span>Margareta</span>
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </a>

            </div>
        </div>

    </ul>

    <!-- Quick-search apartment form -->
    <div id="quick-search">

        <div class="container-fluid">

            <form id="form-quick-search" action="#">

                <!-- Quick-search expanded form -->
                <div class="row" id="qs-expanded">

                    <!-- Find location -->
                    <div class="qs-expanded-block d-none bg-white py-5" id="qsb-location">
                        <div class="row align-items-center">
                            <div class="col-md-3">Find location</div>
                            <div class="col-md-3">
                                <div class="input-group prepend-icon">
                                    <i class="fa fa-map-marker input-group-text"></i>
                                    <input type="text" class="form-control smaller" name="quicksearch-location" placeholder="Enter location place">
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-3">
                                <div class="input-group prepend-icon">
                                    <i class="icon-svg">
                                        <img src="img/icon-locategps.svg" />
                                    </i>
                                    <input type="text" class="form-control smaller" name="quicksearch-gps" placeholder="Your location">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Date calendars -->
                    <div class="col-md-8 qs-expanded-block d-none" id="qsb-check-in-out">
                        <div class="row">
                            <div class="col-md-6 p-0" id="checkin-wrapper">
                                <div id="calendar-checkin" class="calendar-ui mx-1">
                                    <input type="text" name="calender-checkin-input" placeholder="Select a date" id="calendar-checkin-input">
                                </div>
                            </div>

                            <div class="col-md-6 p-0" id="checkout-wrapper">
                                <div id="calendar-checkout" class="calendar-ui mx-1">
                                    <input type="text" name="calender-checkout-input" placeholder="Select a date" id="calendar-checkout-input">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Number of people -->
                    <div class="qs-expanded-block d-none bg-white py-5 w-100" id="qsb-people">
                        <div class="row align-items-center">
                            <div class="col-md-3">Number of people</div>
                            <div class="col-md-3">
                                <select class="custom-select dropup" data-header="Number">
                                    <option value="" disabled selected>Adult</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="2">4</option>
                                    <option value="3">5</option>
                                    <option value="2">6</option>
                                    <option value="3">7</option>
                                </select>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-3">
                                <select class="custom-select dropup" data-header="Number">
                                    <option value="" disabled selected>Children</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="2">4</option>
                                    <option value="3">5</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- Quick-search inputs -->
                <div class="row justify-content-between bg-darkblue" id="qs-wrapper">

                    <div class="col-md-2 qs-inputs">Search to find apartment</div>
                    <div class="col-md-2 qs-inputs">
                        <a href="#" class="qs-btn with-arrow" id="qs-btn-location" data-toggle="qsb-location">Find location</a>
                    </div>

                    <div class="col-md-4 d-flex qs-inputs">
                        <div class="col-md-3 search-field text-right pr-0">
                            <a href="#" class="qs-btn with-arrow" id="qs-btn-checkin" data-toggle="qsb-check-in-out">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                <span id="qs-label-checkin">Check-in</span>
                            </a>
                        </div>
                        <div class="col-md-3 qs-line"></div>
                        <div class="col-md-3  search-field pl-0">
                            <a href="#" class="qs-btn with-arrow" id="qs-btn-checkout" data-toggle="qsb-check-in-out">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                <span id="qs-label-checkout">Check-out</span>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-2 qs-inputs">
                        <a href="#" class="qs-btn" id="qs-btn-people" data-toggle="qsb-people">
                            <i class="fas fa-user-friends mr-2"></i>
                            Number of people
                        </a>
                    </div>
                    <button type="button" class="col-md-2 qs-inputs btn-search-big" id="btn-qs-search">Search</button>

                </div>

            </form>
        </div>
    </div>

    <!-- HTML Block -->
    <section id="intro-text" class="bg-grey-light">
        <div class="container-fluid py-md-6 pl-md-5 p-3">
            <div class="d-flex align-items-end flex-md-row flex-lg-row flex-column-reverse">
                <div class="col-12 col-sm-6 col-md-6 text-left pl-md-5 p-0">
                    <h1 class="title-big red-color">Lorem Ipsum is simply dummy</h1>
                    <p class="text-medium text-center text-md-left">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                </div>
                <div class="col-12 col-md-5 text-right img-max-width">
                    <img src="img/image-homepage-01.jpg" alt="">
                </div>
            </div>
        </div>
    </section>

    <!-- Apartments grid + Tabs -->
    <section id="apart-grid">
        <div class="container-fluid">

            <div class="row">
                <ul class="nav nav-tabs custom-tabs" id="apart-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" id="apart-list-lux" data-toggle="tab" href="#apart-list-lux" role="tab" aria-controls="apart-list-lux" aria-selected="true">Lux apartmani</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="apart-list-day" data-toggle="tab" href="#apart-list-lux" role="tab" aria-controls="apart-list-lux" aria-selected="true">Apartmants for day</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="apart-list-day2" data-toggle="tab" href="#apart-list-lux" role="tab" aria-controls="apart-list-lux" aria-selected="true">Apartmants for day</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="apart-list-day3" data-toggle="tab" href="#apart-list-lux" role="tab" aria-controls="apart-list-lux" aria-selected="true">Apartmants for day</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="apart-list-day4" data-toggle="tab" href="#apart-list-lux" role="tab" aria-controls="apart-list-lux" aria-selected="true">Apartmants for day</a>
                    </li>
                </ul>
            </div>

            <!-- Apartment listing -->
            <div class="apart-items row justify-content-between">

                <!-- One big apartments -->
                <div class="col-12 col-sm-12 col-lg-6 p-0">
                    <?php include('templates/apart-big.php'); ?>
                </div>

                <!-- Listing 4 grid items -->
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-0">
                    <div class="row flex-row m-0">

                        <?php include('templates/apart-small.php'); ?>
                        <?php include('templates/apart-small-withbadge.php'); ?>
                        <?php include('templates/apart-small-withbadge.php'); ?>
                        <?php include('templates/apart-small-noslider.php'); ?>

                    </div>
                </div>

            </div>

            <div class="row text-left bottom-border mt-4 px-5 pb-5">
                <a href="#" class="btn-load-more">Load +50 More
                    <p class="text-blue mt-2 mb-0">If you are looking to reyervisete the cart, choose the car <br>and enter your information</p>
                </a>
            </div>

        </div>
    </section>

    <!-- One big apartment -->
    <section id="apart-full" class="bg-grey">

        <div class="container-fluid px-5">
            <div class="row">
                <div class="section-title-holder">
                    <h3 class="section-title">Name input</h3>
                    <p class="section-subtitle">If you are looking to reyervisete the cart, choose the
                        <br>car and enter your information
                    </p>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12 apart-item wide p-0">
                    <div class="apart-image">
                        <div class="apart-overlay"></div>
                        <img src="img/apartman_main_image.jpg" alt="">
                        <div class="floating-info">
                            <span class="labels label-new mb-4">New</span>
                            <p class="medium-label">Pay per night</p>
                            <p class="apart-item-price">25 €</p>
                            <h4 class="apart-item-title text-white"><a href="single-apartmant">Studio Margarita 33M2</a></h4>
                            <ul class="rate-stars light">
                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="apart-item-bottom">
                        <p class="apart-area">Nova Karaburma</p>
                        <ul class="apart-labels">
                            <li>Entire apartment</li>
                            <li>Free cancellation</li>
                        </ul>
                        <ul class="apart-features">
                            <li class="fe-parking"></li>
                            <li class="fe-location"></li>
                            <li class="fe-phone"></li>
                            <li class="fe-bookmark"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Apartments grid + Tabs - reversed -->
    <section id="apart-grid">
        <div class="container-fluid">

            <!-- Apartment listing -->
            <div class="apart-items row justify-content-between">

                <!-- Listing 4 grid items -->
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-0">
                    <div class="row flex-row m-0">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 apart-item small">

                            <div class="apart-image with-slider">

                                <span class="apart-overlay"></span>

                                <div class="apart-slider">
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                </div>

                                <div class="apart-item-info floating-info">
                                    <div class="price-wrap">
                                        <p class="medium-label">Pay per night</p>
                                        <p class="apart-item-price">25 €</p>
                                    </div>
                                    <div class="apart-title-wrap">
                                        <h4 class="apart-item-title">Studio Margarita 33M2</h4>
                                        <div class="d-flex justify-content-between">

                                            <ul class="rate-stars light">
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                                <div class="slick-controls"></div>

                            </div>

                            <div class="apart-item-bottom">
                                <p class="apart-area">Nova Karaburma</p>
                                <ul class="apart-labels">
                                    <li>Entire apartment</li>
                                    <li>Free cancellation</li>
                                </ul>
                                <ul class="apart-features">
                                    <li class="fe-parking"></li>
                                    <li class="fe-location"></li>
                                    <li class="fe-phone"></li>
                                    <li class="fe-bookmark"></li>
                                </ul>
                            </div>

                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 apart-item small">

                            <div class="apart-image with-slider">

                                <div class="promo-badge">
                                    <div class="mr-4"><img src="img/icon-ringing.svg" class="icon-svg" alt=""></div>
                                    <div class="mr-4">Shocking price</div>
                                    <div>Pay for 1 hour</div>
                                    <div class="promo-price ml-auto">15€</div>
                                </div>

                                <span class="apart-overlay"></span>

                                <div class="apart-slider">
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                </div>

                                <div class="apart-item-info floating-info">
                                    <div class="price-wrap">
                                        <p class="medium-label">Pay per night</p>
                                        <p class="apart-item-price">25 €</p>
                                    </div>
                                    <div class="apart-title-wrap">
                                        <h4 class="apart-item-title">Studio Margarita 33M2</h4>
                                        <div class="d-flex justify-content-between">

                                            <ul class="rate-stars light">
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                                <div class="slick-controls">

                                </div>

                            </div>

                            <div class="apart-item-bottom">
                                <p class="apart-area">Nova Karaburma</p>
                                <ul class="apart-labels">
                                    <li>Entire apartment</li>
                                    <li>Free cancellation</li>
                                </ul>
                                <ul class="apart-features">
                                    <li class="fe-parking"></li>
                                    <li class="fe-location"></li>
                                    <li class="fe-phone"></li>
                                    <li class="fe-bookmark"></li>
                                </ul>
                            </div>

                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 apart-item small">

                            <div class="apart-image with-slider">

                                <span class="apart-overlay"></span>

                                <div class="apart-slider">
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                </div>

                                <div class="apart-item-info floating-info">
                                    <div class="price-wrap">
                                        <p class="medium-label">Pay per night</p>
                                        <p class="apart-item-price">25 €</p>
                                    </div>
                                    <div class="apart-title-wrap">
                                        <h4 class="apart-item-title">Studio Margarita 33M2</h4>
                                        <div class="d-flex justify-content-between">

                                            <ul class="rate-stars light">
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                                <div class="slick-controls"></div>

                            </div>

                            <div class="apart-item-bottom">
                                <p class="apart-area">Nova Karaburma</p>
                                <ul class="apart-labels">
                                    <li>Entire apartment</li>
                                    <li>Free cancellation</li>
                                </ul>
                                <ul class="apart-features">
                                    <li class="fe-parking"></li>
                                    <li class="fe-location"></li>
                                    <li class="fe-phone"></li>
                                    <li class="fe-bookmark"></li>
                                </ul>
                            </div>

                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 apart-item small">

                            <div class="apart-image">

                                <span class="apart-overlay"></span>

                                <div class="apart-image">
                                    <img src="img/apartman_small_items.jpg" alt="">
                                </div>

                                <div class="apart-item-info floating-info">
                                    <div class="price-wrap">
                                        <p class="medium-label">Pay per night</p>
                                        <p class="apart-item-price">25 €</p>
                                    </div>
                                    <div class="apart-title-wrap">
                                        <h4 class="apart-item-title">Studio Margarita 33M2</h4>
                                        <div class="d-flex justify-content-between">

                                            <ul class="rate-stars light">
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="apart-item-bottom">
                                <p class="apart-area">Nova Karaburma</p>
                                <ul class="apart-labels">
                                    <li>Entire apartment</li>
                                    <li>Free cancellation</li>
                                </ul>
                                <ul class="apart-features">
                                    <li class="fe-parking"></li>
                                    <li class="fe-location"></li>
                                    <li class="fe-phone"></li>
                                    <li class="fe-bookmark"></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- One big apartments -->
                <div class="col-12 col-sm-12 col-lg-6 p-0">
                    <div class="apart-item big">
                        <div class="apart-image with-slider">

                            <div class="promo-badge">
                                <div class="mr-4"><img src="img/icon-ringing.svg" class="icon-svg" alt=""></div>
                                <div class="mr-4">Shocking price</div>
                                <div>Pay for 1 hour</div>
                                <div class="promo-price ml-auto">15€</div>
                            </div>

                            <span class="apart-overlay"></span>

                            <div class="apart-slider">
                                <div class="">
                                    <img src="img/apartman_main_image.jpg" alt="">
                                </div>
                                <div class="">
                                    <img src="img/apartman_main_image.jpg" alt="">
                                </div>
                                <div class="">
                                    <img src="img/apartman_main_image.jpg" alt="">
                                </div>
                            </div>

                            <div class="apart-item-info floating-info">
                                <span class="labels label-new mb-4">New</span>
                                <div class="price-wrap">
                                    <p class="medium-label">Pay per night</p>
                                    <p class="apart-item-price">25 €</p>
                                </div>
                                <div class="apart-title-wrap">
                                    <h4 class="apart-item-title">Studio Margarita 33M2</h4>
                                    <div class="d-flex justify-content-between">

                                        <ul class="rate-stars light">
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>

                            <div class="slick-controls"></div>

                        </div>

                        <div class="apart-item-bottom">
                            <p class="apart-area">Nova Karaburma</p>
                            <ul class="apart-labels">
                                <li>Entire apartment</li>
                                <li>Free cancellation</li>
                            </ul>
                            <ul class="apart-features">
                                <li class="fe-parking"></li>
                                <li class="fe-location"></li>
                                <li class="fe-phone"></li>
                                <li class="fe-bookmark"></li>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>

            <!-- Load More apartmants -->
            <div class="row text-left bottom-border mt-4 px-5 pb-5">
                <a href="#" class="btn-load-more">Load +50 More
                    <p class="text-blue mt-2 mb-0">If you are looking to reyervisete the cart, choose the car <br>and enter your information</p>
                </a>
            </div>

        </div>

    </section>

    <!-- Rent-a-car 2x half-width banners -->
    <section id="rentacar-banners">

        <div class="container-fluid bg-grey pb-5">

            <div class="section-title-holder pt-3 p-5">
                <h3 class="section-title">Renta Car</h3>
                <p class="section-subtitle">If you are looking to reyervisete the cart, choose the <br>car and enter your information
                </p>
            </div>

            <div class="row justify-content-center px-md-3 px-0">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 half-width-banner">

                    <div class="banner-image">
                        <img src="img/image-banner1.jpg" style="height: 100%;" alt="Rent a car">
                    </div>
                    <div class="banner-info">
                        <h5 class="banner-title">Belano Limo service</h5>
                        <div class="sepline"></div>
                        <p class="banner-text">Lorem Ipsum is simply dummy text of the <br> printing and typesetting industry</p>
                    </div>

                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 half-width-banner">
                    <div class="banner-image">
                        <img src="img/image-banner2.jpg" alt="Rent a car">
                    </div>
                    <div class="banner-info">
                        <h5 class="banner-title">Belano Limo service</h5>
                        <div class="sepline"></div>
                        <p class="banner-text">Lorem Ipsum is simply dummy text of the <br> printing and typesetting industry</p>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- Static block: Benefits -->
    <section id="benefits" class="bottom-border">

        <div class="container-fluid bottom-border p-md-5 py-md-2 p-4 pt-5">
            <div class="section-title-holder p-0">
                <h3 class="section-title">Benefits</h3>
                <p class="section-subtitle">If you are looking to reyervisete the cart, choose the <br>car and enter your information
                </p>
            </div>
        </div>

        <div class="container-fluid px-md-5 py-md-6 px-0 benefits-slider-wrapper">

            <div class="text-center" id="benefits-slider">
                <div class="benefits-item">
                    <div class="benefits-image">
                        <img src="img/image-benefits-01.jpg" alt="Welcome home pristup">
                    </div>
                    <p class="benefits-title">Welcome home pristup</p>
                </div>
                <div class="benefits-item">
                    <div class="benefits-image">
                        <img src="img/image-benefits-02.png" alt="Transfer od/do aerodroma">
                    </div>
                    <p class="benefits-title light">Transfer od/do aerodroma</p>
                </div>
                <div class="benefits-item">
                    <div class="benefits-image">
                        <img src="img/image-benefits-03.jpg" alt="Atraktivne lokacije">
                    </div>
                    <p class="benefits-title light">Atraktivne lokacije</p>
                </div>
                <div class="benefits-item">
                    <div class="benefits-image">
                        <img src="img/image-benefits-04.png" alt="Welcome home pristup">
                    </div>
                    <p class="benefits-title">Besprekorna čistoća</p>
                </div>
                <div class="benefits-item">
                    <div class="benefits-image">
                        <img src="img/image-benefits-05.png" alt="Transfer od/do aerodroma">
                    </div>
                    <p class="benefits-title light">Parking na upit</p>
                </div>
                <div class="benefits-item">
                    <div class="benefits-image">
                        <img src="img/image-benefits-06.png" alt="Atraktivne lokacije">
                    </div>
                    <p class="benefits-title light">Lojalti program</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Blog: Featured post -->
    <section id="blog" class="bottom-border">

        <div id="blog-featured" class="section-wrapper container-fluid bottom-border py-3 py-md-6 bg-grey">
            <div class="d-flex justify-content-md-start flex-column flex-sm-column flex-md-row">
                <div class="col-12 col-sm-12 col-md-5 col-lg-5 p-0">
                    <p class="block-title mb-3">Blog</p>
                    <h5 class="blog-date mb-5">New post 25. Jun 2018</h5>
                    <h3 class="blog-title red-color mb-4">24 Kitchen<br>Jamie Oliver kuva za vas</h3>
                    <p class="text-medium mb-5 pr-5 text-lightblue">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <p class="text-medium mb-5 pr-5 text-lightblue">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <a href="#" class="more-info">More info</a>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-7 featured-blog-image text-right">
                    <img src="img/image-featured-blog.jpg" alt="24 Kitchen Jamie Oliver kuva za vas">
                </div>
            </div>
        </div>

        <!-- Blog: Post slider -->
        <div id="blog-slider" class="container-fluid pl-5 pr-0">

            <div class="section-title-holder">
                <h3 class="section-title">New posts</h3>
                <p class="section-subtitle">If you are looking to reyervisete the cart, choose the
                    <br>car and enter your information
                </p>
            </div>

            <ul class="blog-slider-wrapper" id="blog-post-slider">
                <li class="bs-item">
                    <div class="bs-image">
                        <img src="img/blog-post-image.jpg" alt="Blog Post Title">
                    </div>
                    <h5 class="bs-title">Belgrade on water. Project inheritance</h5>
                    <p class="bs-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <a href="single-blog-post" class="bs-more-btn">Discover more</a>
                </li>
                <li class="bs-item">
                    <div class="bs-image">
                        <img src="img/blog-post-image.jpg" alt="Blog Post Title">
                    </div>
                    <h5 class="bs-title">Belgrade on water. Project inheritance</h5>
                    <p class="bs-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <a href="single-blog-post" class="bs-more-btn">Discover more</a>
                </li>
                <li class="bs-item">
                    <div class="bs-image">
                        <img src="img/blog-post-image.jpg" alt="Blog Post Title">
                    </div>
                    <h5 class="bs-title">Belgrade on water. Project inheritance</h5>
                    <p class="bs-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <a href="single-blog-post" class="bs-more-btn">Discover more</a>
                </li>
                <li class="bs-item">
                    <div class="bs-image">
                        <img src="img/blog-post-image.jpg" alt="Blog Post Title">
                    </div>
                    <h5 class="bs-title">Belgrade on water. Project inheritance</h5>
                    <p class="bs-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <a href="single-blog-post" class="bs-more-btn">Discover more</a>
                </li>
                <li class="bs-item">
                    <div class="bs-image">
                        <img src="img/blog-post-image.jpg" alt="Blog Post Title">
                    </div>
                    <h5 class="bs-title">Belgrade on water. Project inheritance</h5>
                    <p class="bs-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <a href="single-blog-post" class="bs-more-btn">Discover more</a>
                </li>
                <li class="bs-item">
                    <div class="bs-image">
                        <img src="img/blog-post-image.jpg" alt="Blog Post Title">
                    </div>
                    <h5 class="bs-title">Belgrade on water. Project inheritance</h5>
                    <p class="bs-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <a href="single-blog-post" class="bs-more-btn">Discover more</a>
                </li>
            </ul>
        </div>

    </section>

    <!-- Static block: About -->
    <section id="quick-about" class="bg-grey-light p-md-5 p-0 bottom-border">
        <div class="container-fluid section-wrapper">
            <h3 class="title-big red-color">Belano apartments Belgrade</h3>
            <div class="col-md-7 qa-image text-center mb-3 m-auto img-max-width">
                <img src="img/image-homepage-01.png" alt="">
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <p class="text-small text-blue">Belano apartmani nude ugodan smeštaj u Beogradu tokom cele godine, svima kojima je osećaj vlastitog doma najvažniji na njihovim putovanjima.. </p>
                    <p class="text-small text-blue">Prijatan enterijer, atraktivne lokacije, pristupačne cene i domaćinska usluga su ono to Belano apartmane izdvaja od drugih agencija slične poslovne orijentacije. U duhu domaćinske tradicije i gostoprimljivosti našeg grada i naroda, izaći ćemo u susret svakoj vašoj želji, a kakvi bi to domaćini bili kada vas ne bi sačekali na aerodromu, autobuskoj i železničkoj stanici, ili na bilo kom drugom mestu koje vama odgovara?
                    </p>
                </div>
                <div class="col-12 col-md-6">
                    <p class="text-small text-blue">Belano apartmani nude ugodan smeštaj u Beogradu tokom cele godine, svima kojima je osećaj vlastitog doma najvažniji na njihovim putovanjima.. </p>
                    <p class="text-small text-blue">Prijatan enterijer, atraktivne lokacije, pristupačne cene i domaćinska usluga su ono to Belano apartmane izdvaja od drugih agencija slične poslovne orijentacije. U duhu domaćinske tradicije i gostoprimljivosti našeg grada i naroda, izaći ćemo u susret svakoj vašoj želji, a kakvi bi to domaćini bili kada vas ne bi sačekali na aerodromu, autobuskoj i železničkoj stanici, ili na bilo kom drugom mestu koje vama odgovara?
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact form + Map -->
    <section id="contact" class="pb-mb-5 px-md-5 px-4">
        <div class="container-fluid p-0">

            <div class="section-title-holder">
                <h3 class="section-title">Name input</h3>
                <p class="section-subtitle">If you are looking to reyervisete the cart, choose the
                    <br>car and enter your information
                </p>
            </div>

            <div class="d-flex justify-content-strech mb-5">
                <img src="img/image-map2.jpg" alt="" class="w-100">
            </div>

            <div class="row text-blue justify-content-between">
                <div class="col d-flex justify-content-start">
                    <i class="fa fa-map-marker mr-3 mt-1"></i>
                    <span>
                        Birčaninova, 11000 Beograd,Srbija<br>
                        office@belano.rs
                    </span>
                </div>
                <div class="col d-flex justify-content-center">
                    <i class="fa fa-globe mr-3 mt-1"></i>
                    <span>
                        <a href="http://www.belano.rs">http://www.belano.rs</a>
                    </span>
                </div>
                <div class="col d-flex justify-content-end">
                    <i class="fa fa-phone mr-3 mt-1"></i>
                    <span>
                        +381 60 55 66 509<br>
                        +381 66 55 66 509<br>
                        +381 11 36 77 773
                    </span>
                </div>
            </div>
        </div>
    </section>

    <section id="contact-form" class="p-5 bg-grey">

        <div class="container-fluid p-0">

            <form action="#" class="w-100">

                <div class="row d-flex">

                    <div class="col-12 col-md-6">
                        <div class="form-label-group mb-4">
                            <input type="text" name="contact-name" id="contact-name" class="form-control" required="required" placeholder="Fullname *">
                            <label for="contact-name">Fullname *</label>
                        </div>
                        <div class="form-label-group mb-4">
                            <input type="text" name="contact-email" id="contact-email" class="form-control" required="required" placeholder="Email *">
                            <label for="contact-email">Email *</label>
                        </div>
                        <div class="form-label-group mb-4">
                            <input type="text" name="contact-subject" id="contact-subject" class="form-control" required="required" placeholder="Subject">
                            <label for="contact-subject">Subject</label>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 pr-0">
                        <div class="form-label-group">
                            <textarea name="contact-message" id="contact-message" cols="30" rows="20" class="form-control" placeholder="Message"></textarea>
                            <label for="contact-message">Message</label>
                        </div>

                    </div>

                    <div class="text-right w-100">
                        <input type="submit" value="Send" class="btn btn-primary btn-medium">
                    </div>

            </form>

        </div>

    </section>

</div>

    <!-- Footer-->
    <footer>
        <div class="container-fluid">
            <div class="row justify-content-between">

                <div class="col-3" id="footer-left">

                    <a href="#" class="footer-logo">
                        <img src="img/logo-header-light.png" alt="">
                    </a>

                    <div class="small">
                        <div class="nav-lang mb-3" class="mb-3"><i class="fa fa-globe mr-2"> </i>Eng / Srb</div>
                        <div class="nav-weather"><img src="img/icon-weather.png" alt=""> &nbsp; Belgrade 34F</div>
                    </div>

                    <div class="social-links">
                        <a href="instagram.com"><i class="fa fa-instagram"></i></a>
                        <a href="facebook.com"><i class="fa fa-facebook"></i></a>
                        <a href="pinterest"><i class="fa fa-pinterest"></i></a>
                        <a href="linkedin"><i class="fa fa-linkedin"></i></a>
                        <a href="email"><i class="fa fa-envelope"></i></a>
                    </div>

                </div>

                <div class="col-9" id="footer-right">
                    <div class="row justify-content-between">
                        <ul class="footer-links">
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Apartmani</a></li>
                            <li><a href="#">Kontakt</a></li>
                            <li><a href="#">Korisne informacije</a></li>
                            <li><a href="#">Lojalti</a></li>
                        </ul>

                        <ul class="footer-links">
                            <li class="title">Lux apartments</li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                        </ul>

                        <ul class="footer-links">
                            <li class="title">Lux apartments</li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                        </ul>

                        <ul class="footer-links">
                            <li class="title">Lux apartments</li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </footer>

    <section id="subfooter" class="p-4">
        <div class="d-flex justify-content-between align-items-center">
            <div class="col text-left">© Copyright</div>
            <div class="col text-right d-flex align-items-center justify-content-end">Power by <img src="img/logo-dimis.png" alt="Dimis" class="ml-2"></div>
        </div>
    </section>

    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="js/mdb.min.js"></script>
    <script src="js/scripts.min.js"></script>
    <script src="js/caleran.js"></script>
    <script src="js/bootstrap-select.js"></script>
    <script src="js/main.js"></script>

</body>
</html>