<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Belano.rs - Login/Register Loyalty</title>

    <link rel="stylesheet" href="fonts/productsans.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.css">

    <!-- Material Design for Bootstrap -->
    <link rel="stylesheet" href="css/mdb.min.css">

    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/jquery.min.js"></script>

    <!-- Google Sing-in -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id" content="514439232405-u920cnd9j2e471k92iqjqq2p6ja0rnkd.apps.googleusercontent.com">

    <!-- FontAwesome Icons -->
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700&amp;subset=latin-ext" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- Leave those next 4 lines if you care about users using IE8 -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-loyalty-payment">

<?php include('header.php'); ?>

<?php include('templates/page-preloader.php'); ?>


<div class="main-container">

    <div class="container-fluid">

        <div class="row">

            <!-- Login / Register form -->
            <div class="col-6" id="container-left">

                <div id="inner-left">
                    <div class="inner-left-content d-flex bg-white py-5">
                        <div id="login-loyalty-wrapper" class="access-wrapper w-100">

                            <h4 class="page-title mb-3">Login loyalty</h4>
                            <p class="medium-text text-lightblue mb-0">Welcome back.</p>
                            <p class="medium-text text-lightblue">Please login in to your account</p>

                            <div class="my-5">
                                <div class="g-signin2 btn btn-white with-shadow m-0 rounded" data-onsuccess="onSignIn"></div>
                                <a href="#" class="btn btn-white rounded with-shadow ml-3" id="btn-login-loyalty-facebook">With Facebook</a>
                            </div>

                            <!-- Form: Login -->
                            <form action="" id="form-login-loyalty" class="w-100">

                                <div class="form-label-group mb-0">
                                    <input type="text" name="login-loyalty-username" id="login-loyalty-username" class="form-control with-shadow" required="required" placeholder="Name or E-mail">
                                    <label for="login-loyalty-username" class="active">Name or E-mail</label>
                                </div>

                                <div class="form-label-group mb-0">
                                    <input type="password" name="login-loyalty-password" id="login-loyalty-password" class="form-control with-shadow validate" required="required" placeholder="Password">
                                    <label for="login-loyalty-password" class="active">Password</label>
                                    <span toggle="#login-loyalty-password" class="fas fa-eye-slash toggle-password"></span>
                                </div>

                                <div class="d-flex justify-content-between mt-3">
                                    <div class="form-check p-0">
                                        <input type="checkbox" class="form-check-input" id="login-loyalty-remember" name="login-loyalty-remember">
                                        <label class="form-check-label" for="login-loyalty-remember">Remember me</label>
                                    </div>
                                    <a class="btn-link" href="/lost-password">Forget password</a>
                                </div>

                                <div class="mt-4">
                                    <button type="submit" class="btn btn-outline-primary rounded no-shadow m-0" id="btn-login-regular" disabled>Login</button>
                                    <a href="#" class="btn btn-outline-light rounded no-shadow btn-toggle-div" data-open-div="#register-loyalty-wrapper" data-close-div="#login-loyalty-wrapper" id="btn-open-register-regular">Sign-up</a>
                                </div>

                                <div class="mt-2 p-0 form-check">
                                    <input type="checkbox" class="form-check-input" id="login-loyalty-privacy" name="login-loyalty-privacy">
                                    <label class="form-check-label" for="login-loyalty-privacy">Do you agree withe Privte Police / Terms</label>
                                </div>

                            </form>

                        </div>
                        <div id="register-loyalty-wrapper" class="access-wrapper w-100 hidden">

                            <h4 class="page-title mb-3">Register loyalty</h4>
                            <p class="medium-text text-lightblue mb-0">Welcome back.</p>
                            <p class="medium-text text-lightblue">Please register in to your account</p>

                            <div class="my-5">
                                <div class="g-signin2 btn btn-white with-shadow m-0 rounded" data-onsuccess="onSignIn"></div>
                                <a href="#" class="btn btn-white rounded with-shadow ml-3" id="btn-login-loyalty-facebook">With Facebook</a>
                            </div>

                            <!-- Form: Register -->
                            <form action="" id="form-register-loyalty" class="w-100">

                                <div class="form-label-group mb-0">
                                    <input type="text" name="register-loyalty-username" id="register-loyalty-username" class="form-control with-shadow" required="required" placeholder="Fullname">
                                    <label for="register-loyalty-username" class="active">Fullname *</label>
                                </div>

                                <div class="form-label-group mb-0">
                                    <input type="text" name="register-loyalty-email" id="register-loyalty-email" class="form-control with-shadow" required="required" placeholder="E-mail address">
                                    <label for="register-loyalty-email" class="active">E-mail address *</label>
                                </div>

                                <div class="form-label-group mb-0">
                                    <input type="password" name="login-loyalty-password" id="login-loyalty-password" class="form-control with-shadow validate" required="required" placeholder="Password">
                                    <label for="login-loyalty-password" class="active">Password *</label>
                                    <span toggle="#login-loyalty-password" class="fas fa-eye-slash toggle-password"></span>
                                </div>

                                <div class="form-label-group mb-0">
                                    <input type="password" name="login-loyalty-password2" id="login-loyalty-password2" class="form-control with-shadow validate" required="required" placeholder="Repeat password">
                                    <label for="login-loyalty-password" class="active">Repeat password *</label>
                                    <span toggle="#login-loyalty-password" class="fas fa-eye-slash toggle-password"></span>
                                </div>

                                <div class="d-flex justify-content-start mt-3">
                                    <div class="form-check p-0">
                                        <input type="checkbox" class="form-check-input" id="register-loyalty-remember" name="register-loyalty-remember">
                                        <label class="form-check-label" for="register-loyalty-remember">Remember me</label>
                                    </div>
                                </div>

                                <div class="mt-4">
                                    <button type="submit" class="btn btn-outline-primary rounded no-shadow m-0" id="btn-register-regular" disabled>Register</button>
                                    <a href="#" class="btn btn-outline-light rounded no-shadow btn-toggle-div" data-open-div="#login-loyalty-wrapper" data-close-div="#register-loyalty-wrapper">Login</a>
                                </div>

                                <div class="mt-2 p-0 form-check">
                                    <input type="checkbox" class="form-check-input" id="register-loyalty-privacy"
                                           name="register-loyalty-privacy">
                                    <label class="form-check-label" for="register-loyalty-privacy">Do you agree withe Privte Police / Terms</label>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>

            </div>

            <!-- Single Apart. Promo -->
            <div class="col-6 d-none d-sm-none d-md-block d-lg-block" id="container-right">

                <div class="inner-right border-left">

                    <div class="img-max-width">
                        <img src="img/image-loyalty-image1.jpg" alt="Loyalty">
                    </div>

                    <div class="p-6">
                        <h1 class="text-darkblue mb-5">Smooth out your day, <br> everyday.</h1>
                        <p class="text-small text-lightblue">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                            irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                            pariatur. </p>
                        <p class="text-small text-lightblue">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                            irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                            pariatur. </p>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>


<script>

    $(document).ready(function(){

        // Google Sign-in
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
        }

        preloader();

        // Login (regular) form
        $('#form-login-loyalty').submit(function(e) {
            e.preventDefault();

            // TODO validate
            // Submit form

            var loginData = $(this).serialize();
            alert(loginData);

            window.location = "loyalty-payment"

        });

        // Login (regular) privacy check
        $('input[name="login-loyalty-privacy"]').change(function () {
            if( $(this).prop("checked") == true ) {
                $('#btn-login-regular').removeAttr('disabled');
            }
            else {
                $('#btn-login-regular').attr('disabled', true);
            }
        });

        $('.btn-toggle-div').click(function(e){
            e.preventDefault();
            var openDiv = $(this).data('open-div');
            var closeDiv = $(this).data('close-div');

            $(openDiv).removeClass('hidden');
            $(closeDiv).addClass('hidden');
        });

        // Register (regular) form
        $('#form-register-loyalty').submit(function(e) {
            e.preventDefault();

            // TODO validate
            // Submit form

            var registerData = $(this).serialize();
            alert(registerData);
        });

        // Register privacy check
        $('input[name="register-loyalty-privacy"]').change(function () {
            if( $(this).prop("checked") == true ) {
                $('#btn-register-regular').removeAttr('disabled');
            }
            else {
                $('#btn-register-regular').attr('disabled', true);
            }
        });

        // Login with Google (regular)
        $('#btn-login-reg-google').click(function(e) {
            e.preventDefault();
            alert('init google login');
        });

        // Login with Facebook (regular)
        $('#btn-login-reg-facebook').click(function(e) {
            e.preventDefault();
            alert('init facebook login');
        });

    })

</script>


<?php include('bottom-includes.php'); ?>