<!-- Header -->
<header>

    <div id="header-top">
        <div class="container-fluid">
            <div class="row justify-content-between align-items-center header-wrapper">

                <a id="logo-header" href="index.php">
                    <img src="img/logo-belano.svg" alt="" class="logo-header logo-light">
                    <img src="img/logo-belano-dark.svg" alt="" class="logo-header logo-dark">
                </a>

                <div class="d-block d-sm-block d-md-none d-lg-none nav-weather"><img src="img/icon-weather.png" alt="">Belgrade 34F</div>

                <a id="search-trigger" href="#" class="d-none d-sm-none d-md-block d-lg-block">
                    <i class="fa fa-search mr-2"></i>
                    Search apartments
                </a>

                <span class="d-block d-sm-block d-md-none d-lg-none navbar-text white-text" id="nav-account">
                        <a href="#" class="nav-acc-link"><i class="fa fa-heart"></i></a>
                        <a href="#" class="nav-acc-link icon-notification active">
                            <i class="fa fa-bell-o" aria-hidden="true"></i>
                            <span class="icon-bubble">9</span>
                        </a>
                    </span>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="d-none d-sm-none d-lg-block social-links">
                    <a href="instagram.com"><i class="fab fa-instagram"></i></a>
                    <a href="facebook.com"><i class="fab fa-facebook"></i></a>
                    <a href="pinterest"><i class="fab fa-pinterest"></i></a>
                    <a href="linkedin"><i class="fab fa-linkedin"></i></a>
                    <a href="email"><i class="fa fa-envelope"></i></a>
                </div>

            </div>


        </div>
    </div>

    <!-- Search dropdown -->
    <div id="search-expanded">

        <div class="w-100 bottom-border search-input-wrapper">
            <div class="container">
                <div class="label-float">
                    <label>Search -  address, name of apartmans or street</label>
                    <input type="search" class="mdb-autocomplete" name="search-input" id="search-input" placeholder='“ Belgrade -Palilula - Bulevar Kralja Aleksandra 18 - Apartman - Belano "'>
                </div>
            </div>
        </div>

        <div id="search-recent">
            <div class="container">
                <ul class="search-history">
                    <span class="block-label">Recent searches</span>
                    <li><img src="img/icon-clock.png" alt=""> Vozdovac - vojovde stepe 18 - DIMIS</li>
                    <li><img src="img/icon-clock.png" alt=""> Vozdovac - vojovde stepe 18 - DIMIS Agencija</li>
                    <li><img src="img/icon-clock.png" alt=""> Kralja Milutina 30, Vracar</li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light" id="nav">

        <!-- Logo -->
        <a id="logo-header" class="d-block d-sm-block d-md-none d-lg-none" href="index.php">
            <img src="img/logo-belano.svg" alt="" class="logo-header logo-light">
            <img src="img/logo-belano-dark.svg" alt="" class="logo-header logo-dark">
        </a>

        <!-- Lang + Weather -->
        <div id="nav-left" class="d-block d-sm-block d-md-none d-lg-none">
            <div class="nav-lang" class="mr-5"><i class="fa fa-globe mr-2"> </i>Eng / Srb</div>
            <div class="nav-weather"><img src="img/icon-weather.svg" alt=""> &nbsp; Belgrade 34F</div>
        </div>

        <button id="search-trigger-mobile" class="d-block d-sm-block d-mg-none d-lg-none">
            <i class="fa fa-search mr-2"></i>
        </button>

        <a href="#" class="nav-acc-link d-block d-sm-block d-md-none d-lg-none"><i class="fa fa-heart"></i></a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-main" aria-controls="navbar-main"
                aria-expanded="false" aria-label="Toggle navigation">
            <div id="hamburger-menu">
                <span></span><span></span><span></span>
            </div>
        </button>

        <div class="collapse navbar-collapse" id="navbar-main">

            <!-- Lang + Weather -->
            <div id="nav-left" class="d-none d-sm-flex d-md-flex">
                <div class="nav-lang" class="mr-5"><i class="fa fa-globe mr-2"> </i>Eng / Srb</div>
                <div class="nav-weather"><img src="img/icon-weather.png" alt=""> &nbsp; Belgrade 34F</div>
            </div>

            <!-- Main navigation -->
            <ul class="navbar-nav" id="nav-menu">
                <li class="nav-item"><a href="blog" class="nav-link">Blog</a></li>
                <li class="nav-item">

                    <a href="apartments" class="nav-link">Apartmani</a>

                    <!-- <a href="#apartments" class="nav-link dropdown-toggle" data-trigger="click" id="nav-apartments" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Apartmani</a> -->
                    <!-- <div class="dropdown-menu mega-menu v-2 w-100" aria-labelledby="nav-apartments">

                        <div id="apart-dropdown">

                            <div class="apart-slider-wrapper" id="apartments-nav-slider">
                                <div class="as-item centar">
                                    <span>explore</span>
                                    <h4 class="as-title">Apartmani Centar</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                                </div>
                                <div class="as-item slavija">
                                    <span>explore</span>
                                    <h4 class="as-title">Apartmani Centar</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                                </div>
                                <div class="as-item centar">
                                    <span>explore</span>
                                    <h4 class="as-title">Apartmani Centar</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                                </div>
                                <div class="as-item slavija">
                                    <span>explore</span>
                                    <h4 class="as-title">Apartmani Centar</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                                </div>
                                <div class="as-item centar">
                                    <span>explore</span>
                                    <h4 class="as-title">Apartmani Centar</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                                </div>
                                <div class="as-item slavija">
                                    <span>explore</span>
                                    <h4 class="as-title">Apartmani Centar</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                                </div>
                                <div class="as-item centar">
                                    <span>explore</span>
                                    <h4 class="as-title">Apartmani Centar</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                                </div>
                                <div class="as-item slavija">
                                    <span>explore</span>
                                    <h4 class="as-title">Apartmani Centar</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                                </div>
                                <div class="as-item centar">
                                    <span>explore</span>
                                    <h4 class="as-title">Apartmani Centar</h4>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                                </div>
                            </div>

                        </div>

                    </div> -->

                </li>
                <li class="nav-item"><a href="contact" class="nav-link">Kontakt</a></li>
                <li class="nav-item"><a href="#" class="nav-link">Korisne informacije</a></li>
                <li class="nav-item"><a href="loyalty" class="nav-link">Lojalti</a></li>
                <li class="nav-item"><a href="rent-a-car" class="nav-link">Rent-a-car</a></li>
            </ul>

            <span class="navbar-text white-text" id="nav-account">
                <a href="#" class="nav-acc-link"><i class="fa fa-heart"></i></a>
                <a href="#" class="nav-acc-link icon-notification active">
                    <i class="fa fa-bell-o" aria-hidden="true"></i>
                    <span class="icon-bubble">9</span>
                </a>
                <a href="login-register" class="nav-acc-link">
                    <i class="icon-fingerprint"></i>
                </a>
                <a href="login-register" class="nav-acc-link">Sign-up</a>

            </span>
        </div>

    </nav>

</header>