<?php include('head-owners.php'); ?>

<body id="dash-users" class="dash-history">
<header>

    <!-- Top navigation -->
    <nav id="dash-nav" class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
        <div class="container-fluid p-0">
            <div class="navbar-left pl-4">

                <div class="logo">
                    <a href="../"><img src="img/header-logo.svg" alt="Belano.rs"></a>
                </div>

                <div class="page-title">Owners Dashboard</div>

                <!-- Language switcher -->
                <div class="lang">
                    <a href="#">Eng</a> / <a href="#">Srb</a>
                </div>

                <!-- Weather widget -->
                <div class="nav-weather">
                    <img src="../img/icon-weather.png" alt="">
                    <span>Belgrade 34F</span>
                </div>

            </div>

            <div class="navbar-right">
                <div class="paket">
                    <img src="../img/dashboard/flag-icon.png" alt="">
                    <span>Basic Paket</span>
                </div>
                <div class="user-id">
                    <i class="material-icons text-secondary mr-3">fingerprint</i>
                    <span>ID 12345679</span>
                </div>

                <!-- Profile menu -->
                <div class="dropdown profile-dropdown-wrap px-4">
                    <a href="" class="profile-dropdown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="https://via.placeholder.com/38x38" class="rounded-circle mr-3" alt="Milena Dragisic">
                        <span class="user-name text-medium mr-3">Milena Dragisic</span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Additional questions</a>
                        <a class="dropdown-item" href="#">Help</a>
                        <a class="dropdown-item" href="#">Privacy</a>
                    </div>
                </div>

                <!-- Messages menu -->
                <div class="message-dropdown-wrap chat has-msg p-4">
                    <a href="" class="message-dropdown dropdown-toggle d-flex align-items-center" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">chat_bubble_outline</i>
                    </a>
                    <div class="message-dropdown-menu dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">
                            <div>
                                <div class="avatar">
                                    <span class="letter">B</span>
                                </div>
                            </div>
                            <div class="about">
                                <div class="role text-medium">Administrator Belana</div>
                                <div class="desc text-medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</div>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#">
                            <div>
                                <div class="avatar">
                                    <span class="letter">B</span>
                                </div>
                            </div>
                            <div class="about">
                                <div class="role text-medium">Administrator Belana</div>
                                <div class="desc text-medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</div>
                            </div>
                        </a>
                    </div>
                </div>

                <!-- Sign-out / close button -->
                <div class="close-page">
                    <a href=""><i class="material-icons md-24">power_settings_new</i></a>
                </div>
            </div>
        </div>
    </nav>

    <!-- Sidebar navigation -->
    <aside id="dash-sidebar" class="sidebar-fixed position-fixed">
        <div class="content">
            <ul class="nav">
                <li class="nav-link">
                    <a href="owners"><i class="material-icons">home</i></a>
                </li>
                <li class="nav-link">
                    <a href="owners">
                        <i class="material-icons">assignment_ind</i>
                        <span class="nav-link-text">Profil</span>
                    </a>
                </li>
                <li class="nav-link">
                    <a href="owners-history">
                        <i class="material-icons">history</i>
                        <span class="nav-link-text">History</span>
                    </a>
                </li>
                <li class="nav-link">
                    <a href="owners-favorites">
                        <i class="material-icons">favorite</i>
                        <span class="nav-link-text">Favorites</span>
                    </a>
                </li>
                <li class="nav-link">
                    <a href="owners-card">
                        <i class="material-icons">credit_card</i>
                        <span class="nav-link-text">Card</span>
                    </a>
                </li>
                <li class="nav-link">
                    <a href="owners-comments">
                        <i class="material-icons">chat_bubble_outline</i>
                        <span class="nav-link-text">Comments</span>
                    </a>
                </li>
                <li class="nav-link">
                    <a href="owners-new-apartment">
                        <i class="material-icons">playlist_add</i>
                        <span class="nav-link-text">Add new apartment</span>
                    </a>
                </li>
                <li class="nav-link download">
                    <a href="#">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19.35 10.04C18.67 6.59 15.64 4 12 4C9.11 4 6.6 5.64 5.35 8.04C2.34 8.36 0 10.91 0 14C0 17.31 2.69 20 6 20H19C21.76 20 24 17.76 24 15C24 12.36 21.95 10.22 19.35 10.04ZM17 13L12 18L7 13H10V9H14V13H17Z" fill="#D1DBE2"/>
                        </svg>

                        <span class="nav-link-text">Download. PDF</span>
                    </a>
                </li>
            </ul>
            <div class="menu-trigger">
                <a href="" class="menu-open">
                    <img src="../img/dashboard/Hamburger%20Menu.svg" alt="">
                </a>
                <a href="" class="menu-close">
                    <img src="../img/dashboard/ic_close_black_24px.svg" alt="">
                    <span>Close</span>
                </a>
            </div>
        </div>
    </aside>

</header>
<main id="dash-content">
    <div class="container-fluid p-0">
        <div class="card">
            <div class="card-body">
                <!--download actions-->
                <div class="row">
                    <div class="col-12 download-actions">
                        <div class="d-flex justify-content-end">
                            <div class="d-flex align-items-center">
                                <span class="actions-label">Download</span>
                                <i class="fa fa-cloud-download-alt"></i>
                                <i class="fa fa-search"></i>
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <!--history table new info-->
                <div class="row">
                    <div class="col-12">
                        <table class="table table-history table-history-new-info new-likes">
                            <tbody>
                            <tr>
                                <td class="order">
                                    <div class="td-label">Order</div>
                                    <div class="td-value">210685</div>
                                </td>
                                <td class="apart-name">
                                    <div class="td-label">Name apartments :</div>
                                    <div class="td-value text-truncate">Dorcol Delux</div>
                                </td>
                                <td class="date-time">
                                    <div class="td-label">Date & Time</div>
                                    <div class="td-value">21. 06. 2018</div>
                                </td>
                                <td class="fav">
                                    <div class="td-value">
                                        <i class="fa fa-heart fa-2x"></i>
                                    </div>
                                </td>
                                <td class="rating">
                                    <div class="td-value">
                                        <ul class="rate-stars apart-stars">
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="full-screen">
                                    <div class="td-value"><span>Full screen</span><i class="fa fa-external-link-alt"></i> </div>
                                </td>
                                <td class="delete">
                                    <div class="td-value">
                                        <i class="fa fa-trash-alt"></i>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!--history table-->
                <div class="row">
                    <div class="col-12">
                        <table class="table table-history">
                            <tbody>
                            <tr>
                                <td class="order">
                                    <div class="td-label">Order</div>
                                    <div class="td-value">210685</div>
                                </td>
                                <td class="apart-name">
                                    <div class="td-label">Name apartments :</div>
                                    <div class="td-value text-truncate">Dorcol Delux</div>
                                </td>
                                <td class="date-time">
                                    <div class="td-label">Date & Time</div>
                                    <div class="td-value">21. 06. 2018</div>
                                </td>
                                <td class="fav">
                                    <div class="td-value">
                                        <i class="fa fa-heart fa-2x"></i>
                                    </div>
                                </td>
                                <td class="rating">
                                    <div class="td-value">
                                        <ul class="rate-stars apart-stars">
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="full-screen">
                                    <div class="td-value"><span>Full screen</span><i class="fa fa-external-link-alt"></i> </div>
                                </td>
                                <td class="delete">
                                    <div class="td-value">
                                        <i class="fa fa-trash-alt"></i>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="order">
                                    <div class="td-label">Order</div>
                                    <div class="td-value">210685</div>
                                </td>
                                <td class="apart-name">
                                    <div class="td-label">Name apartments :</div>
                                    <div class="td-value text-truncate">Dorcol Delux</div>
                                </td>
                                <td class="date-time">
                                    <div class="td-label">Date & Time</div>
                                    <div class="td-value">21. 06. 2018</div>
                                </td>
                                <td class="fav">
                                    <div class="td-value">
                                        <i class="fa fa-heart fa-2x"></i>
                                    </div>
                                </td>
                                <td class="rating">
                                    <div class="td-value">
                                        <ul class="rate-stars apart-stars">
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="full-screen">
                                    <div class="td-value"><span>Full screen</span><i class="fa fa-external-link-alt"></i> </div>
                                </td>
                                <td class="delete">
                                    <div class="td-value">
                                        <i class="fa fa-trash-alt"></i>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="load-more"><span>Load More + 16</span></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="content-footer" class="d-flex justify-content-between align-items-center">
            <div><i class="fa fa-phone mr-2"></i> Call us <a href="" class="ml-2">Number</a></div>
            <div>Having problems with the web app? <a href="" class="ml-2">Contact Us</a></div>
            <div>Copyright <a href="" class="ml-2">Belano</a></div>
        </div>
    </div>
</main>
    <script src="../js/bootstrap.bundle.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script src="../js/mdb.min.js"></script>
    <script src="../js/scripts.min.js"></script>
    <script src="../js/caleran.js"></script>
    <script src="../js/bootstrap-select.js"></script>
    <script src="../js/timepicker.js"></script>
    <script src="../js/main.js"></script>

</body>
</html>
