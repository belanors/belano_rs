<nav id="dash-nav" class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
    <div class="container-fluid p-0">
        <div class="navbar-left pl-4">

            <div class="logo">
                <a href="../"><img src="img/header-logo.svg" alt="Belano.rs"></a>
            </div>

            <div class="page-title">Owners Dashboard</div>

            <!-- Language switcher -->
            <div class="lang">
                <a href="#">Eng</a> / <a href="#">Srb</a>
            </div>

            <!-- Weather widget -->
            <div class="nav-weather">
                <img src="../img/icon-weather.png" alt="">
                <span>Belgrade 34F</span>
            </div>

        </div>

        <div class="navbar-right">
            <div class="paket">
                <img src="../img/dashboard/flag-icon.png" alt="">
                <span>Basic Paket</span>
            </div>
            <div class="user-id">
                <i class="material-icons text-secondary mr-3">fingerprint</i>
                <span>ID 12345679</span>
            </div>

            <!-- Profile menu -->
            <div class="dropdown profile-dropdown-wrap px-4">
                <a href="" class="profile-dropdown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="https://via.placeholder.com/38x38" class="rounded-circle mr-3" alt="Milena Dragisic">
                    <span class="user-name text-medium mr-3">Milena Dragisic</span>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Additional questions</a>
                    <a class="dropdown-item" href="#">Help</a>
                    <a class="dropdown-item" href="#">Privacy</a>
                </div>
            </div>

            <!-- Messages menu -->
            <div class="message-dropdown-wrap chat has-msg p-4">
                <a href="" class="message-dropdown dropdown-toggle d-flex align-items-center" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons">chat_bubble_outline</i>
                </a>
                <div class="message-dropdown-menu dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#">
                        <div>
                            <div class="avatar">
                                <span class="letter">B</span>
                            </div>
                        </div>
                        <div class="about">
                            <div class="role text-medium">Administrator Belana</div>
                            <div class="desc text-medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</div>
                        </div>
                    </a>
                    <a class="dropdown-item" href="#">
                        <div>
                            <div class="avatar">
                                <span class="letter">B</span>
                            </div>
                        </div>
                        <div class="about">
                            <div class="role text-medium">Administrator Belana</div>
                            <div class="desc text-medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</div>
                        </div>
                    </a>
                </div>
            </div>

            <!-- Sign-out / close button -->
            <div class="close-page">
                <a href=""><i class="material-icons md-24">power_settings_new</i></a>
            </div>
        </div>
    </div>
</nav>