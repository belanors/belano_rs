<div id="content-footer" class="d-flex justify-content-between align-items-center">
    <div><i class="fa fa-phone mr-2"></i> Call us <a href="tel:+381113677773" class="ml-2">+381 11 36 77 773</a></div>
    <div>Having problems with the web app? <a href="#" class="ml-2">Contact Us</a></div>
    <div>Copyright <a href="#" class="ml-2">Belano</a></div>
</div>