<aside id="dash-sidebar" class="sidebar-fixed position-fixed">
    <div class="content">
        <ul class="nav">
            <li class="nav-link">
                <a href="owners"><i class="material-icons">home</i></a>
            </li>
            <li class="nav-link">
                <a href="owners">
                    <i class="material-icons">assignment_ind</i>
                    <span class="nav-link-text">Profil</span>
                </a>
            </li>
            <li class="nav-link">
                <a href="owners-history">
                    <i class="material-icons">history</i>
                    <span class="nav-link-text">History</span>
                </a>
            </li>
            <li class="nav-link">
                <a href="owners-favorites">
                    <i class="material-icons">favorite</i>
                    <span class="nav-link-text">Favorites</span>
                </a>
            </li>
            <li class="nav-link">
                <a href="owners-card">
                    <i class="material-icons">credit_card</i>
                    <span class="nav-link-text">Card</span>
                </a>
            </li>
            <li class="nav-link">
                <a href="owners-comments">
                    <i class="material-icons">chat_bubble_outline</i>
                    <span class="nav-link-text">Comments</span>
                </a>
            </li>
            <li class="nav-link">
                <a href="owners-new-apartment">
                    <i class="material-icons">playlist_add</i>
                    <span class="nav-link-text">Add new apartment</span>
                </a>
            </li>
            <li class="nav-link download">
                <a href="#">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M19.35 10.04C18.67 6.59 15.64 4 12 4C9.11 4 6.6 5.64 5.35 8.04C2.34 8.36 0 10.91 0 14C0 17.31 2.69 20 6 20H19C21.76 20 24 17.76 24 15C24 12.36 21.95 10.22 19.35 10.04ZM17 13L12 18L7 13H10V9H14V13H17Z" fill="#D1DBE2"/>
                    </svg>

                    <span class="nav-link-text">Download. PDF</span>
                </a>
            </li>
        </ul>
        <div class="menu-trigger">
            <a href="" class="menu-open">
                <img src="../img/dashboard/Hamburger%20Menu.svg" alt="">
            </a>
            <a href="" class="menu-close">
                <img src="../img/dashboard/ic_close_black_24px.svg" alt="">
                <span>Close</span>
            </a>
        </div>
    </div>
</aside>
<script>
    $(document).ready(function(){
        var activePage = $(location).attr('pathname').substring( $(location).attr('pathname').lastIndexOf('/')+1);
        $('a[href="' + activePage + '"]').parent().addClass('is-active');
    });
</script>