<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Owners - Comments</title>

    <link rel="stylesheet" href="../fonts/productsans.css">
    <link rel="stylesheet" href="../fonts/helvetica.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-reboot.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.min.css">

    <!-- Material Design for Bootstrap + Material Icons library -->
    <link rel="stylesheet" href="../css/mdb.min.css">
    <link rel="stylesheet" href="../fonts/material-icons.css">

    <link rel="stylesheet" href="scss/main.css">
    <link rel="stylesheet" href="../css/styles.css">
    <link rel="stylesheet" href="js/font/font-fileuploader.css">

    <script src="../js/jquery.min.js"></script>

    <!-- FontAwesome Icons -->
    <link rel="stylesheet" href="../css/fontawesome-all.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- Leave those next 4 lines if you care about users using IE8 -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="dash-comments" class="dash-history">
<header>

    <!-- Top navigation -->
    <nav id="dash-nav" class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
        <div class="container-fluid p-0">
            <div class="navbar-left pl-4">

                <div class="logo">
                    <a href="../"><img src="img/header-logo.svg" alt="Belano.rs"></a>
                </div>

                <div class="page-title">Owners Dashboard</div>

                <!-- Language switcher -->
                <div class="lang">
                    <a href="#">Eng</a> / <a href="#">Srb</a>
                </div>

                <!-- Weather widget -->
                <div class="nav-weather">
                    <img src="../img/icon-weather.png" alt="">
                    <span>Belgrade 34F</span>
                </div>

            </div>

            <div class="navbar-right">
                <div class="paket">
                    <img src="../img/dashboard/flag-icon.png" alt="">
                    <span>Basic Paket</span>
                </div>
                <div class="user-id">
                    <i class="material-icons text-secondary mr-3">fingerprint</i>
                    <span>ID 12345679</span>
                </div>

                <!-- Profile menu -->
                <div class="dropdown profile-dropdown-wrap px-4">
                    <a href="" class="profile-dropdown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="https://via.placeholder.com/38x38" class="rounded-circle mr-3" alt="Milena Dragisic">
                        <span class="user-name text-medium mr-3">Milena Dragisic</span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Additional questions</a>
                        <a class="dropdown-item" href="#">Help</a>
                        <a class="dropdown-item" href="#">Privacy</a>
                    </div>
                </div>

                <!-- Messages menu -->
                <div class="message-dropdown-wrap chat has-msg p-4">
                    <a href="" class="message-dropdown dropdown-toggle d-flex align-items-center" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">chat_bubble_outline</i>
                    </a>
                    <div class="message-dropdown-menu dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">
                            <div>
                                <div class="avatar">
                                    <span class="letter">B</span>
                                </div>
                            </div>
                            <div class="about">
                                <div class="role text-medium">Administrator Belana</div>
                                <div class="desc text-medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</div>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#">
                            <div>
                                <div class="avatar">
                                    <span class="letter">B</span>
                                </div>
                            </div>
                            <div class="about">
                                <div class="role text-medium">Administrator Belana</div>
                                <div class="desc text-medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</div>
                            </div>
                        </a>
                    </div>
                </div>

                <!-- Sign-out / close button -->
                <div class="close-page">
                    <a href=""><i class="material-icons md-24">power_settings_new</i></a>
                </div>
            </div>
        </div>
    </nav>

    <!-- Sidebar navigation -->
    <aside id="dash-sidebar" class="sidebar-fixed position-fixed">
        <div class="content">
            <ul class="nav">
                <li class="nav-link">
                    <a href="owners"><i class="material-icons">home</i></a>
                </li>
                <li class="nav-link">
                    <a href="owners">
                        <i class="material-icons">assignment_ind</i>
                        <span class="nav-link-text">Profil</span>
                    </a>
                </li>
                <li class="nav-link">
                    <a href="owners-history">
                        <i class="material-icons">history</i>
                        <span class="nav-link-text">History</span>
                    </a>
                </li>
                <li class="nav-link">
                    <a href="owners-favorites">
                        <i class="material-icons">favorite</i>
                        <span class="nav-link-text">Favorites</span>
                    </a>
                </li>
                <li class="nav-link">
                    <a href="owners-card">
                        <i class="material-icons">credit_card</i>
                        <span class="nav-link-text">Card</span>
                    </a>
                </li>
                <li class="nav-link">
                    <a href="owners-comments">
                        <i class="material-icons">chat_bubble_outline</i>
                        <span class="nav-link-text">Comments</span>
                    </a>
                </li>
                <li class="nav-link">
                    <a href="owners-new-apartment">
                        <i class="material-icons">playlist_add</i>
                        <span class="nav-link-text">Add new apartment</span>
                    </a>
                </li>
                <li class="nav-link download">
                    <a href="#">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M19.35 10.04C18.67 6.59 15.64 4 12 4C9.11 4 6.6 5.64 5.35 8.04C2.34 8.36 0 10.91 0 14C0 17.31 2.69 20 6 20H19C21.76 20 24 17.76 24 15C24 12.36 21.95 10.22 19.35 10.04ZM17 13L12 18L7 13H10V9H14V13H17Z" fill="#D1DBE2"/>
                        </svg>

                        <span class="nav-link-text">Download. PDF</span>
                    </a>
                </li>
            </ul>
            <div class="menu-trigger">
                <a href="" class="menu-open">
                    <img src="../img/dashboard/Hamburger%20Menu.svg" alt="">
                </a>
                <a href="" class="menu-close">
                    <img src="../img/dashboard/ic_close_black_24px.svg" alt="">
                    <span>Close</span>
                </a>
            </div>
        </div>
    </aside>

</header>

<main id="dash-content">

    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12 card-info">
                <div class="panel-label">Comments</div>
                <div class="card">
                    <div class="card-body">
                        <table class="table table-history table-history-new-info new-messages table-comments">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="comment-avatar">M</div>
                                    </td>
                                    <td>
                                        <div class="td-value table-comment-name pb-2">Milena</div>
                                        <div class="td-value table-comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</div>
                                    </td>
                                    <td class="date-time">
                                        <div class="td-label">Date &amp; Time</div>
                                        <div class="td-value">21. 06. 2018</div>
                                    </td>
                                    <td class="status">
                                        <div class="td-label">Status</div>
                                        <div class="td-value">Read</div>
                                    </td>
                                    <td class="status">
                                        <div class="td-label p-0">
                                            <i class="material-icons">attach_file</i>
                                        </div>
                                    </td>
                                    <td class="read-more">
                                        <a href="#" class="btn btn-white no-shadow">Read more</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-history table-comments">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="comment-avatar">M</div>
                                    </td>
                                    <td>
                                        <div class="td-value table-comment-name pb-2">Milena</div>
                                        <div class="td-value table-comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</div>
                                    </td>
                                    <td class="date-time">
                                        <div class="td-label">Date &amp; Time</div>
                                        <div class="td-value">21. 06. 2018</div>
                                    </td>
                                    <td class="status">
                                        <div class="td-label">Status</div>
                                        <div class="td-value">Read</div>
                                    </td>
                                    <td class="attachment">
                                        <div class="td-label p-0">
                                            <a href="#"><i class="material-icons">attach_file</i></a>
                                        </div>
                                    </td>
                                    <td class="read-more">
                                        <a href="#" class="btn btn-white no-shadow btn-show-comment" data-comment-id="12">Read more</a>
                                    </td>
                                </tr>

                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dashboard footer -->
        <?php include('templates/footer.php'); ?>

    </div>

</main>

<!-- Read comment - frame -->
<div class="frame-container" id="comment-frame">
    <div class="container-fluid">
        <div class="row">

            <!-- Anchor Links -->
            <div class="inner-top-row">
                <ul id="left-submenu" class="justify-content-start">
                    <li>Komentari</li>
                    <li class="ml-auto">
                        <a href="#" type="button" class="btn-close-container" data-close-container="#comment-frame">Close</a>
                    </li>
                </ul>
            </div>
            
            <div class="comment-holder">

                <!-- Comment details -->
                <div class="col-6 comment-data">
                    <div class="comment-preview-head">
                        <div class="avatar-wrap">
                            <div class="comment-avatar">M</div>
                        </div>
                        <div>
                            <h6 class="comment-name">Milena Dragišić</h6>
                            <p class="text-lightblue">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</p>
                        </div>
                    </div>
                    <div class="comment-preview-content">
                        <p class="text-small text-lightblue">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <p class="text-small text-lightblue">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    </div>
                </div>

                <!-- Apart/Image details -->
                <div class="col-6 comment-apart">

                    <div class="caption">
                        <h3 class="h3-responsive">Living room</h3>
                        <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
                    </div>
                    <span class="apart-overlay"></span>
                    <img src="../img/main-slider-image.jpg" alt="Frida">
                    
                </div>
                
            </div>
            
        </div>
    </div>
</div>

<script>

    $(document).ready(function(){

        $('[data-toggle="tooltip"]').tooltip();
        $('.mdb-select').materialSelect();

        $('.big-checkbox input[type="checkbox"]').change(function () {
            var check_button = $(this, 'input').data('btn');
            if( $(this).prop('checked') == true ) {
                $(this).parent().addClass('checked');
                $(check_button).attr('disabled', false).addClass('enabled');
            }
            else {
                $(this).parent().removeClass('checked');
                $(check_button).attr('disabled', true);
            }
        });

        // delete row from table
        // TODO: get row/data ID
        $('.btn-delete-row').click(function(){
            $(this).parent().parent().fadeOut(); // fade out row
            setTimeout(function () { // remove element from DOM
                $(this).parent().parent().remove();
            }, 3000);

            console.log('action: delete row (with row id)');

        });

        $('.btn-close-container').click(function (e) {
            e.preventDefault();
            var container = $(this).data('close-container');
            console.log('close container: ' + container);

            $(container).removeClass('open');
            $('body').removeClass('frame-opened');

        })

        $('.btn-show-comment').click(function(e){
            e.preventDefault();

            var commentID = $(this).data('comment-id');
            console.log('show comment with ID: ' + commentID);

            $('#comment-frame').addClass('open');
            $('body').addClass('frame-opened');

        })

    });

</script>


    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script type="text/javascript" src="js/moment.js"></script>
    <script src="js/mdb.min.js"></script>
    <script src="js/scripts.min.js"></script>
    <script src="js/caleran.js"></script>
    <script src="js/bootstrap-select.js"></script>
    <script src="js/jquery-clockpicker.js"></script>
    <script src="js/main.js"></script>

</body>
</html>
