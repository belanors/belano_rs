<?php include('head-owners.php'); ?>

<body id="dash-users">
    <header>

        <!-- Top navigation -->
        <?php include('templates/topnav-owners.php'); ?>

        <!-- Sidebar navigation -->
        <?php include('templates/sidebar-owners.php'); ?>

    </header>

    <main id="dash-content">

        <!-- Profile block -->
        <div class="container-fluid p-0" id="dasboard-profile">
            <div class="row">
                <div class="col-4 profile-info">
                    <div class="panel-label">Profile Information</div>
                    <div class="card">
                        <div class="card-body">
                            <div class="user-info">
                                <div class="user-image has-edit">
                                    <img src="https://via.placeholder.com/135x135" alt="">
                                    <img class="edit-icon" src="../img/dashboard/ic_mode_edit_black_24px.png">
                                </div>
                                <div class="user-name">Milena Dragisic</div>
                                <span class="image-info">
                                Recommended to use<br>
                                JPEG, GIF or PNG, 150х150px, max 1MB
                                </span>
                            </div>

                            <form action="#" class="user-info-form w-100">

                                <div class="row d-flex">

                                    <div class="col-12">
                                        <div class="form-label-group mb-0 has-edit">
                                            <input type="text" name="user-name" id="user-name" class="form-control" required="required" placeholder="Fullname" value="Name">
                                            <label for="user-name">Fullname</label>
                                            <span class="edit-icon"></span>
                                        </div>
                                        <div class="form-label-group mb-0 has-edit">
                                            <input type="text" name="user-email" id="user-email" class="form-control" required="required" placeholder="Email *" value="email">
                                            <label for="user-email">Email</label>
                                            <span class="edit-icon"></span>
                                        </div>
                                        <div class="form-label-group mb-0 has-edit">
                                            <input type="text" name="user-mobile" id="user-mobile" class="form-control" required="required" placeholder="Subject">
                                            <label for="user-mobile">Mobile *</label>
                                            <span class="edit-icon"></span>
                                        </div>
                                        <div class="form-label-group mb-0 has-edit">
                                            <input type="text" name="user-mobile" id="user-address" class="form-control" required="required" placeholder="Subject">
                                            <label for="user-address">Address *</label>
                                            <span class="edit-icon"></span>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <div class="linked-acc">
                                <h4>Linked Accounts</h4>
                                <p class="text-medium text-lightblue">Manage the linked accounts which you have previously authorized to be used for logging into this website.</p>
                                <div class="buttons">
                                    <button type="button" class="btn"><i class="fab fa-google" aria-hidden="true"></i> Unlink</button>
                                    <button type="button" class="btn"><i class="fab fa-facebook-f pr-1"></i> Unlink</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="row mb-4">

                        <!-- Recent comments -->
                        <div class="col-8 comment-notification">
                            <div class="panel-label">Comments Notifications</div>
                            <div class="card">
                                <div class="card-body">

                                    <a href="owners-comments" class="btn btn-link">See more</a>

                                    <ul class="dash-comments">
                                        <li>
                                            <div>
                                                <div class="comment-avatar">M</div>
                                            </div>
                                            <div class="comment-wrap">
                                                <span>Milena</span>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div>
                                                <div class="comment-avatar">M</div>
                                            </div>
                                            <div class="comment-wrap">
                                                <span>Milena</span>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                            </div>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="col-4 loyalty">
                            <div class="panel-label">Loyalty</div>
                            <div class="card">
                                <div class="card-body">
                                    <h5>Basic Paket</h5>
                                    <p class="pass-code">pass code</p>
                                    <img src="../img/dashboard/finger-print.png" alt="">
                                    <div class="id">ID 1245897</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Creditcard Information -->
                    <div class="row">

                        <div class="col-12 card-info">
                            <div class="panel-label">Credit Card Information</div>
                            <div class="card">
                                <div class="card-body">

                                    <!-- Creditcard list wrapper -->
                                    <div class="accordion md-accordion" id="accordionEx1" role="tablist" aria-multiselectable="true">

                                        <!-- Accordion card -->
                                        <div class="card">

                                            <!-- Card header -->
                                            <div class="card-header" role="tab" id="headingTwo1">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo1"
                                                   aria-expanded="false" aria-controls="collapseTwo1">
                                                    <div class="d-flex justify-content-between">
                                                        <div class="d-flex align-items-center">
                                                            <div class="name">
                                                                <div class="full-name-label">Full Name</div>
                                                                <div class="full-name">Milena Dragic</div>
                                                            </div>
                                                            <div class="card-type">
                                                                <img src="../img/dashboard/master-card.png" alt="">
                                                            </div>
                                                            <div class="card-num">
                                                                <div class="card-num-label">Credit card</div>
                                                                <div class="card-num-num">3845 *** ***</div>
                                                            </div>
                                                            <div class="card-default-card">
                                                                <div class="form-check">
                                                                    <input type="radio" class="form-check-input" id="profile-default-card" name="profile-default-card">
                                                                    <label class="form-check-label" for="profile-default-card">Default Creditcard</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center">
                                                            <div class="card-icon">
                                                                <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M20 4.76367H4C2.89 4.76367 2.01 5.65367 2.01 6.76367L2 18.7637C2 19.8737 2.89 20.7637 4 20.7637H20C21.11 20.7637 22 19.8737 22 18.7637V6.76367C22 5.65367 21.11 4.76367 20 4.76367ZM20 18.7637H4V12.7637H20V18.7637ZM20 8.76367H4V6.76367H20V8.76367Z" fill="rgb(255, 255, 255 ,0.5)"/>
                                                                </svg>

                                                            </div>
                                                            <div class="delete-icon">
                                                                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M6.66211 19.5723C6.66211 20.6723 7.56211 21.5723 8.66211 21.5723H16.6621C17.7621 21.5723 18.6621 20.6723 18.6621 19.5723V7.57227H6.66211V19.5723ZM9.12211 12.4523L10.5321 11.0423L12.6621 13.1623L14.7821 11.0423L16.1921 12.4523L14.0721 14.5723L16.1921 16.6923L14.7821 18.1023L12.6621 15.9823L10.5421 18.1023L9.13211 16.6923L11.2521 14.5723L9.12211 12.4523ZM16.1621 4.57227L15.1621 3.57227H10.1621L9.16211 4.57227H5.66211V6.57227H19.6621V4.57227H16.1621Z"/>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </a>
                                            </div>

                                            <!-- Card body -->
                                            <div id="collapseTwo1" class="collapse show" role="tabpanel" aria-labelledby="headingTwo1"
                                                 data-parent="#accordionEx1">
                                                <div class="card-body">
                                                    <div class="row d-flex align-items-center">
                                                        <div class="col-6">
                                                            <div class="d-flex justify-content-between align-items-center cc-graphic">
                                                                <div class="col-md-5 text-center">
                                                                    <img src="../img/image-mastercard.svg" alt="MasterCard">
                                                                </div>
                                                                <div class="col-md-7 p-0">
                                                                    <img src="../img/image-creditcard.png" alt="CreditCard" class="w-100">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <form action="" class="w-100" id="input-creditcard">
                                                                <div class="form-label-group mb-0 input-group-prepend has-edit">
                                                                    <i class="far fa-credit-card"></i>
                                                                    <input type="text" id="booking-cc-numb" name="booking-cc-numb" class="form-control w-100 with-shadow border-bottom-0" placeholder="Card Number">
                                                                    <label for="booking-cc-numb">Card Information</label>
                                                                    <span class="edit-icon"></span>
                                                                </div>
                                                                <div class="d-flex">
                                                                    <div class="col-md-6 col-12 p-0">
                                                                        <div class="form-label-group m-0 has-edit">
                                                                            <input type="text" class="form-control with-shadow border-right-0" id="booking-cc-name" placeholder="Card Holder">
                                                                            <label for="booking-cc-name">Card Holder</label>
                                                                            <span class="edit-icon"></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-6 p-0">
                                                                        <div class="form-label-group m-0 has-edit">
                                                                            <input type="text" class="form-control with-shadow border-right-0" id="booking-cc-date" placeholder="Exp. Date">
                                                                            <label for="booking-cc-date">Exp. Date</label>
                                                                            <span class="edit-icon"></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-6 p-0">
                                                                        <div class="form-label-group m-0 has-edit">
                                                                            <input type="text" class="form-control with-shadow" id="booking-cc-date" placeholder="CVC">
                                                                            <label for="booking-cc-date">CVC</label>
                                                                            <span class="edit-icon"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <!-- Accordion card -->
                                        <div class="card">

                                            <!-- Card header -->
                                            <div class="card-header" role="tab" id="headingTwo1">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo1"
                                                   aria-expanded="false" aria-controls="collapseTwo1">
                                                    <div class="d-flex justify-content-between">
                                                        <div class="d-flex align-items-center">
                                                            <div class="name">
                                                                <div class="full-name-label">Full Name</div>
                                                                <div class="full-name">Milena Dragic</div>
                                                            </div>
                                                            <div class="card-type">
                                                                <img src="../img/dashboard/master-card.png" alt="">
                                                            </div>
                                                            <div class="card-num">
                                                                <div class="card-num-label">Credit card</div>
                                                                <div class="card-num-num">3845 *** ***</div>
                                                            </div>
                                                        </div>
                                                        <div class="d-flex align-items-center">
                                                            <div class="card-icon">
                                                                <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M20 4.76367H4C2.89 4.76367 2.01 5.65367 2.01 6.76367L2 18.7637C2 19.8737 2.89 20.7637 4 20.7637H20C21.11 20.7637 22 19.8737 22 18.7637V6.76367C22 5.65367 21.11 4.76367 20 4.76367ZM20 18.7637H4V12.7637H20V18.7637ZM20 8.76367H4V6.76367H20V8.76367Z"/>
                                                                </svg>
                                                            </div>
                                                            <div class="delete-icon">
                                                                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M6.66211 19.5723C6.66211 20.6723 7.56211 21.5723 8.66211 21.5723H16.6621C17.7621 21.5723 18.6621 20.6723 18.6621 19.5723V7.57227H6.66211V19.5723ZM9.12211 12.4523L10.5321 11.0423L12.6621 13.1623L14.7821 11.0423L16.1921 12.4523L14.0721 14.5723L16.1921 16.6923L14.7821 18.1023L12.6621 15.9823L10.5421 18.1023L9.13211 16.6923L11.2521 14.5723L9.12211 12.4523ZM16.1621 4.57227L15.1621 3.57227H10.1621L9.16211 4.57227H5.66211V6.57227H19.6621V4.57227H16.1621Z"/>
                                                                </svg>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </a>
                                            </div>

                                            <!-- Card body -->
                                            <div id="collapseTwo1" class="collapse" role="tabpanel" aria-labelledby="headingTwo1"
                                                 data-parent="#accordionEx1">
                                                <div class="card-body">
                                                    <div class="row d-flex align-items-center">
                                                        <div class="col-6">
                                                            <div class="d-flex justify-content-between align-items-center cc-graphic">
                                                                <div class="col-md-5 text-center">
                                                                    <img src="../img/image-mastercard.svg" alt="MasterCard">
                                                                </div>
                                                                <div class="col-md-7 p-0">
                                                                    <img src="../img/image-creditcard.png" alt="CreditCard" class="w-100">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <form action="" class="w-100" id="input-creditcard">
                                                                <div class="form-label-group mb-0 input-group-prepend">
                                                                    <i class="far fa-credit-card"></i>
                                                                    <input type="text" id="booking-cc-numb" name="booking-cc-numb" class="form-control w-100 with-shadow border-bottom-0" placeholder="Card Number">
                                                                    <label for="booking-cc-numb">Card Information</label>
                                                                </div>
                                                                <div class="d-flex">
                                                                    <div class="col-md-6 col-12 p-0">
                                                                        <div class="form-label-group m-0">
                                                                            <input type="text" class="form-control with-shadow border-right-0" id="booking-cc-name" placeholder="Card Holder">
                                                                            <label for="booking-cc-name">Card Holder</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-6 p-0">
                                                                        <div class="form-label-group m-0">
                                                                            <input type="text" class="form-control with-shadow border-right-0" id="booking-cc-date" placeholder="Exp. Date">
                                                                            <label for="booking-cc-date">Exp. Date</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-6 p-0">
                                                                        <div class="form-label-group m-0">
                                                                            <input type="text" class="form-control with-shadow" id="booking-cc-date" placeholder="CVC">
                                                                            <label for="booking-cc-date">CVC</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Accordion card -->

                                    </div>
                                    <!-- Accordion wrapper -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="content-footer" class="d-flex justify-content-between align-items-center">
                <div><i class="fa fa-phone mr-2"></i> Call us <a href="" class="ml-2">Number</a></div>
                <div>Having problems with the web app? <a href="" class="ml-2">Contact Us</a></div>
                <div>Copyright <a href="" class="ml-2">Belano</a></div>
            </div>
        </div>

    </main>

    <?php include('templates/bottom-includes.php'); ?>

</body>
</html>
