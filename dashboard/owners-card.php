<?php include('head-owners.php'); ?>

<body id="dash-users" class="dash-history">
<header>

    <!-- Top navigation -->
    <?php include('templates/topnav-owners.php'); ?>

    <!-- Sidebar navigation -->
    <?php include('templates/sidebar-owners.php'); ?>

    <div id="history-nav">
        <div class="history-nav-link">
            <a href="">Favorites</a>
        </div>
        <div class="history-nav-link">
            <a href="">Last Information</a>
        </div>
    </div>

</header>

<main id="dash-content">

    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-6 card-info">
                <div class="panel-label">Last information</div>
                <div class="card">
                    <div class="card-body">
                        <div class="search-download d-flex justify-content-end align-items-center">
                            <i class="fa fa-search mr-3"></i>
                            <i class="fa fa-cloud-download-alt"></i>
                        </div>
                        <!--Accordion wrapper-->
                        <div class="accordion md-accordion" id="accordionEx1" role="tablist" aria-multiselectable="true">

                            <!-- Accordion card -->
                            <div class="card">
                                <!-- Card header -->
                                <div class="card-header" role="tab" id="headingTwo1">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo1"
                                       aria-expanded="false" aria-controls="collapseTwo1">
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">
                                                <div class="name">
                                                    <div class="full-name-label">Full Name</div>
                                                    <div class="full-name">Milena Dragic</div>
                                                </div>
                                                <div class="card-type">
                                                    <img src="../img/dashboard/master-card.png" alt="">
                                                </div>
                                                <div class="card-num">
                                                    <div class="card-num-label">Credit card</div>
                                                    <div class="card-num-num">3845 *** ***</div>
                                                </div>
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <div class="delete-icon">
                                                    <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M6.66211 19.5723C6.66211 20.6723 7.56211 21.5723 8.66211 21.5723H16.6621C17.7621 21.5723 18.6621 20.6723 18.6621 19.5723V7.57227H6.66211V19.5723ZM9.12211 12.4523L10.5321 11.0423L12.6621 13.1623L14.7821 11.0423L16.1921 12.4523L14.0721 14.5723L16.1921 16.6923L14.7821 18.1023L12.6621 15.9823L10.5421 18.1023L9.13211 16.6923L11.2521 14.5723L9.12211 12.4523ZM16.1621 4.57227L15.1621 3.57227H10.1621L9.16211 4.57227H5.66211V6.57227H19.6621V4.57227H16.1621Z"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>

                                    </a>
                                </div>

                                <!-- Card body -->
                                <div id="collapseTwo1" class="collapse show" role="tabpanel" aria-labelledby="headingTwo1"
                                     data-parent="#accordionEx1">
                                    <div class="card-body">
                                        <div class="row d-flex align-items-center justify-content-center">

                                            <div class="col-10">
                                                <form action="" class="w-100" id="input-creditcard">
                                                    <div class="form-label-group mb-0 input-group-prepend has-edit">
                                                        <i class="far fa-credit-card"></i>
                                                        <input type="text" id="booking-cc-numb" name="booking-cc-numb" class="form-control w-100 with-shadow border-bottom-0" placeholder="Card Number">
                                                        <label for="booking-cc-numb">Card Information</label>
                                                        <span class="edit-icon"></span>
                                                    </div>
                                                    <div class="d-flex">
                                                        <div class="col-md-6 col-12 p-0">
                                                            <div class="form-label-group m-0 has-edit">
                                                                <input type="text" class="form-control with-shadow border-right-0" id="booking-cc-name" placeholder="Card Holder">
                                                                <label for="booking-cc-name">Card Holder</label>
                                                                <span class="edit-icon"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-6 p-0">
                                                            <div class="form-label-group m-0 has-edit">
                                                                <input type="text" class="form-control with-shadow border-right-0" id="booking-cc-date" placeholder="Exp. Date">
                                                                <label for="booking-cc-date">Exp. Date</label>
                                                                <span class="edit-icon"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-6 p-0">
                                                            <div class="form-label-group m-0 has-edit">
                                                                <input type="text" class="form-control with-shadow" id="booking-cc-date" placeholder="CVC">
                                                                <label for="booking-cc-date">CVC</label>
                                                                <span class="edit-icon"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- Accordion card -->
                            <div class="card">

                                <!-- Card header -->
                                <div class="card-header" role="tab" id="headingTwo1">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx1" href="#collapseTwo1" aria-expanded="false" aria-controls="collapseTwo1">
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">
                                                <div class="name">
                                                    <div class="full-name-label">Full Name</div>
                                                    <div class="full-name">Milena Dragic</div>
                                                </div>
                                                <div class="card-type">
                                                    <img src="../img/dashboard/master-card.png" alt="">
                                                </div>
                                                <div class="card-num">
                                                    <div class="card-num-label">Credit card</div>
                                                    <div class="card-num-num">3845 *** ***</div>
                                                </div>
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <div class="delete-icon">
                                                    <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M6.66211 19.5723C6.66211 20.6723 7.56211 21.5723 8.66211 21.5723H16.6621C17.7621 21.5723 18.6621 20.6723 18.6621 19.5723V7.57227H6.66211V19.5723ZM9.12211 12.4523L10.5321 11.0423L12.6621 13.1623L14.7821 11.0423L16.1921 12.4523L14.0721 14.5723L16.1921 16.6923L14.7821 18.1023L12.6621 15.9823L10.5421 18.1023L9.13211 16.6923L11.2521 14.5723L9.12211 12.4523ZM16.1621 4.57227L15.1621 3.57227H10.1621L9.16211 4.57227H5.66211V6.57227H19.6621V4.57227H16.1621Z"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>

                                    </a>
                                </div>

                                <!-- Card body -->
                                <div id="collapseTwo1" class="collapse" role="tabpanel" aria-labelledby="headingTwo1"
                                     data-parent="#accordionEx1">
                                    <div class="card-body">
                                        <div class="row d-flex align-items-center justify-content-center">
                                            <div class="col-10">
                                                <form action="" class="w-100" id="input-creditcard">
                                                    <div class="form-label-group mb-0 input-group-prepend">
                                                        <i class="far fa-credit-card"></i>
                                                        <input type="text" id="booking-cc-numb" name="booking-cc-numb" class="form-control w-100 with-shadow border-bottom-0" placeholder="Card Number">
                                                        <label for="booking-cc-numb">Card Information</label>
                                                    </div>
                                                    <div class="d-flex">
                                                        <div class="col-md-6 col-12 p-0">
                                                            <div class="form-label-group m-0">
                                                                <input type="text" class="form-control with-shadow border-right-0" id="booking-cc-name" placeholder="Card Holder">
                                                                <label for="booking-cc-name">Card Holder</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-6 p-0">
                                                            <div class="form-label-group m-0">
                                                                <input type="text" class="form-control with-shadow border-right-0" id="booking-cc-date" placeholder="Exp. Date">
                                                                <label for="booking-cc-date">Exp. Date</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-6 p-0">
                                                            <div class="form-label-group m-0">
                                                                <input type="text" class="form-control with-shadow" id="booking-cc-date" placeholder="CVC">
                                                                <label for="booking-cc-date">CVC</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="load-more"><span>Load More + 16</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-6 payment">
                <div class="panel-label">Card</div>
                <div class="card">
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-10">

                                <div class="mb-5">
                                    <h3 class="title">Payment Card process</h3>
                                    <p class="desc">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
                                </div>

                                <div class="d-flex justify-content-between align-items-center cc-graphic mb-4">
                                    <div class="col-md-5">
                                        <img src="../img/image-mastercard.svg" alt="MasterCard">
                                    </div>
                                    <div class="col-md-7 p-0">
                                        <img src="../img/image-creditcard.png" alt="CreditCard" class="w-100">
                                    </div>
                                </div>
                                <form action="" class="w-100" id="input-creditcard">
                                    <div class="form-label-group mb-0 input-group-prepend has-edit">
                                        <i class="far fa-credit-card"></i>
                                        <input type="text" id="booking-cc-numb" name="booking-cc-numb" class="form-control w-100 with-shadow border-bottom-0" placeholder="Card Number">
                                        <label for="booking-cc-numb">Card Information</label>
                                        <span class="edit-icon"></span>
                                    </div>

                                    <div class="d-flex mb-4">
                                        <div class="col-md-6 col-12 p-0">
                                            <div class="form-label-group m-0 has-edit">
                                                <input type="text" class="form-control with-shadow border-right-0" id="booking-cc-name" placeholder="Card Holder">
                                                <label for="booking-cc-name">Card Holder</label>
                                                <span class="edit-icon"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6 p-0">
                                            <div class="form-label-group m-0 has-edit">
                                                <input type="text" class="form-control with-shadow border-right-0" id="booking-cc-date" placeholder="Exp. Date">
                                                <label for="booking-cc-date">Exp. Date</label>
                                                <span class="edit-icon"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6 p-0">
                                            <div class="form-label-group m-0 has-edit">
                                                <input type="text" class="form-control with-shadow" id="booking-cc-date" placeholder="CVC">
                                                <label for="booking-cc-date">CVC</label>
                                                <span class="edit-icon"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="big-checkbox-wrapper">
                                        <div class="col-md-9 col-9 p-0">
                                            <div class="custom-control big-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="booking-cc-confirm" data-btn="#btn-cc-confirm">
                                                <label class="custom-control-label" for="booking-cc-confirm">Do you agree with your information</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 p-0">
                                            <button type="submit" class="btn btn-lg btn-secondary m-0 w-100 h-100 shadow-none enabled" id="btn-cc-confirm" disabled="disabled">All in</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Dashboard footer -->
        <div id="content-footer" class="d-flex justify-content-between align-items-center">
            <div><i class="fa fa-phone mr-2"></i> Call us <a href="tel:+381113677773" class="ml-2">+381 11 36 77 773</a></div>
            <div>Having problems with the web app? <a href="#" class="ml-2">Contact Us</a></div>
            <div>Copyright <a href="#" class="ml-2">Belano</a></div>
        </div>

    </div>

</main>

<script>

    $(document).ready(function(){

        $('[data-toggle="tooltip"]').tooltip();
        $('.mdb-select').materialSelect();

        $('.big-checkbox input[type="checkbox"]').change(function () {
            var check_button = $(this, 'input').data('btn');
            if( $(this).prop('checked') == true ) {
                $(this).parent().addClass('checked');
                $(check_button).attr('disabled', false).addClass('enabled');
            }
            else {
                $(this).parent().removeClass('checked');
                $(check_button).attr('disabled', true);
            }
        });

        // Make readonly input ediable
        $('.btn-enable-edit').click(function(e){
            e.preventDefault();
            $(this).parent().find('input').attr('readonly', false).focus();
        });

        // payment options
        $('.edit-payment-deposit, .edit-payment-option').change(function () {
            var check_button = $(this, 'input').data('btn');
            if( $(this).prop('checked') == true ) {
                $(this).parent().addClass('checked');
            }
            else {
                $(this).parent().removeClass('checked');
            }
        });

    });

</script>

<?php include('templates/bottom-includes.php'); ?>

</body>
</html>
