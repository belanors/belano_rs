<?php
    $page_title = 'Belano.rs - All apartments';
    include('head.php');
?>

<body id="page-apartments">

<?php include('header.php'); ?>

<div class="main-container">

    <?php include('templates/page-preloader.php'); ?>

    <!-- HTML Block -->
    <section id="intro-text" class="bg-white">
        <div class="container-fluid py-md-6 p-3">
            <div class="d-flex align-items-center flex-md-row flex-lg-row flex-column-reverse">
                <div class="col-12 col-sm-6 col-md-6 text-left p-0 px-5">
                    <h1 class="title-big red-color mb-3">Apartman Beograd centar</h1>
                    <p class="text-medium text-center text-md-left">Apartmani u centru Beograda su atraktivni i veoma traženi. Veliki broj apartmana u centru Beograda se nalazi u ponudi agencije Belano. Nudimo lux apartmanski smeštaj u centru Beograda klijentima kojima su prijatan i uredan prostor, blizina glavnih saobraćajnica, vlastiti mir i spokoj prioriteti prilikom odabira smeštaja. Jasno je da su apartmani u centru Beograda privilegija koju svako želi. Zato pogledajte našu ponudu apartmana u centru Beograda ili nas kontaktirajte i mi ćemo vam pomoći da odaberete idealan smeštaj u centru Beograda!</p>
                </div>
                <div class="col-12 col-md-5 text-right img-max-width">
                    <img src="img/image-apartments-static.png" alt="">
                </div>
            </div>
        </div>
    </section>

    <!-- Apartments slider -->
    <section id="apart-slider" class="bg-grey">
        <div class="container-fluid py-4">

            <!-- Apartments slider -->
            <div class="apart-slider-wrapper" id="apartments-slider">
                <div class="as-item centar">
                    <span>explore</span>
                    <h4 class="as-title">Apartmani Centar</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                </div>
                <div class="as-item slavija">
                    <span>explore</span>
                    <h4 class="as-title">Apartmani Centar</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                </div>
                <div class="as-item centar">
                    <span>explore</span>
                    <h4 class="as-title">Apartmani Centar</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                </div>
                <div class="as-item slavija">
                    <span>explore</span>
                    <h4 class="as-title">Apartmani Centar</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                </div>
                <div class="as-item centar">
                    <span>explore</span>
                    <h4 class="as-title">Apartmani Centar</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                </div>
                <div class="as-item slavija">
                    <span>explore</span>
                    <h4 class="as-title">Apartmani Centar</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                </div>
                <div class="as-item centar">
                    <span>explore</span>
                    <h4 class="as-title">Apartmani Centar</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                </div>
                <div class="as-item slavija">
                    <span>explore</span>
                    <h4 class="as-title">Apartmani Centar</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                </div>
                <div class="as-item centar">
                    <span>explore</span>
                    <h4 class="as-title">Apartmani Centar</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem.</p>
                    <a href="single-apartmant" class="btn-viewmore">More <span></span></a>
                </div>
            </div>
        </div>
    </section>

    <!-- Wide apartment banner -->
    <section id="apart-wide-banner">

        <div class="d-flex apart-wide">
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 bg-darkblue">

                <div class="ms-heading">
                    <div class="ms-price-wrap">
                        <p class="medium-label">Pay per night</p>
                        <div class="ms-price">25 €</div>
                    </div>
                    <h1 class="ms-title">twin violet</h1>
                </div>

                <div class="ms-info">
                    <ul class="ms-rating">
                        <li class="rated-star"><i class="fa fa-star"></i></li>
                        <li class="rated-star"><i class="fa fa-star"></i></li>
                        <li class="rated-star"><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                    </ul>
                    <p class="mb-4 ms-area">Nova Karaburma</p>
                    <p class="ms-desc">Lorem Ipsum is simply dummy text of the <br> printing and typesetting industry. Lorem <br>Ipsum has been.</p>

                    <a href="#" class="btn-viewmore light" tabindex="0">View more <span></span></a>

                    <ul class="apart-features light mt-5">
                        <li class="fe-parking"></li>
                        <li class="fe-location"></li>
                        <li class="fe-phone"></li>
                        <li class="fe-bookmark"></li>
                    </ul>

                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 ms-right">
                <div class="apart-wide-img">
                    <img src="img/image-apart-wide.jpg" alt="Twin Violet">
                </div>
            </div>
        </div>

    </section>

    <!-- Apartments grid + Tabs -->
    <section id="apart-grid">
        <div class="container-fluid">

            <div class="row">
                <ul class="nav nav-tabs custom-tabs" id="apart-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" id="apart-list-lux" data-toggle="tab" href="#apart-list-lux" role="tab" aria-controls="apart-list-lux" aria-selected="true">Lux apartmani</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="apart-list-day" data-toggle="tab" href="#apart-list-lux" role="tab" aria-controls="apart-list-lux" aria-selected="true">Apartmants for day</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="apart-list-day2" data-toggle="tab" href="#apart-list-lux" role="tab" aria-controls="apart-list-lux" aria-selected="true">Apartmants for day</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="apart-list-day3" data-toggle="tab" href="#apart-list-lux" role="tab" aria-controls="apart-list-lux" aria-selected="true">Apartmants for day</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="apart-list-day4" data-toggle="tab" href="#apart-list-lux" role="tab" aria-controls="apart-list-lux" aria-selected="true">Apartmants for day</a>
                    </li>
                </ul>
            </div>

            <!-- Apartment listing -->
            <div class="apart-items row justify-content-between">

                <!-- One big apartments -->
                <div class="col-12 col-sm-12 col-lg-6">
                    <?php include('templates/apart-big.php'); ?>
                </div>

                <!-- Listing 4 grid items -->
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-0">
                    <div class="row flex-row m-0">
                        <?php include('templates/apart-small.php'); ?>
                        <?php include('templates/apart-small.php'); ?>
                        <?php include('templates/apart-small.php'); ?>
                        <?php include('templates/apart-small.php'); ?>
                    </div>
                </div>

            </div>

            <div class="row text-center bottom-border mt-4 px-5 pb-5">
                <a href="#" class="btn-load-more">Load +50 More
                    <p class="text-blue mt-2 mb-0">If you are looking to reyervisete the cart, choose the car <br>and enter your information</p>
                </a>
            </div>

        </div>
    </section>

    <!-- One big apartment -->
    <section id="apart-full" class="bg-grey">

        <div class="container-fluid px-5">
            <div class="row">
                <div class="section-title-holder">
                    <h3 class="section-title">Name input</h3>
                    <p class="section-subtitle">If you are looking to reyervisete the cart, choose the
                        <br>car and enter your information
                    </p>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12 apart-item wide p-0">
                    <div class="apart-image">
                        <div class="apart-overlay"></div>
                        <img src="img/apartman_main_image.jpg" alt="">
                        <div class="floating-info">
                            <span class="labels label-new mb-4">New</span>
                            <p class="medium-label">Pay per night</p>
                            <p class="apart-item-price">25 €</p>
                            <h4 class="apart-item-title text-white">Studio Margarita 33M2</h4>
                            <ul class="rate-stars light">
                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="apart-item-bottom">
                        <p class="apart-area">Nova Karaburma</p>
                        <ul class="apart-labels">
                            <li>Entire apartment</li>
                            <li>Free cancellation</li>
                        </ul>
                        <ul class="apart-features">
                            <li class="fe-parking"></li>
                            <li class="fe-location"></li>
                            <li class="fe-phone"></li>
                            <li class="fe-bookmark"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Apartments grid + Tabs - reversed -->
    <section id="apart-grid">
        <div class="container-fluid">

            <!-- Apartment listing -->
            <div class="apart-items row justify-content-between">

                <!-- Listing 4 grid items -->
                <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-0">
                    <div class="row flex-row m-0">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 apart-item small">

                            <div class="apart-image with-slider">

                                <span class="apart-overlay"></span>

                                <div class="apart-slider">
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                </div>

                                <div class="apart-item-info floating-info">
                                    <div class="price-wrap">
                                        <p class="medium-label">Pay per night</p>
                                        <p class="apart-item-price">25 €</p>
                                    </div>
                                    <div class="apart-title-wrap">
                                        <h4 class="apart-item-title">Studio Margarita 33M2</h4>
                                        <div class="d-flex justify-content-between">

                                            <ul class="rate-stars light">
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                                <div class="slick-controls"></div>

                            </div>

                            <div class="apart-item-bottom">
                                <p class="apart-area">Nova Karaburma</p>
                                <ul class="apart-labels">
                                    <li>Entire apartment</li>
                                    <li>Free cancellation</li>
                                </ul>
                                <ul class="apart-features">
                                    <li class="fe-parking"></li>
                                    <li class="fe-location"></li>
                                    <li class="fe-phone"></li>
                                    <li class="fe-bookmark"></li>
                                </ul>
                            </div>

                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 apart-item small">

                            <div class="apart-image with-slider">

                                <div class="promo-badge">
                                    <div class="mr-4"><img src="img/icon-ringing.svg" class="icon-svg" alt=""></div>
                                    <div class="mr-4">Shocking price</div>
                                    <div>Pay for 1 hour</div>
                                    <div class="promo-price ml-auto">15€</div>
                                </div>

                                <span class="apart-overlay"></span>

                                <div class="apart-slider">
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                </div>

                                <div class="apart-item-info floating-info">
                                    <div class="price-wrap">
                                        <p class="medium-label">Pay per night</p>
                                        <p class="apart-item-price">25 €</p>
                                    </div>
                                    <div class="apart-title-wrap">
                                        <h4 class="apart-item-title">Studio Margarita 33M2</h4>
                                        <div class="d-flex justify-content-between">

                                            <ul class="rate-stars light">
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                                <div class="slick-controls">

                                </div>

                            </div>

                            <div class="apart-item-bottom">
                                <p class="apart-area">Nova Karaburma</p>
                                <ul class="apart-labels">
                                    <li>Entire apartment</li>
                                    <li>Free cancellation</li>
                                </ul>
                                <ul class="apart-features">
                                    <li class="fe-parking"></li>
                                    <li class="fe-location"></li>
                                    <li class="fe-phone"></li>
                                    <li class="fe-bookmark"></li>
                                </ul>
                            </div>

                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 apart-item small">

                            <div class="apart-image with-slider">

                                <span class="apart-overlay"></span>

                                <div class="apart-slider">
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                    <div class="">
                                        <img src="img/apartman_small_items.jpg" alt="">
                                    </div>
                                </div>

                                <div class="apart-item-info floating-info">
                                    <div class="price-wrap">
                                        <p class="medium-label">Pay per night</p>
                                        <p class="apart-item-price">25 €</p>
                                    </div>
                                    <div class="apart-title-wrap">
                                        <h4 class="apart-item-title">Studio Margarita 33M2</h4>
                                        <div class="d-flex justify-content-between">

                                            <ul class="rate-stars light">
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                                <div class="slick-controls"></div>

                            </div>

                            <div class="apart-item-bottom">
                                <p class="apart-area">Nova Karaburma</p>
                                <ul class="apart-labels">
                                    <li>Entire apartment</li>
                                    <li>Free cancellation</li>
                                </ul>
                                <ul class="apart-features">
                                    <li class="fe-parking"></li>
                                    <li class="fe-location"></li>
                                    <li class="fe-phone"></li>
                                    <li class="fe-bookmark"></li>
                                </ul>
                            </div>

                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 apart-item small">

                            <div class="apart-image">

                                <span class="apart-overlay"></span>

                                <div class="apart-image">
                                    <img src="img/apartman_small_items.jpg" alt="">
                                </div>

                                <div class="apart-item-info floating-info">
                                    <div class="price-wrap">
                                        <p class="medium-label">Pay per night</p>
                                        <p class="apart-item-price">25 €</p>
                                    </div>
                                    <div class="apart-title-wrap">
                                        <h4 class="apart-item-title">Studio Margarita 33M2</h4>
                                        <div class="d-flex justify-content-between">

                                            <ul class="rate-stars light">
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li class="rated-star"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="apart-item-bottom">
                                <p class="apart-area">Nova Karaburma</p>
                                <ul class="apart-labels">
                                    <li>Entire apartment</li>
                                    <li>Free cancellation</li>
                                </ul>
                                <ul class="apart-features">
                                    <li class="fe-parking"></li>
                                    <li class="fe-location"></li>
                                    <li class="fe-phone"></li>
                                    <li class="fe-bookmark"></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- One big apartments -->
                <div class="col-12 col-sm-12 col-lg-6 p-0">
                    <div class="apart-item big">
                        <div class="apart-image with-slider">

                            <div class="promo-badge">
                                <div class="mr-4"><img src="img/icon-ringing.svg" class="icon-svg" alt=""></div>
                                <div class="mr-4">Shocking price</div>
                                <div>Pay for 1 hour</div>
                                <div class="promo-price ml-auto">15€</div>
                            </div>

                            <span class="apart-overlay"></span>

                            <div class="apart-slider">
                                <div class="">
                                    <img src="img/apartman_main_image.jpg" alt="">
                                </div>
                                <div class="">
                                    <img src="img/apartman_main_image.jpg" alt="">
                                </div>
                                <div class="">
                                    <img src="img/apartman_main_image.jpg" alt="">
                                </div>
                            </div>

                            <div class="apart-item-info floating-info">
                                <span class="labels label-new mb-4">New</span>
                                <div class="price-wrap">
                                    <p class="medium-label">Pay per night</p>
                                    <p class="apart-item-price">25 €</p>
                                </div>
                                <div class="apart-title-wrap">
                                    <h4 class="apart-item-title">Studio Margarita 33M2</h4>
                                    <div class="d-flex justify-content-between">

                                        <ul class="rate-stars light">
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li class="rated-star"><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>

                            <div class="slick-controls"></div>

                        </div>

                        <div class="apart-item-bottom">
                            <p class="apart-area">Nova Karaburma</p>
                            <ul class="apart-labels">
                                <li>Entire apartment</li>
                                <li>Free cancellation</li>
                            </ul>
                            <ul class="apart-features">
                                <li class="fe-parking"></li>
                                <li class="fe-location"></li>
                                <li class="fe-phone"></li>
                                <li class="fe-bookmark"></li>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>

            <!-- Load More apartmants -->
            <div class="row text-left bottom-border mt-4 px-5 pb-5">
                <a href="#" class="btn-load-more">Load +50 More
                    <p class="text-blue mt-2 mb-0">If you are looking to reyervisete the cart, choose the car <br>and enter your information</p>
                </a>
            </div>

        </div>

    </section>

    <!-- Rent-a-car 2x half-width banners -->
    <section id="rentacar-banners">

        <div class="container-fluid bg-grey pb-5">

            <div class="section-title-holder pt-3 p-5">
                <h3 class="section-title">Renta Car</h3>
                <p class="section-subtitle">If you are looking to reyervisete the cart, choose the <br>car and enter your information
                </p>
            </div>

            <div class="row justify-content-center px-md-3 px-0">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 half-width-banner">

                    <div class="banner-image">
                        <img src="img/image-banner1.jpg" style="height: 100%;" alt="Rent a car">
                    </div>
                    <div class="banner-info">
                        <h5 class="banner-title">Belano Limo service</h5>
                        <div class="sepline"></div>
                        <p class="banner-text">Lorem Ipsum is simply dummy text of the <br> printing and typesetting industry</p>
                    </div>

                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 half-width-banner">
                    <div class="banner-image">
                        <img src="img/image-banner2.jpg" alt="Rent a car">
                    </div>
                    <div class="banner-info">
                        <h5 class="banner-title">Belano Limo service</h5>
                        <div class="sepline"></div>
                        <p class="banner-text">Lorem Ipsum is simply dummy text of the <br> printing and typesetting industry</p>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- Static block: Benefits -->
    <section id="benefits" class="bottom-border">

        <div class="container-fluid bottom-border p-md-5 py-md-2 p-4 pt-5">
            <div class="section-title-holder p-0">
                <h3 class="section-title">Benefits</h3>
                <p class="section-subtitle">If you are looking to reyervisete the cart, choose the <br>car and enter your information
                </p>
            </div>
        </div>

        <div class="container-fluid px-md-5 py-md-6 px-0 benefits-slider-wrapper">

            <div class="text-center" id="benefits-slider">
                <div class="benefits-item">
                    <div class="benefits-image">
                        <img src="img/image-benefits-01.jpg" alt="Welcome home pristup">
                    </div>
                    <p class="benefits-title">Welcome home pristup</p>
                </div>
                <div class="benefits-item">
                    <div class="benefits-image">
                        <img src="img/image-benefits-02.png" alt="Transfer od/do aerodroma">
                    </div>
                    <p class="benefits-title light">Transfer od/do aerodroma</p>
                </div>
                <div class="benefits-item">
                    <div class="benefits-image">
                        <img src="img/image-benefits-03.jpg" alt="Atraktivne lokacije">
                    </div>
                    <p class="benefits-title light">Atraktivne lokacije</p>
                </div>
                <div class="benefits-item">
                    <div class="benefits-image">
                        <img src="img/image-benefits-04.png" alt="Welcome home pristup">
                    </div>
                    <p class="benefits-title">Besprekorna čistoća</p>
                </div>
                <div class="benefits-item">
                    <div class="benefits-image">
                        <img src="img/image-benefits-05.png" alt="Transfer od/do aerodroma">
                    </div>
                    <p class="benefits-title light">Parking na upit</p>
                </div>
                <div class="benefits-item">
                    <div class="benefits-image">
                        <img src="img/image-benefits-06.png" alt="Atraktivne lokacije">
                    </div>
                    <p class="benefits-title light">Lojalti program</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Blog: Featured post -->
    <section id="blog" class="bottom-border">

        <div id="blog-featured" class="section-wrapper container-fluid bottom-border py-3 py-md-6 bg-grey">
            <div class="d-flex justify-content-md-start flex-column flex-sm-column flex-md-row">
                <div class="col-12 col-sm-12 col-md-5 col-lg-5 p-0">
                    <p class="block-title mb-3">Blog</p>
                    <h5 class="blog-date mb-5">New post 25. Jun 2018</h5>
                    <h3 class="blog-title red-color mb-4">24 Kitchen<br>Jamie Oliver kuva za vas</h3>
                    <p class="text-medium mb-5 pr-5 text-lightblue">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <p class="text-medium mb-5 pr-5 text-lightblue">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    <a href="#" class="more-info">More info</a>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-7 featured-blog-image text-right">
                    <img src="img/image-featured-blog.jpg" alt="24 Kitchen Jamie Oliver kuva za vas">
                </div>
            </div>
        </div>

        <!-- Blog: Post slider -->
        <div id="blog-slider" class="container-fluid pl-5 pr-0">

            <div class="section-title-holder">
                <h3 class="section-title">New posts</h3>
                <p class="section-subtitle">If you are looking to reyervisete the cart, choose the
                    <br>car and enter your information
                </p>
            </div>

            <ul class="blog-slider-wrapper" id="blog-post-slider">
                <li class="bs-item">
                    <div class="bs-image">
                        <img src="img/blog-post-image.jpg" alt="Blog Post Title">
                    </div>
                    <h5 class="bs-title">Belgrade on water. Project inheritance</h5>
                    <p class="bs-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <a href="" class="bs-more-btn">Discover more</a>
                </li>
                <li class="bs-item">
                    <div class="bs-image">
                        <img src="img/blog-post-image.jpg" alt="Blog Post Title">
                    </div>
                    <h5 class="bs-title">Belgrade on water. Project inheritance</h5>
                    <p class="bs-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <a href="" class="bs-more-btn">Discover more</a>
                </li>
                <li class="bs-item">
                    <div class="bs-image">
                        <img src="img/blog-post-image.jpg" alt="Blog Post Title">
                    </div>
                    <h5 class="bs-title">Belgrade on water. Project inheritance</h5>
                    <p class="bs-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <a href="" class="bs-more-btn">Discover more</a>
                </li>
                <li class="bs-item">
                    <div class="bs-image">
                        <img src="img/blog-post-image.jpg" alt="Blog Post Title">
                    </div>
                    <h5 class="bs-title">Belgrade on water. Project inheritance</h5>
                    <p class="bs-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <a href="" class="bs-more-btn">Discover more</a>
                </li>
                <li class="bs-item">
                    <div class="bs-image">
                        <img src="img/blog-post-image.jpg" alt="Blog Post Title">
                    </div>
                    <h5 class="bs-title">Belgrade on water. Project inheritance</h5>
                    <p class="bs-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <a href="" class="bs-more-btn">Discover more</a>
                </li>
                <li class="bs-item">
                    <div class="bs-image">
                        <img src="img/blog-post-image.jpg" alt="Blog Post Title">
                    </div>
                    <h5 class="bs-title">Belgrade on water. Project inheritance</h5>
                    <p class="bs-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>
                    <a href="" class="bs-more-btn">Discover more</a>
                </li>
            </ul>
        </div>

    </section>

    <!-- Static block: About -->
    <section id="quick-about" class="bg-grey-light p-md-5 p-0 bottom-border">
        <div class="container-fluid section-wrapper">
            <h3 class="title-big red-color">Belano apartments Belgrade</h3>
            <div class="col-md-7 qa-image text-center mb-3 m-auto img-max-width">
                <img src="img/image-homepage-01.png" alt="">
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <p class="text-small text-blue">Belano apartmani nude ugodan smeštaj u Beogradu tokom cele godine, svima kojima je osećaj vlastitog doma najvažniji na njihovim putovanjima.. </p>
                    <p class="text-small text-blue">Prijatan enterijer, atraktivne lokacije, pristupačne cene i domaćinska usluga su ono to Belano apartmane izdvaja od drugih agencija slične poslovne orijentacije. U duhu domaćinske tradicije i gostoprimljivosti našeg grada i naroda, izaći ćemo u susret svakoj vašoj želji, a kakvi bi to domaćini bili kada vas ne bi sačekali na aerodromu, autobuskoj i železničkoj stanici, ili na bilo kom drugom mestu koje vama odgovara?
                    </p>
                </div>
                <div class="col-12 col-md-6">
                    <p class="text-small text-blue">Belano apartmani nude ugodan smeštaj u Beogradu tokom cele godine, svima kojima je osećaj vlastitog doma najvažniji na njihovim putovanjima.. </p>
                    <p class="text-small text-blue">Prijatan enterijer, atraktivne lokacije, pristupačne cene i domaćinska usluga su ono to Belano apartmane izdvaja od drugih agencija slične poslovne orijentacije. U duhu domaćinske tradicije i gostoprimljivosti našeg grada i naroda, izaći ćemo u susret svakoj vašoj želji, a kakvi bi to domaćini bili kada vas ne bi sačekali na aerodromu, autobuskoj i železničkoj stanici, ili na bilo kom drugom mestu koje vama odgovara?
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact form + Map -->
    <section id="contact" class="pb-mb-5 px-md-5 px-4">
        <div class="container-fluid p-0">

            <div class="section-title-holder">
                <h3 class="section-title">Name input</h3>
                <p class="section-subtitle">If you are looking to reyervisete the cart, choose the
                    <br>car and enter your information
                </p>
            </div>

            <div class="d-flex justify-content-strech mb-5">
                <img src="img/image-map2.jpg" alt="" class="w-100">
            </div>

            <div class="row text-blue justify-content-between">
                <div class="col d-flex justify-content-start">
                    <i class="fa fa-map-marker mr-3 mt-1"></i>
                    <span>
                        Birčaninova, 11000 Beograd,Srbija<br>
                        office@belano.rs
                    </span>
                </div>
                <div class="col d-flex justify-content-center">
                    <i class="fa fa-globe mr-3 mt-1"></i>
                    <span>
                        <a href="http://www.belano.rs">http://www.belano.rs</a>
                    </span>
                </div>
                <div class="col d-flex justify-content-end">
                    <i class="fa fa-phone mr-3 mt-1"></i>
                    <span>
                        +381 60 55 66 509<br>
                        +381 66 55 66 509<br>
                        +381 11 36 77 773
                    </span>
                </div>
            </div>
        </div>
    </section>

    <section id="contact-form" class="p-5 bg-grey">

        <div class="container-fluid p-0">

            <form action="#" class="w-100">

                <div class="row d-flex">

                    <div class="col-12 col-md-6">
                        <div class="form-label-group mb-4">
                            <input type="text" name="contact-name" id="contact-name" class="form-control" required="required" placeholder="Fullname *">
                            <label for="contact-name">Fullname *</label>
                        </div>
                        <div class="form-label-group mb-4">
                            <input type="text" name="contact-email" id="contact-email" class="form-control" required="required" placeholder="Email *">
                            <label for="contact-email">Email *</label>
                        </div>
                        <div class="form-label-group mb-4">
                            <input type="text" name="contact-subject" id="contact-subject" class="form-control" required="required" placeholder="Subject">
                            <label for="contact-subject">Subject</label>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 pr-0">
                        <div class="form-label-group">
                            <textarea name="contact-message" id="contact-message" cols="30" rows="20" class="form-control" placeholder="Message"></textarea>
                            <label for="contact-message">Message</label>
                        </div>

                    </div>

                    <div class="text-right w-100">
                        <input type="submit" value="Send" class="btn btn-primary btn-medium">
                    </div>

            </form>

        </div>

    </section>

</div>

<script>

    $(document).ready(function(){

        $('#apartments-slider').slick({
            infinite: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            nextArrow: '<button type="button" class="slick-next btn-floating"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
            prevArrow: '<button type="button" class="slick-prev btn-floating"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: true,
                        arrows: false
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                        arrows: false
                    }
                }
            ]
        });

    })

</script>

<?php include('footer.php'); ?>

