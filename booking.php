<?php

	$page_title = 'Belano.rs - Booking';
	include('head.php');

	include('header.php');

?>

    <script src="js/timepicker.js"></script>

    <div class="main-container" id="page-booking">

    	<div class="container-fluid">
    		<div class="row">

    			<div class="col-6 scrollable" id="container-left">

                    <ul id="left-submenu" class="nav-tabs" id="apartman-tabs" role="tablist">
                        <li class="active"><a href="" id="tab-info" data-toggle="tab" href="#apart-info" role="tab" aria-controls="tab-apart-info">Apartman</a></li>
                        <li><a href="">Pricelist</a></li>
                        <li><a href="">Structure</a></li>
                        <li><a href="#">Location</a></li>
                        <li><a href="#">Reviews</a></li>
                        <li><a href="#" class="btn-booking">Booking</a></li>
                    </ul>
                    <!--  Booking steps-->
                    <div id="booking-steps">

                        <div id="booking-top-info" class="d-flex">
                            <div class="bt-name">Frida</div>
                            <div class="bt-info">2-soban stan, Beograd 65 M2</div>
                            <div class="bt-rating ml-auto">
                                <span>4.5</span>
                                <ul class="rate-stars">
                                    <li class="rated-star"><i class="fa fa-star"></i></li>
                                    <li class="rated-star"><i class="fa fa-star"></i></li>
                                    <li class="rated-star"><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                </ul>
                            </div>
                            <div class="bt-cart">
                                <img src="img/icon-bookingcart.svg" alt="">
                            </div>
                        </div>

                        <ul class="stepper stepper-vertical">

                            <!-- Step: Apartman -->
                            <li>

                                <div class="step-content lighten-3">

                                    <h4 class="booking-title">Do you want to book apartman?</h4>
                                    <p class="text-medium mb-4">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>

                                    <div class="form-label-group mb-0">
                                        <input type="text" name="booking-fullname" id="booking-fullname" class="form-control with-shadow" required="required" placeholder="Fullname *">
                                        <label for="booking-name">Fullname *</label>
                                    </div>

                                    <div class="form-label-group mb-0">
                                        <input type="text" name="booking-email" id="booking-email" class="form-control with-shadow" required="required" placeholder="Email *">
                                        <label for="booking-email">Email *</label>
                                    </div>

                                    <div class="form-label-group mb-0">
                                        <input type="text" name="booking-mobile" id="booking-mobile" class="form-control with-shadow" required="required" placeholder="Mobile *">
                                        <label for="booking-mobile">Mobile *</label>
                                    </div>

                                    <div class="form-row">
                                        
                                        <div class="form-group">
                                            <label for="booking-peoples">Broj osoba</label>
                                            <select name="booking-peoples" id="booking-peoples" class="browser-default custom-select">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="booking-peoples">Dece</label>
                                            <select name="booking-childs" id="booking-childs" class="browser-default custom-select">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="booking-peoples">Pet</label>
                                            <select name="booking-pet" id="booking-pet" class="browser-default custom-select">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        
                                    </div>

                                    <div class="d-flex border justify-content-between title-box-big red-border mb-4">
                                        <h5>Arrival & Going- information</h5>
                                    </div>



                                    <!-- Day picker -->
                                    <div id="booking-daypicker" class="cal-custom-head mb-4">
                                        <input type="text" class="form-control" id="form-booking-day" name="form-booking-day">
                                    </div>
                                    <script>
                                        $(document).ready(function(){
                                            $("#form-booking-day").caleran({
                                                inline: true,
                                                startEmpty: true,
                                                startDate: null,
                                                singleDate: false,
                                                startOnMonday: true,
                                                calendarCount: 1,
                                                showHeader: true,
                                                showFooter: false,
                                                startEmpty: true,
                                                dateUpdateContainer: "form-booking-day",
                                                enableKeybard: false,
                                                enableMonthSwitcher: true,
                                                container: ".calendar-ui",
                                            });
                                        });
                                    </script>

                                    <!--  Time picker-->
                                    <div class="d-flex border justify-content-between title-box-big">
                                        <h5>Time information</h5>
                                    </div>

                                    <div id="booking-timepicker">
                                        <input id="booking-time-in" class="clocklet-events" data-clocklet="format: k; appent-to="parent"" value="">
                                        <input id="booking-time-out" class="clocklet-events" data-clocklet="format: _H; "  value="">

                                        <!-- TIME PICKER -->
                                        <div id="time-picker">
                                            <div>
                                                <div id="time-picker-header" class="time-picker-bg">
                                                    <p>
                                                        <span id="time-picker-hour"></span>
                                                        <span id="time-picker-mins"></span>
                                                        <span id="time-picker-ampm"></span>
                                                    </p>
                                                </div>
                                                <div>
                                                    <a id="time-picker-alt-ok-button" class="btn btn-sm btn-flat"><i class="material-icons">check_circle</i></a>
                                                </div>
                                                <div id="time-picker-clock">
                                                    <span id="time-picker-hour-hand"></span>
                                                    <span id="time-picker-hour-center"></span>
                                                    <p id="time-picker-hour-1" class="time-picker-hour" data-value="1"></p>
                                                    <p id="time-picker-hour-2" class="time-picker-hour" data-value="2"></p>
                                                    <p id="time-picker-hour-3" class="time-picker-hour" data-value="3"></p>
                                                    <p id="time-picker-hour-4" class="time-picker-hour" data-value="4"></p>
                                                    <p id="time-picker-hour-5" class="time-picker-hour" data-value="5"></p>
                                                    <p id="time-picker-hour-6" class="time-picker-hour" data-value="6"></p>
                                                    <p id="time-picker-hour-7" class="time-picker-hour" data-value="7"></p>
                                                    <p id="time-picker-hour-8" class="time-picker-hour" data-value="8"></p>
                                                    <p id="time-picker-hour-9" class="time-picker-hour" data-value="9"></p>
                                                    <p id="time-picker-hour-10" class="time-picker-hour" data-value="10"></p>
                                                    <p id="time-picker-hour-11" class="time-picker-hour" data-value="11"></p>
                                                    <p id="time-picker-hour-12" class="time-picker-hour" data-value="12"></p>
                                                </div>
                                                <p id="time-picker-am-button" class="time-picker-ampm-button">AM</p>
                                                <p id="time-picker-pm-button" class="time-picker-ampm-button">PM</p>
                                                <div id="time-picker-buttons" class="text-right">
                                                    <a id="time-picker-reset-button" class="btn btn-sm btn-flat"><i class="material-icons">today</i></a>
                                                    <a id="time-picker-cancel-button" class="btn btn-sm btn-flat">Cancel</a>
                                                    <a id="time-picker-ok-button" class="btn btn-sm btn-flat">OK</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- TIME PICKER -->


                                        <!-- JQuery, Bootstrap JS -->


                                        <!-- TimePicker JS -->
                                        <script type="text/javascript">
                                            // Initialize the TimePicker
                                            var tp = TimePicker("#0D47A1");
                                            // Set Button Click Listener
                                            $("#time-button").click(function() {
                                                var time = new Date();
                                                tp.show(time, function(selected) {
                                                    $("#time").html(selected.toLocaleTimeString());
                                                });
                                            });
                                        </script>

                                    </div>


                                    <script>

                                        (function () {
                                            var inputElement = document.querySelector('.clocklet-events');

                                            inputElement.addEventListener('clocklet.opening', function (event) {
                                                console.log(event.type, event.target.value, event.detail.options);
                                                if (document.querySelector('.clocklet-cancel-opening').checked) {
                                                    event.preventDefault();
                                                }
                                            });

                                            inputElement.addEventListener('clocklet.opened', function (event) {
                                                console.log(event.type, event.target.value, event.detail.options);
                                            });

                                            inputElement.addEventListener('clocklet.closing', function (event) {
                                                console.log(event.type, event.target.value);
                                                // if (document.querySelector('.clocklet-cancel-closing').checked) {
                                                //     event.preventDefault();
                                                // }
                                            });

                                            inputElement.addEventListener('clocklet.closed', function (event) {
                                                // console.log(event.type, event.target.value);
                                            });

                                            inputElement.addEventListener('input', function (event) {
                                                // console.log(event.type, event.target.value, event.target.value);
                                                // console.log(event.target);
                                                var pickedtime = event.target.value;
                                                var time_output = pickedtime.split(':', 5);
                                                var time_checkin    = time_output[0];
                                                var time_checkout   = time_output[1];
                                                console.log('Check in: ' + time_checkin + " --- Check Out: " + time_checkout);
                                            });
                                        })();
                                    </script>

                                    <div class="bi-box d-flex border justify-content-between p-4 mb-4">
                                        <div class="booking-info-time">
                                            <p>Time from</p>
                                            <div class="d-flex flex-row align-items-end">
                                                <div class="bi-time">13:00</div>
                                                <div class="bi-time-label">AM</div>
                                            </div>
                                        </div>
                                        <div>
                                            <i class="icon-svg">
                                                <img src="img/icon-arrow-right.svg" alt="">
                                            </i>
                                        </div>
                                        <div class="booking-info-time">
                                            <p>Time to</p>
                                            <div class="d-flex flex-row align-items-end">
                                                <div class="bi-time">15:00</div>
                                                <div class="bi-time-label">PM</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </li>

                        </ul>

                    </div>


                    <div id="left-content">

                    </div>
                    
    			</div>

                <!-- Apartment informations -->
    			<div class="col-6" id="container-right">
    				
    				<?php include('templates/single-apartment-inner.php'); ?>

    			</div>

    		</div>
    	</div>

    </div>

<?php include('footer.php'); ?>