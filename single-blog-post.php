<?php
$page_title = 'Belano.rs - Blog listing';
include('head.php');
include('header.php');
?>

    <body id="page-single-blog-post">

    <?php include('templates/page-preloader.php'); ?>

    <div id="main-container">

        <!-- Blog SubHeader -->
        <div id="sub-header">
            <h1 class="page-title">The Blog</h1>
            <ul class="blog-links">
                <li class="active"><a href="#">Dogadjaji u Beogradu</a></li>
                <li><a href="#">Vodič kroz Beograd</a></li>
                <li><a href="#">Znamenitosti Beograda</a></li>
                <li><a href="#">Turizam u Beogradu</a></li>
            </ul>

            <div class="blog-actions">
                <a href="#"><i class="fa fa-search mr-2"></i></a>
                <a href="#"><i class="fas fa-rss"></i></a>
            </div>
        </div>

        <!-- Single Blog Post: Share Bar -->
        <div id="single-blog-top">
            <a href="#">Share <i class="fas fa-share-alt ml-2"></i></a>
            <div class="sbp-links">
                <a href="#"><i class="fab fa-instagram"></i></a>
                <a href="#"><i class="fab fa-facebook-f"></i></a>
                <a href="#"><i class="fab fa-twitter"></i></a>
                <a href="#"><i class="fab fa-linkedin-in"></i></a>
            </div>
            <div></div>
        </div>

        <!-- Blog Post Title + Image -->
        <div class="bottom-border py-5">
            <div class="container">
                <h2 class="sbp-title">Belgrade cocktail bars and food -  Characters of years in Belgrade</h2>
                <div class="sbp-image">
                    <img src="img/single-blog-post-image.jpg" alt="Belgrade cocktail bars and food -  Characters of years in Belgrade">
                </div>
            </div>
        </div>

        <!-- Blog Post Content -->
        <div class="border-bottom py-md-5 py-4">
            <div class="container">
                <div class="d-flex">

                    <!-- Single Blog Post - Left -->
                    <div class="col-md-2 sbp-left">
                        <div class="sbp-avatar">
                            <img src="http://placehold.it/150x150" alt="">
                        </div>
                        <div class="sbp-author">Belano</div>
                        <div class="sbp-info d-flex">
                            <p>Published</p>
                            <p class="sbp-date">Avgust 5 2018</p>
                        </div>
                    </div>

                    <!-- Single Blog Post - Right - Content -->
                    <div class="col-md-10 sbp-right" id="sbp-content">

                        <p> Odvajkada je druženje bilo beogradski. Od vremena kafana i kaldrma do danas, suština ljubavi
                            prema
                            dobrom društvu i priči se nije promenila niti malo. Danas su beogradski koktel barovi upravo
                            ono
                            što
                            oslikava tu kulturu spontanog susretanja i sastajanja, kao i kafane koje odolevaju zubu
                            vremena
                            i
                            svim modernim pojavama kao stubovi socijalizacije u ovom prelepom gradu.
                        </p>

                        <h5>Šta je zapravo koktel bar</h5>

                        <p> Svi uživaju u posetama barova različitih vrsta, svakodnevno, tokom putovanja, kao
                            relaksaciju
                            pre
                            odmora u smeštaju u gradu tokom turističkih izleta, ali među apsolutnim favoritima su koktel
                            barovi.
                            Pored toga što nude priliku da probate neke izvanredne koktele, akcenat se stavlja i na
                            kompletan
                            doživljaj i uslugu, atmosferu i sve ostalo kako biste kompletno uživali u iskustvu izgleda,
                            mirisa i
                            ukusa.
                        </p>


                        <h5>koktel barovi</h5>

                        <p> Čak i među koktel barovima postoji mnogo različitih vrsta, od kojih svaki nudi različite
                            vrste
                            iskustava. Od opuštenih bašti koje pružaju izuzetno ugodnu atmosferu i lagana predjela, do
                            onih
                            u
                            lounge bar stilu sa formalnim iskustvom, svaki je drugačiji.
                        </p>
                        <h5> Koji su koktel barovi najpopularniji u Beogradu?</h5>
                        <p> Mišljenja o ovome možda variraju, ali mi ćemo izložiti lična iskustva, pa se unapred
                            izvinjavamo
                            ako smo nekog izostavili, pozovite da nas podsetite.
                        </p>


                        <p>Ovo bi mogao biti objektivni utisak da nije bilo loših iskustava vezanih za rezervaciju kada
                            su bez obaveštenja otkazali čitavo devojačko veče zbog druge, ,,jače“ rezervacije. Ovako
                            nedopustivo ponašanje navodno je stvar prošlosti, ali bez obzira na lično iskustvo ovog tipa, mnoga
                            dobra iskustva su takođe učinila da je Bar Central bio i ostao jedno od najboljih mesta za
                            doživljaj kvalitetnih koktela i zanimljive pripreme. Smešten u srcu grada, predstavlja svojim dizajnom
                            duh Njujorka i Berlina, a sa nagrađivanim koktel majstorima, nije bilo moguće izostaviti ga sa ove
                            liste.</p>


                        <p>Smešten u jezgru Dorćola, Bitters bar je brzo stekao verne mušterije pre svega izuzetnom
                            ponudom
                            koktela, ali i nastojanjima barmena da za svakoga nađu idealni recept i rešenje, pa i mimo
                            ponuđene karte pića. Šarmantan i topao ambijent je prijateljski udešen sa dosta detalja, a
                            tikki modus
                            učiniće da pomislite i da ste možda na obali nekog egzotičnog okeana.</p>

                        <h5>3) Shootiranje</h5>

                        <p>Gavrila Principa 7, Beograd</p>

                        <p>Shootiranje je bar u neposrednoj blizini mnogobrojnih smeštaja kod Kalemegdana i apartmana u
                            centru Beograda i sa konkurentnim cenama i konceptom kako koktela, tako i kratkih takozvanih
                            šotova,
                            brzo je stekao popularnost. Jednako ga vole i mlađi i stariji gosti, mada je idealan za
                            zagrevanje
                            pre nego što krenete put Savamale ili Beton hale.</p>

                        <h5>4) Belgradski Koktel Klub</h5>


                        <p>Beogradski koktel klub je novotvoreno mesto u Beogradu, a ono po čemu je posebno je iznad
                            svega
                            jesu
                            vrhunski koktel majstori sa udruženim iskustvom i znanjem koji u toplom i ušuškanom
                            ambijentu
                            stvaraju atmosferu nekadašnjih klubova, glamura i zabave. Širi koncept koji obuhvata i
                            muziku
                            uživo
                            i prijateljski odnos prema gostima jeste upravo-beogradski.</p>


                        <h5>5) KLjUC-Klub Ljubitelja Cipela</h5>

                        <h6>Gospodar Jovanova 47, Beograd</h6>

                        <p>KljUC – Klub ljubitelja cipela jeste osveženje na gradskoj gastro sceni i jedini je lokal
                            među
                            spomenutima koji je 100% namenjen nepušačima. Ambijent u stilu holivudskog glamura poziva na
                            druženje, a nesvakidašnja i elegantno prezentovana ponuda izmamljuje svima osmeh na lice.
                            Druženje i
                            ukus su ovde akcentovani, kao i prezentacija kao takva. Za druženje sa stilom, ovaj koktel
                            klub
                            i
                            kafe u srcu Starog grada zaslužio je svoje mesto na ovoj listi.</p>

                        <p>Ne smemo zaboraviti da postoje i fantastični bezalkoholni kokteli, tako da ukoliko vozite,
                            svaki
                            od
                            ovih lokala postaraće se da za vas pripremi minimalno alkoholni ili sasvim bezalkoholni
                            koktel.
                            Eksperimentišite, igrajte se i uživajte u činjenici da ste danas u Beogradu, i u mogućnosti
                            da
                            iskusite sve ono najbolje što on nudi. Makar to bila i lagana šetnja, sređene i oronule
                            fasade,
                            vrhunski koktel po izuzetnoj ceni, udoban smeštaj na dobroj lokaciji samo za vas ili ljudi i
                            restorani oko vas.
                            Javite nam utiske, a ova lista svakako može biti revidirana. Beograd jeste zaista-centar
                            provoda.</p>

                    </div>
                </div>
            </div>
        </div>

        <!-- Past Posts - Listing -->
        <div class="py-6">
            <div class="container">
                <div class="pb-5">
                    <h3 class="inner-block-title">Older Posts</h3>
                </div>
                <div id="blog-post-listing" class="row">
                    <?php include('templates/blog-post-listing.php') ?>
                    <?php include('templates/blog-post-listing.php') ?>
                    <?php include('templates/blog-post-listing.php') ?>
                    <?php include('templates/blog-post-listing.php') ?>
                </div>
            </div>
        </div>

    </div>

    <script>

        $(document).ready(function(){

            // Hide topbar on scroll
            $(window).scroll(function(){
                if ( $(window).scrollTop() >= 60 ) {
                    $('body').addClass('header-sticky');
                }
                else {
                    $('body').removeClass('header-sticky');
                }
            });

        });

    </script>

<?php include('footer.php'); ?>