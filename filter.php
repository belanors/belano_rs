
<?php
    $page_title = 'Belano.rs - Filter';
    include('head.php');
?>

<body id="page-filter" onload="initMap()">

    <?php include('header.php'); ?>
    <?php include('templates/page-preloader.php'); ?>



<div id="main-container">

    <!-- Map + Listing container -->
    <div class="container-fluid" id="filter-container">

        <div class="row d-flex h-100">

            <!-- Map + Filter form -->
            <div class="col-md-7 p-0" id="filter-map-wrapper">

                <!-- Google Map -->
                <div id="filter-gmaps"></div>

                <!-- Filter: Location -->
                <div id="filter-location" class="tab-pane filter-holder" role="tabpanel" aria-labelledby="btn-filter-location">

                    <div class="filter-title">
                        <span>
                            <i class="fas fa-map-marker-alt mr-3"></i>
                            Location
                        </span>
                        <button class="filter-close"><i class="fas fa-times"></i></button>
                    </div>

                    <form action="#" id="form-filter-location">

                        <div class="filter-content">

                            <div class="row w-100 m-0">

                                <div class="col-md-6 pl-0">

                                    <div class="big-block">Cities</div>

                                    <!-- Filter: City list -->
                                    <ul class="filter-loc-list">

                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-beograd" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-beograd">Beograd</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-novisad" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-novisad">Novi Sad</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-kragujevac" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-kragujevac">Kragujevac</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-nis" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-nis">Nis</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-paracin" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-paracin">Paracin</label>
                                            </div>
                                        </li>


                                    </ul>


                                </div>
                                <div class="col-md-6 pr-0">

                                    <div class="big-block">Municipalities</div>

                                    <!-- Filter: Municipalities list -->
                                    <ul class="filter-loc-list">

                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-vracar" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-vracar">Vracar</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-zvezdara" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-zvezdara">Zvezdara</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-novibg" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-novibg">Novi Beograd</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-palilula" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-palilula">Palilula</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-rakovica" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-rakovica">Rakovica</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-savskivenac" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-savskivenac">Savski Venac</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-vozdovac" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-vozdovac">Vozdovac</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="form-check p-0">
                                                <input type="checkbox" class="form-check-input" id="filter-loc-zemun" name="filter-loc-city">
                                                <label class="form-check-label" for="filter-loc-zemun">Zemun</label>
                                            </div>
                                        </li>

                                    </ul>

                                </div>

                            </div>

                        </div>

                        <div class="filter-actions">
                            <p>
                                <i class="fas fa-exclamation-circle mr-3"></i>
                                svaki ulaz pre 14h se doplacuje 50% od cene jednog nocenja <br>
                                svako napustanje stana posle 12h se doplacuje 50% od cene jednog nocenja
                            </p>
                            <div>
                                <button type="button" class="btn btn-white no-shadow filter-clear">Clear</button>
                                <button type="submit" class="btn btn-primary filter-apply">Apply</button>
                            </div>
                        </div>

                    </form>

                </div>

                <!-- Filter: Persons -->
                <div id="filter-persons" class="tab-pane filter-holder" role="tabpanel" aria-labelledby="btn-filter-persons">

                    <div class="filter-title">
                        <span>
                            <i class="fas fa-map-marker-alt mr-3"></i>
                            Persons
                        </span>
                        <button class="filter-close"><i class="fas fa-times"></i></button>
                    </div>

                    <form action="#" id="form-filter-persons" class="d-flex justify-content-between h-100 bg-white">

                        <div class="filter-content">
                    
                            <div class="row align-items-center m-auto col-md-10 h-100">
                                <div class="col-md-6 pr-5">
                                    <h5 class="mb-3">Adults</h5>
                                    <select class="custom-select w-100">
                                        <option value="" disabled selected>How many adult person is</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="2">4</option>
                                        <option value="3">5</option>
                                        <option value="2">6</option>
                                        <option value="3">7</option>
                                    </select>
                                </div>
                                <div class="col-md-6 pl-5">
                                    <h5 class="mb-3">Children</h5>
                                    <select class="custom-select w-100">
                                        <option value="" disabled selected>How many chiled person is</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="2">4</option>
                                        <option value="3">5</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="filter-actions">
                            <p>
                                <i class="fas fa-exclamation-circle mr-3"></i>
                                svaki ulaz pre 14h se doplacuje 50% od cene jednog nocenja <br>
                                svako napustanje stana posle 12h se doplacuje 50% od cene jednog nocenja
                            </p>
                            <div>
                                <button type="button" class="btn btn-white no-shadow filter-clear">Clear</button>
                                <button type="submit" class="btn btn-primary filter-apply">Apply</button>
                            </div>
                        </div>

                    </form>

                </div>

                <!-- Filter: Structure -->
                <div id="filter-structure" class="tab-pane filter-holder" role="tabpanel" aria-labelledby="btn-filter-structure">

                    <div class="filter-title">
                        <span>
                            <i class="fas fa-map-marker-alt mr-3"></i>
                            Structure
                        </span>
                        <button class="filter-close"><i class="fas fa-times"></i></button>
                    </div>
                    
                    <form action="#" id="form-filter-structure" class="d-flex justify-content-between h-100 bg-white">

                        <div class="filter-content">
                            
                            <div class="row align-items-center m-auto col-md-10 h-100">
                                <div class="col-md-6 pr-5">
                                    <h5 class="mb-3">Apartment</h5>
                                    <select class="custom-select w-100">
                                        <option value="" disabled selected>Jednosoban</option>
                                        <option value="1">Dvosoban</option>
                                        <option value="2">Trosoban</option>
                                        <option value="3">Cetvorosoban</option>
                                        <option value="2">Petosoban</option>
                                    </select>
                                </div>
                                <div class="col-md-6 pl-5">
                                    <h5 class="mb-3">Room</h5>
                                    <select class="custom-select w-100">
                                        <option value="" disabled selected>1/4</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="2">4</option>
                                        <option value="3">5</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        
                        

                        <div class="filter-actions">
                            <p>
                                <i class="fas fa-exclamation-circle mr-3"></i>
                                svaki ulaz pre 14h se doplacuje 50% od cene jednog nocenja <br>
                                svako napustanje stana posle 12h se doplacuje 50% od cene jednog nocenja
                            </p>
                            <div>
                                <button type="button" class="btn btn-white no-shadow filter-clear">Clear</button>
                                <button type="submit" class="btn btn-primary filter-apply">Apply</button>
                            </div>
                        </div>

                    </form>

                </div>

                <!-- Filter: Calendar -->
                <div id="filter-calendar" class="tab-pane filter-holder" role="tabpanel" aria-labelledby="btn-filter-calendar">

                    <div class="filter-title">
                        <span>
                            <i class="far fa-calendar-alt mr-3"></i>
                            Date from - to
                        </span>
                        <button class="filter-close"><i class="fas fa-times"></i></button>
                    </div>
                
                    <form action="#" id="form-filter-timedate">

                        <div class="filter-content">
                            
                            <div class="row w-100 nav m-0">
                            
                                <div class="col-md-6 pl-0">
                                    <button class="dropdown-button w-100 filter-datetime-switch" data-toggle="tab" id="tab-filter-date" href="#filter-date-block">
                                    Arrival & Going Information</button>
                                </div>

                                <div class="col-md-6 pr-0">
                                    <button class="dropdown-button w-100 filter-datetime-switch" data-toggle="tab" id="tab-filter-time" href="#filter-time-block">
                                    Time Information</button>
                                </div>

                            </div>

                            <div class="tab-content w-100 p-0 pt-2 filter-inner">
                                
                                <!-- Date inputs -->
                                <div class="tab-pane fade filter-datetime-wrap" id="filter-date-block" role="tabpanel" aria-labelledby="tab-filter-date">
                                    
                                    <div id="filter-date-input-wrap" class="cal-custom-head mb-4">
                                        <input type="text" class="form-control" id="filter-date-input" name="filter-date-input">
                                    </div>

                                    <script>
                                        $(document).ready(function () {
                                            $("#filter-date-input").caleran({
                                                inline: true,
                                                startEmpty: true,
                                                startDate: null,
                                                singleDate: false,
                                                startOnMonday: true,
                                                calendarCount: 1,
                                                showHeader: true,
                                                showFooter: false,
                                                startEmpty: true,
                                                legendFirst: "Check-in day",
                                                legendSecond: "Check-out day",
                                                dateUpdateContainer: "filter-date-input",
                                                enableKeybard: false,
                                                enableMonthSwitcher: true,
                                                container: ".calendar-ui",
                                            });
                                        });
                                    </script>


                                </div>

                                <!-- Time inputs -->
                                <div class="tab-pane fade filter-datetime-wrap" id="filter-time-block" role="tabpanel" aria-labelledby="tab-filter-time">
                                    
                                    <!-- Time picker -->
                                    <div id="filter-timepicker">

                                        <div class="bi-box d-flex border justify-content-between p-4 mb-2 bg-white">
                                            <div class="booking-info-time">
                                                <p>Time from</p>
                                                <div class="d-flex flex-row align-items-end">
                                                    <div class="bi-time"><span id="time-picker-checkin-update">00</span>:00</div>
                                                    <div class="bi-time-label" id="time-picker-checkin-ampm">AM</div>
                                                </div>
                                            </div>
                                            <div>
                                                <i class="icon-svg">
                                                    <img src="img/icon-arrow-right.svg" alt="">
                                                </i>
                                            </div>
                                            <div class="booking-info-time">
                                                <p>Time to</p>
                                                <div class="d-flex flex-row align-items-end">
                                                    <div class="bi-time"><span id="time-picker-checkout-update">00</span>:00</div>
                                                    <div class="bi-time-label" id="time-picker-checkout-ampm">AM</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="time-picker-holder" class="bg-white p-5 border">
                                            <input type="text" id="filter-time-checkin" class="timepicker">
                                            <input type="text" id="filter-time-checkout" class="timepicker">

                                            <div id="clockpick-checkin"></div>
                                            <div id="clockpick-checkout"></div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        
                        <div class="filter-actions">
                            <p>
                                <i class="fas fa-exclamation-circle mr-3"></i>
                                svaki ulaz pre 14h se doplacuje 50% od cene jednog nocenja <br>
                                svako napustanje stana posle 12h se doplacuje 50% od cene jednog nocenja
                            </p>
                            <div>
                                <button type="button" class="btn btn-white no-shadow filter-clear">Clear</button>
                                <button type="submit" class="btn btn-primary filter-apply">Apply</button>
                            </div>
                        </div>

                    </form>

                </div>

                <!-- Filter: Price -->
                <div id="filter-price" class="tab-pane filter-holder" role="tabpanel" aria-labelledby="btn-filter-price">
                    
                    <div class="filter-title">
                        <span>
                            <i class="far fa-calendar-alt mr-3"></i>
                            Price
                        </span>
                        <button class="filter-close"><i class="fas fa-times"></i></button>
                    </div>

                    <form class="multi-range-field">

                        <div class="filter-content d-flex align-items-center justify-content-between m-3 bg-white">

                            <div class="price-range-inner">

                                <div class="text-center">
                                    
                                </div>
                                
                                <div class="multi-range-price-wrap">
                                    <input id="multi" class="multi-range" type="range" />
                                </div>

                                <ul class="price-range-block">
                                    <li class="price-light">
                                        <span>$20</span>
                                        <div class="pr-block" style="height: 20%"></div>
                                    </li>
                                    <li class="price-med">
                                        <span>$30</span>
                                        <div class="pr-block" style="height: 30%"></div>
                                    </li>
                                    <li>
                                        <span>$40</span>
                                        <div class="pr-block" style="height: 40%"></div>
                                    </li>
                                    <li class="price-high">
                                        <span>$50</span>
                                        <div class="pr-block" style="height: 50%"></div>
                                    </li>
                                    <li>
                                        <span>$20</span>
                                        <div class="pr-block" style="height: 20%"></div>
                                    </li>
                                    <li class="price-light">
                                        <span>$20</span>
                                        <div class="pr-block" style="height: 20%"></div>
                                    </li>
                                    <li class="price-med">
                                        <span>$30</span>
                                        <div class="pr-block" style="height: 30%"></div>
                                    </li>
                                    <li>
                                        <span>$40</span>
                                        <div class="pr-block" style="height: 40%"></div>
                                    </li>
                                    <li class="price-high">
                                        <span>$50</span>
                                        <div class="pr-block" style="height: 50%"></div>
                                    </li>
                                    <li>
                                        <span>$20</span>
                                        <div class="pr-block" style="height: 20%"></div>
                                    </li>
                                </ul>

                            </div>

                        </div>

                        <div class="filter-actions">
                            <p>
                                <i class="fas fa-exclamation-circle mr-3"></i>
                                svaki ulaz pre 14h se doplacuje 50% od cene jednog nocenja <br>
                                svako napustanje stana posle 12h se doplacuje 50% od cene jednog nocenja
                            </p>
                            <div>
                                <button type="button" class="btn btn-white no-shadow filter-clear">Clear</button>
                                <button type="submit" class="btn btn-primary filter-apply">Apply</button>
                            </div>
                        </div>

                    </form>

                </div>

                <!-- Filter: Rating -->
                <div id="filter-rating" class="tab-pane filter-holder" role="tabpanel" aria-labelledby="btn-filter-rating">
                    
                    <div class="filter-title">
                        <span>
                            <i class="far fa-star mr-3"></i>
                            Rating
                        </span>
                        <button class="filter-close"><i class="fas fa-times"></i></button>
                    </div>

                    <div class="filter-content">
                        
                        <div class="w-100 d-flex apart-reviews bg-white">
                            <div class="d-flex apart-review-row">
                                <div class="col-5">Lokacija</div>
                                <div class="col-4 pl-0">
                                    <ul class="review-stars rate-stars">
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                </div>
                                <div class="col-3">Vrlo dobro</div>
                            
                            </div>
                            <div class="d-flex apart-review-row">
                                <div class="col-5">Sobe</div>
                                <div class="col-4 pl-0">
                                    <ul class="review-stars rate-stars">
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                </div>
                                <div class="col-3">Pristojno</div>
                            
                            </div>
                            <div class="d-flex apart-review-row">
                                <div class="col-5">Usluge</div>
                                <div class="col-4 pl-0">
                                    <ul class="review-stars rate-stars">
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                </div>
                                <div class="col-3">Vrlo dobro</div>
                             
                            </div>
                            <div class="d-flex apart-review-row">
                                <div class="col-5">Cistoća</div>
                                <div class="col-4 pl-0">
                                    <ul class="review-stars rate-stars">
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                </div>
                                <div class="col-3">Pristojno</div>
                              
                            </div>
                            <div class="d-flex apart-review-row">
                                <div class="col-5">Vrednost novca</div>
                                <div class="col-4 pl-0">
                                    <ul class="review-stars rate-stars">
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                </div>
                                <div class="col-3">Vrlo dobro</div>
                           
                            </div>
                            <div class="d-flex apart-review-row">
                                <div class="col-5">Komfor</div>
                                <div class="col-4 pl-0">
                                    <ul class="review-stars rate-stars">
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                </div>
                                <div class="col-3">Vrlo dobro</div>
                    
                            </div>
                            <div class="d-flex apart-review-row">
                                <div class="col-5">Gradjevina</div>
                                <div class="col-4 pl-0">
                                    <ul class="review-stars rate-stars">
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                </div>
                                <div class="col-3">Vrlo dobro</div>
                            
                            </div>
                            <div class="d-flex apart-review-row">
                                <div class="col-5">Okolina</div>
                                <div class="col-4 pl-0">
                                    <ul class="review-stars rate-stars">
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li class="rated-star"><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                </div>
                                <div class="col-3">Pristojno</div>
                            
                            </div>
                        </div>

                    </div>

                    <div class="filter-actions">
                        <p>
                            <i class="fas fa-exclamation-circle mr-3"></i>
                            svaki ulaz pre 14h se doplacuje 50% od cene jednog nocenja <br>
                            svako napustanje stana posle 12h se doplacuje 50% od cene jednog nocenja
                        </p>
                        <div>
                            <button type="button" class="btn btn-white no-shadow filter-clear">Clear</button>
                            <button type="submit" class="btn btn-primary filter-apply">Apply</button>
                        </div>
                    </div>

                </div>

                <!-- Filter: More filters -->
                <div id="filter-extrafilters" class="tab-pane filter-holder" role="tabpanel" aria-labelledby="btn-filter-extrafilters">
                    Rating
                </div>

            </div>

            <!-- Apartment listing -->
            <div class="col-md-5 bg-white p-0" id="filter-listing-wrapper">

                <ul class="filter-tabs">
                    <li><a href="#" class="active">All</a></li>
                    <li><a href="#">Search nearby</a></li>
                    <li class="ml-auto">12 Apartments selected</li>
                </ul>

                <div class="d-flex flex-wrap h-100" id="filter-listing">
                    <?php include('templates/apart-list-small.php'); ?>
                    <?php include('templates/apart-list-small.php'); ?>
                    <?php include('templates/apart-list-small.php'); ?>
                    <?php include('templates/apart-list-small.php'); ?>
                    <?php include('templates/apart-list-small.php'); ?>
                    <?php include('templates/apart-list-small.php'); ?>
                    <?php include('templates/apart-list-small.php'); ?>
                    <?php include('templates/apart-list-small.php'); ?>
                    <?php include('templates/apart-list-small.php'); ?>
                </div>

            </div>

        </div>
    </div>

    <!-- Filter buttons -->
    <div id="filter-bottom">

        <div class="d-flex filter-inputs nav">

            <div class="col">
                <div class="filter-field">
                    <button class="btn btn-flat no-shadow filter-button" data-toggle="tab" href="#filter-location" id="btn-filter-location">
                        <i class="fas fa-map-marker-alt"></i>
                        Location
                    </button>
                </div>
            </div>

            <div class="col">
                <div class="filter-field">
                    <button class="btn btn-flat no-shadow filter-button" data-toggle="tab" href="#filter-persons" id="btn-filter-persons" role="tab" aria-selected="true">
                        <i class="fas fa-user-plus"></i>
                        Persons
                    </button>
                </div>
            </div>
            <div class="col">
                <div class="filter-field">
                    <button class="btn btn-flat no-shadow filter-button" data-toggle="tab" href="#filter-structure" id="btn-filter-structure">
                        <i class="fas fa-user-plus"></i>
                        Structure
                    </button>
                </div>
            </div>
            <div class="col">
                <div class="filter-field">
                    <button class="btn btn-flat no-shadow filter-button" data-toggle="tab" href="#filter-calendar" id="btn-filter-date" >
                        <i class="far fa-calendar-alt"></i>
                        Date from - to
                    </button>
                </div>
            </div>
            <div class="col">
                <div class="filter-field">
                    <button class="btn btn-flat no-shadow filter-button" data-toggle="tab" href="#filter-price" id="btn-filter-price">
                        <i class="far fa-credit-card"></i>
                        Price from - to
                    </button>
                </div>
            </div>
            <div class="col">
                <div class="filter-field">
                    <button class="btn btn-flat no-shadow filter-button" data-toggle="tab" href="#filter-rating" id="btn-filter-rating">
                        <i class="far fa-star"></i>
                        Rating
                    </button>
                </div>
            </div>

            <div class="col">
                <div class="filter-field">
                    <i class="icon-svg icon-parking"></i>
                    Parking
                    <div class="form-check p-0 d-flex align-items-center ml-3">
                        <input type="checkbox" class="form-check-input" id="filter-parking" name="filter-parking">
                        <label class="form-check-label" for="filter-parking"></label>
                    </div>
                </div>
            </div>

            <div class="col bg-darkblue">
                <div class="filter-field">
                    <button class="btn btn-primary">
                        <i class="icon-svg icon-filter-list"></i>
                        Filter search
                    </button>
                </div>
            </div>
            <div class="col bg-darkblue">Clear all</div>
            
        </div>

    </div>

</div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfSPBFW9HWAOyKbdXE7TLkiXa-honwMsc&callback=initMap"></script>
    <script src="js/markerWithLabel.js"></script>

    <script src="js/multi-range.min.js"></script>

    <script>

        // Define apartment location markers
        // Apartment name | Lat | Long | zIndex | Price | Image path | Available (1) / Not available (0)
        var apartments = [
            ['Twin', 44.81927132, 20.46501841, 4, '50$', 'img/apartman_small_items.jpg', 1],
            ['Margarita', 44.81999013, 20.46185911, 5, '40$', 'img/apartman_small_items.jpg', 1],
            ['Cronulla', 44.81861266, 20.46097934, 3, '55$', 'img/apartman_small_items.jpg', 0],
            ['Manly', 44.81818648, 20.4639405, 2, '35$', 'img/apartman_small_items.jpg', 1],
            ['Maroubra', 44.81807232, 20.46136022, 1, '45$', 'img/apartman_small_items.jpg', 0]
        ];

        // Google Maps API
        var map;

        function initMap() {

            map = new google.maps.Map(document.getElementById('filter-gmaps'), {
                center: {
                    lat: -34.397, lng: 150.644
                },
                zoom: 10,
                // styles: mapStyle,
                fullscreenControl: true
            });

            // map marker style
            var markerImage = {
                url: 'img/map-marker.svg',
                size: new google.maps.Size(40, 50),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 32)
            };

            var labelClass = "marker_label";

            infowindow = new google.maps.InfoWindow({
                pixelOffset: new google.maps.Size(160,340)
            });

            var bounds = new google.maps.LatLngBounds();

            // Loop thru locations JSON + generate map markers
            for (var i in apartments) {

                var location = apartments[i];
                var latlng = new google.maps.LatLng(location[1], location[2]);

                var priceLabel = location[4];
                var apartImage = location[5];
                var labelData = location[6];

                if( labelData == 1 ) {
                    labelClass = "marker_label available";
                }
                else {
                    labelClass = "marker_label";
                }

                var infoContent = "<div class='apart-item list-item apart-popup'><div class='apart-image'><img src='" + apartImage + "' alt=''><a href='#' class='btn-add-wishlist' data-id='49'><i class='far fa-heart'></i></a></div> <div class='apart-item-bottom'><div class='apart-title-wrap'><div class='d-flex justify-content-between'> <ul class='rate-stars light'> <li class='rated-star'><i class='fa fa-star'></i></li> <li class='rated-star'><i class='fa fa-star'></i></li> <li class='rated-star'><i class='fa fa-star'></i></li> <li><i class='fa fa-star'></i></li> <li><i class='fa fa-star'></i></li> </ul> <span class='text-secondary'>Available</span> </div> <h4 class='apart-item-title'>" + location[0] + "</h4> </div> <ul class='apart-labels'> <li>Entire apartment</li> <li>Free cancellation</li> </ul> <ul class='apart-features'> <li class='fe-parking'></li> <li class='fe-location'></li> <li class='fe-phone'></li> <li class='fe-bookmark\"></li> </ul> </div> </div>";

                bounds.extend(latlng);

                // generate marker with price labels
                var marker = new MarkerWithLabel({
                    position: latlng,
                    map: map,
                    title: location[0],
                    content: infoContent,

                    labelContent: location[4],
                    labelAnchor: new google.maps.Point(0, 63),
                    labelClass: labelClass, // the CSS class for the label
                    labelInBackground: true,
                    icon: markerImage
                });

                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent(this.content);
                    infowindow.open(map, this);
                });

                map.fitBounds(bounds); // Center the map to fit all markers on the screen

            }

        }

        $(document).ready(function(){
            
            $('#multi').mdbRange({
                width: '100%',
                single: {
                  active: true,
                  multi: {
                    active: true,
                    rangeLength: 10,
                    step: 5
                  },
                }
            });

           // Filter button actions
           // $('.filter-button').click(function(){
           //
           //     // TODO - close previous window
           //
           //     $('.filter-button').removeClass('active');
           //     // unselect active buttons
           //
           //
           //     var container = $(this).data('container');
           //
           //     // open container
           //     $(this).toggleClass('active');
           //     $(container).removeClass('hidden');
           //
           // });

            $('.filter-button').on('click', function (e) {
                $('.filter-button').removeClass('active');
                e.preventDefault()
                $(this).tab('show');
            });

            $('.filter-datetime-switch').on('click', function (e) {
                $('.filter-datetime-switch').removeClass('active');
                e.preventDefault()
                $(this).tab('show');
            });

            $('.filter-close').click(function(e){
                $('.tab-pane.filter-holder').removeClass('active');
            });

            $('.filter-holder form').submit(function(e){

                e.preventDefault();

                var form_data = $(this).serialize();
                form_data = JSON.stringify(form_data);

                var formID = $(this).attr('id');

                alert("form: " + formID + " | data = " + form_data);

            })

            // Time pickers
            $('#filter-time-checkin').clockpicker({
                donetext: 'Done',
                autoclose: false,
                wrapper: "#clockpick-checkin",
                twelvehour: true,
                hoursOnly: true,
                autoShow: true,
                clockUpdateDiv: "#time-picker-checkin-update",
                clockUpdateAmPm: "#time-picker-checkin-ampm"
            });

            $('#filter-time-checkin').clockpicker('show');

            $('#filter-time-checkout').clockpicker({
                donetext: 'Done',
                autoclose: false,
                wrapper: "#clockpick-checkout",
                twelvehour: true,
                hoursOnly: true,
                autoShow: true,
                showDone: false,
                clockUpdateDiv: "#time-picker-checkout-update",
                clockUpdateAmPm: "#time-picker-checkout-ampm",
                addClass: 'clockpick-checkout'
            });

            $('#filter-time-checkout').clockpicker('show');

        });

    </script>

<?php include('bottom-includes.php'); ?>