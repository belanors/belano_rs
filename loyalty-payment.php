<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Belano.rs - Login/Register Loyalty</title>

    <link rel="stylesheet" href="fonts/productsans.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-reboot.css">

    <!-- Material Design for Bootstrap -->
    <link rel="stylesheet" href="css/mdb.min.css">

    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/jquery.min.js"></script>

    <!-- Google Sing-in -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-client_id" content="514439232405-u920cnd9j2e471k92iqjqq2p6ja0rnkd.apps.googleusercontent.com">

    <!-- FontAwesome Icons -->
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700&amp;subset=latin-ext" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- Leave those next 4 lines if you care about users using IE8 -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-loyalty-payment">

<?php include('header.php'); ?>

<?php include('templates/page-preloader.php'); ?>


<div class="main-container">

    <div class="container-fluid">

        <div class="row">

            <!-- Loyalty payment form -->
            <div class="col-6" id="container-left">

                <div id="inner-left">

                    <div class="inner-left-content d-flex flex-column h-100 bg-white">

                        <h2 class="text-darkblue">Card information</h2>
                        <p class="text-medium text-lightblue mb-5">If you are looking to reyervisete the cart, choose the car <br>and enter your information</p>

                        <!-- Payment labels -->
                        <div class="d-flex justify-content-between p-3 border block-shadow px-5 mb-3">
                            <img src="img/icon-mastercard.svg" alt="MasterCard">
                            <img src="img/icon-visa.svg" alt="Visa">
                            <img src="img/icon-visa.svg" alt="Telenor Bank">
                            <img src="img/icon-payoneer.svg" alt="Payoneer">
                        </div>

                        <!-- Form: Loyalty payment -->
                        <form action="" id="form-loyalty-payment">

                            <!-- Payment switcher -->
                            <div class="payment-switcher block-row justify-content-start block-shadow my-3 p-0">
                                <div class="col-4 p-0">
                                    <div class="form-check">
                                        <input type="radio" class="form-check-input" id="loyalty-payment_paypal" name="loyalty-payment_method">
                                        <label class="form-check-label" for="loyalty-payment_paypal"><img src="img/icon-paypal.png" alt="PayPal"></label>
                                    </div>
                                </div>
                                <div class="col-4 p-0">
                                    <div class="form-check">
                                        <input type="radio" class="form-check-input" id="loyalty-payment_money" name="loyalty-payment_method">
                                        <label class="form-check-label" for="loyalty-payment_money">Money</label>
                                    </div>
                                </div>
                            </div>

                            <!-- Creditcard Inputs -->
                            <div class="d-flex w-100 flex-column">
                                <div class="d-flex justify-content-between align-items-center my-5 cc-graphic">
                                    <div class="col-md-5 text-center">
                                        <img src="img/image-mastercard.svg" alt="MasterCard">
                                    </div>
                                    <div class="col-md-7 p-0">
                                        <img src="img/image-creditcard.png" alt="CreditCard">
                                    </div>
                                </div>

                                <div class="form-label-group mb-0 input-group-prepend">
                                    <i class="far fa-credit-card"></i>
                                    <input type="text" id="loyalty-cc-numb" name="loyalty-cc-numb" class="form-control w-100 with-shadow border-bottom-0" placeholder="Card Number">
                                    <label for="loyalty-cc-numb">Card Information</label>
                                </div>
                                <div class="d-flex">
                                    <div class="col-md-6 col-12 p-0">
                                        <div class="form-label-group">
                                            <input type="text" class="form-control with-shadow border-right-0" name="loyalty-cc-name" id="loyalty-cc-name" placeholder="Card Holder">
                                            <label for="loyalty-cc-name">Card Holder</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-6 p-0">
                                        <div class="form-label-group">
                                            <input type="text" class="form-control with-shadow border-right-0" name="loyalty-cc-date" id="loyalty-cc-date" placeholder="Exp. Date">
                                            <label for="loyalty-cc-date">Exp. Date</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-6 p-0">
                                        <div class="form-label-group">
                                            <input type="text" class="form-control with-shadow" name="loyalty-cc-cvc" id="loyalty-cc-cvc" placeholder="CVC">
                                            <label for="loyalty-cc-cvc">CVC</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Creditcard Confirmation block -->
                            <div class="big-checkbox-wrapper">
                                <div class="col-md-9 col-9 p-0">
                                    <div class="custom-control big-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="loyalty-cc-confirm" data-btn="#btn-loyalty-cc-confirm">
                                        <label class="custom-control-label" for="loyalty-cc-confirm">Do you agree with your information</label>
                                    </div>
                                </div>
                                <div class="col-md-3 p-0">
                                    <button type="submit" class="btn btn-tall btn-secondary m-0 w-100 h-100 shadow-none with-icon" id="btn-loyalty-cc-confirm" disabled="disabled">
                                        <span>All in</span>
                                        <i class="far fa-check-circle ml-2"></i>
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div>

                    <!-- Loyalty payment waiting window -->
                    <div id="loyalty-payment-window" class="payment-overlay fade">
                        <div id="loyalty-payment-waiting" class="booking-overlay-inner open fade bg-white">
                            <div class="d-flex justify-content-center align-items-center w-100 flex-column">
                                <div class="loading-dots mb-5">
                                    <span class="dot one"></span>
                                    <span class="dot two"></span>
                                    <span class="dot three"></span>
                                </div>
                                <h3>Waiting</h3>
                                <p>Name users</p>
                                <p class="text-small text-grey mt-3">if you don't go to the page press button</p>
                            </div>
                        </div>
                        <div id="loyalty-payment-confirmed" class="payment-overlay-inner hidden fade bg-white">
                            <div class="d-flex justify-content-center align-items-center w-100 flex-column">
                                <div class="circle-icon"><i class="fas fa-check"></i></div>
                                <h5 class="text-lightblue mb-2">Thank you</h5>
                                <h4 class="mb-5">Name of users</h4>
                                <h2 class="my-4">You successfully buy Name of card</h2>
                                <div class="mt-5 w-50 text-center">
                                    <a href="dashboard" class="btn btn-lg btn-white with-shadow rounded text-secondary">View your dashboard</a>
                                </div>
                                <p class="text-small text-lightblue mt-3">if you don't go to the page press button</p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <!-- Loyalty banner -->
            <div class="col-6 d-none d-sm-none d-md-block d-lg-block" id="container-right">

                <div class="inner-right border-left">
                    <div class="img-max-width">
                        <img src="img/image-loyalty-image1.jpg" alt="Loyalty">
                    </div>

                    <div class="p-6">
                        <h1 class="text-darkblue">Join Belano with Basic Card</h1>
                        <p class="text-medium text-lightblue">1Kod & gratis dan</p>

                        <h3 class="text-darkblue mb-4">$20/mo</h3>

                        <p class="mb-1">Registruj se kod nas</p>
                        <p>Lajk - instagramu</p>

                        <p class="text-small text-lightblue mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do <br>eiusmod tempor incididunt
                            ut labore et dolore magna aliqua.</p>

                    </div>
                </div>

            </div>

        </div>

    </div>

</div>


<script>

    $(document).ready(function(){

        preloader();

        // BigCheckbox - Booking agreement
        $('.big-checkbox input[type="checkbox"]').change(function () {
            var check_button = $(this, 'input').data('btn');
            if( $(this).prop('checked') == true ) {
                $(this).parent().addClass('checked');
                $(check_button).attr('disabled', false).addClass('enabled');
                $(check_button).find('span').text('You can book'); // update text
            }
            else {
                $(this).parent().removeClass('checked');
                $(check_button).attr('disabled', true);
                $(check_button).find('span').text('All in');
            }
        });

        // Payment type switcher
        $('input[name="loyalty-payment_method"]').change(function () {
            $('.form-check.payment-selected').removeClass('payment-selected');
            $(this).prop("checked", true).parent().addClass('payment-selected');
        });

        // Unselect payment methods if user start using CC inputs
        $('#loyalty-cc-numb, #loyalty-cc-holder').keypress(function() {
            $('input[name="loyalty-payment_method"]').prop('checked', false);
            $('.payment-switcher .form-check').removeClass('payment-selected');
        });


        $('#form-loyalty-payment').submit(function(e){

            e.preventDefault();

            console.log('booking: open confirm window');

            // waiting on payment status
            $('body').toggleClass('fixed-window-open');
            $('#loyalty-payment-window').addClass('show');
            $('#loyalty-payment-waiting').addClass('open');


            // simulation of successful payment
            setTimeout(function(){
                $('#loyalty-payment-waiting').removeClass('open');
                $('#loyalty-payment-confirmed').addClass('open');
            }, 3000);

        });

    })

</script>


<?php include('bottom-includes.php'); ?>