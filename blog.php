<?php
	$page_title = 'Belano.rs - Blog listing';
	include('head.php');
	include('header.php');
?>

    <body id="page-blog">

    <?php include('templates/page-preloader.php'); ?>
    
    <div id="main-container">

    	<div id="sub-header">

            <h1 class="page-title">The Blog</h1>

            <ul class="blog-links">
                <li class="active"><a href="#">Dogadjaji u Beogradu</a></li>
                <li><a href="#">Vodič kroz Beograd</a></li>
                <li><a href="#">Znamenitosti Beograda</a></li>
                <li><a href="#">Turizam u Beogradu</a></li>
            </ul>
            
            <div class="blog-actions">
                <a href="#"><i class="fa fa-search mr-2"></i></a>
                <a href="#"><i class="fas fa-rss"></i></a>
            </div>

        </div>

        <!-- Top Posts - Featured -->
    	<div class="bottom-border py-md-6 py-4 blog-top-post">
            <div class="container">
                <div id="feature-blog-post" class="blog-post-big">
                    <div class="col-md-5 p-0 bpb-left">
                        <h3>New Posts</h3>
                        <div class="bpb-content">
                            <h5 class="bpb-title">Belgrade cocktail bars and food - <br>Characters of years in Belgrade</h5>
                            <p class="bpb-text">Odvaj kada je druženje bilo beogradski. Od vremena kafana i kaldrma do
                                danas, suština ljubavi prema dobrom društvu i priči se nije promenila niti malo. Danas
                                su beogradski koktel barovi upravo ono što oslikava tu kulturu spontanog susretanja i
                                sastajanja, kao i kafane...</p>
                            <a href="single-blog-post" class="btn-link mt-2">Read more</a>
                        </div>
                    </div>
                    <div class="col-md-7 bpb-right">
                        <div class="bpb-image">
                            <img src="img/featured-blog-post.jpg" alt="Blog Post Title">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Top Posts -->
        <div class="border-bottom py-md-6 py-4">
            <div class="container">
                <div class="pb-5">
                    <h3 class="inner-block-title">Top Posts</h3>
                </div>
                <div id="blog-post-listing" class="row">
                    <?php include('templates/blog-post-listing.php') ?>
                    <?php include('templates/blog-post-listing.php') ?>
                    <?php include('templates/blog-post-listing.php') ?>
                    <?php include('templates/blog-post-listing.php') ?>
                </div>
            </div>
        </div>

        <!-- Past Posts - Featured -->
        <div class="border-bottom py-md-6 py-4">
            <div class="container">
                <div id="feature-blog-post" class="blog-post-big">
                    <div class="col-md-5 p-0 bpb-left">
                        <h3>Past Posts</h3>
                        <div class="bpb-content">
                            <h5 class="bpb-title">Belgrade cocktail bars and food - <br>Characters of years in Belgrade</h5>
                            <p class="bpb-text">Odvaj kada je druženje bilo beogradski. Od vremena kafana i kaldrma do
                                danas, suština ljubavi prema dobrom društvu i priči se nije promenila niti malo. Danas
                                su beogradski koktel barovi upravo ono što oslikava tu kulturu spontanog susretanja i
                                sastajanja, kao i kafane...</p>
                            <a href="single-blog-post" class="btn-link mt-2">Read more</a>
                        </div>
                    </div>
                    <div class="col-md-7 bpb-right">
                        <div class="bpb-image">
                            <img src="img/featured-blog-post.jpg" alt="Blog Post Title">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Past Posts - Listing -->
        <div class="py-6">
            <div class="container">
                <div class="pb-5">
                    <h3 class="inner-block-title">Top Posts</h3>
                </div>
                <div id="blog-post-listing" class="row">
                    <?php include('templates/blog-post-listing.php') ?>
                    <?php include('templates/blog-post-listing.php') ?>
                    <?php include('templates/blog-post-listing.php') ?>
                    <?php include('templates/blog-post-listing.php') ?>
                </div>
            </div>
        </div>

    </div>

    <script>

        $(document).ready(function(){

            // Hide topbar on scroll
            $(window).scroll(function(){
                if ( $(window).scrollTop() >= 60 ) {
                    $('body').addClass('header-sticky');
                }
                else {
                    $('body').removeClass('header-sticky');
                }
            });

        });

    </script>

<?php include('footer.php'); ?>