<div id="apart-inner-view">
    <div class="main-image">

        <div class="main-image-price">
            <p>25 €</p>
            <span class="apartman-status">Available</span>
        </div>

        <div class="carousel slide carousel-fade" id="inner-view-slider" data-ride="carousel">

            <!-- Apartments images -->
            <div class="carousel-inner" role="listbox">

                <div class="carousel-item active">
                    <div class="carousel-caption">
                        <div class="animated fadeInDown">
                            <h3 class="h3-responsive">Living room</h3>
                            <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
                        </div>
                    </div>
                    <img src="img/apartman_main_image.jpg" alt="First slide">
                </div>

                <div class="carousel-item">
                    <div class="carousel-caption">
                        <div class="animated fadeInDown">
                            <h3 class="h3-responsive">Bedroom</h3>
                            <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
                        </div>
                    </div>
                    <img src="img/apartman_main_image2.jpg" alt="Second slide">
                </div>

                <div class="carousel-item">
                    <div class="carousel-caption">
                        <div class="animated fadeInDown">
                            <h3 class="h3-responsive">Garden</h3>
                            <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
                        </div>
                    </div>
                    <img src="img/apartman_main_image.jpg" alt="First slide">
                </div>

                <div class="carousel-item">
                    <div class="carousel-caption">
                        <div class="animated fadeInDown">
                            <h3 class="h3-responsive">Rest rooms</h3>
                            <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
                        </div>
                    </div>
                    <img src="img/apartman_main_image2.jpg" alt="Second slide">
                </div>

                <div class="carousel-item">
                    <div class="carousel-caption">
                        <div class="animated fadeInDown">
                            <h3 class="h3-responsive">Garden</h3>
                            <p>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
                        </div>
                    </div>
                    <img src="img/apartman_main_image.jpg" alt="First slide">
                </div>

            </div>

        </div>

    </div>

    <!-- Slider Controls-->
    <div id="inner-images-bottom">
        <ol class="carousel-indicators">
            <li data-target="#inner-view-slider" class="active" data-slide-to="0"><img src="img/inner-view-thumb3.jpg"></li>
            <li data-target="#inner-view-slider" data-slide-to="1"><img src="img/inner-view-thumb4.jpg"></li>
            <li data-target="#inner-view-slider" data-slide-to="2"><img src="img/inner-view-thumb3.jpg"></li>
            <li data-target="#inner-view-slider" data-slide-to="3"><img src="img/inner-view-thumb4.jpg"></li>
            <li data-target="#inner-view-slider" data-slide-to="4"><img src="img/inner-view-thumb3.jpg"></li>
        </ol>

        <div class="split-apart-info">
            <div class="apart-info-status">
                <i class="apart-status free"></i>
                <p>Slobodan</p>
            </div>
            <div>
                <h4 class="apart-item-title">Frida</h4>
                <span>2-Soban stan, Beograd 65 M2</span>
            </div>
            <div class="d-flex justify-content-between align-content-center">
                <ul class="feature-icons light mt-3">
                    <li><i class="material-icons">local_parking</i></li>
                    <li><i class="material-icons">navigation</i></li>
                    <li><i class="material-icons">phone</i></li>
                    <li><i class="material-icons">pool</i></li>
                    <li><i class="material-icons">wifi</i></li>
                </ul>
                <div>
                    <button type="button" class="btn btn-md btn-primary">Preview</button>
                    <button type="button" class="btn btn-md btn-secondary">Next</button>
                </div>
            </div>
        </div>
    </div>

</div>