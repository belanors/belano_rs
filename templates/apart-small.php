<div class="col-12 col-sm-12 col-md-6 col-lg-6 apart-item small">

    <div class="apart-image with-slider">

        <span class="apart-overlay"></span>

        <div class="apart-slider">
            <div class="">
                <img src="img/apartman_small_items.jpg" alt="">
            </div>
            <div class="">
                <img src="img/apartman_small_items.jpg" alt="">
            </div>
            <div class="">
                <img src="img/apartman_small_items.jpg" alt="">
            </div>
        </div>

        <div class="apart-item-info floating-info">
            <div class="price-wrap">
                <p class="medium-label">Pay per night</p>
                <p class="apart-item-price">25 €</p>
            </div>
            <div class="apart-title-wrap">
                <h4 class="apart-item-title"><a href="single-apartmant">Studio Margarita 33M2</a></h4>
                <div class="d-flex justify-content-between">
                    <ul class="rate-stars light">
                        <li class="rated-star"><i class="fa fa-star"></i></li>
                        <li class="rated-star"><i class="fa fa-star"></i></li>
                        <li class="rated-star"><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="slick-controls"></div>

    </div>

    <div class="apart-item-bottom">
        <p class="apart-area">Nova Karaburma</p>
        <ul class="apart-labels">
            <li>Entire apartment</li>
            <li>Free cancellation</li>
        </ul>
        <ul class="feature-icons mt-3">
            <li><i class="material-icons">local_parking</i></li>
            <li><i class="material-icons">navigation</i></li>
            <li><i class="material-icons">phone</i></li>
            <li><i class="material-icons">pool</i></li>
            <li><i class="material-icons">wifi</i></li>
        </ul>
    </div>

</div>
