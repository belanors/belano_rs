<ul class="feature-icons apart-features mt-3">
    <li class="fe-parking"></li>
    <li class="fe-location"></li>
    <li class="fe-phone"></li>
    <li class="fe-bookmark"></li>
    <li class="fe-pool"></li>
    <li class="fe-wash"></li>
</ul>