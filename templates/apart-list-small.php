<div class="col-12 col-sm-12 col-md-4 col-lg-4 apart-item list-item small">

    <div class="apart-image">
        <span class="apart-overlay"></span>
        <div class="">
            <img src="img/apartman_small_items.jpg" alt="">
        </div>
        <div class="apart-item-info floating-info">
            <div class="price-wrap">
                <p class="medium-label">Pay per night</p>
                <p class="apart-item-price">25 €</p>
            </div>
        </div>
    </div>

    <div class="apart-item-bottom">
        <div class="apart-title-wrap">
            <!-- Rating stars -->
            <div class="d-flex justify-content-between">
                <ul class="rate-stars light">
                    <li class="rated-star"><i class="fa fa-star"></i></li>
                    <li class="rated-star"><i class="fa fa-star"></i></li>
                    <li class="rated-star"><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                </ul>
            </div>
            <h4 class="apart-item-title">Studio Margarita 33M2</h4>
        </div>
        <ul class="apart-labels">
            <li>Entire apartment</li>
            <li>Free cancellation</li>
        </ul>
        <ul class="apart-features">
            <li><i class="material-icons">local_parking</i></li>
            <li><i class="material-icons">navigation</i></li>
            <li><i class="material-icons">phone</i></li>
            <li><i class="material-icons">pool</i></li>
            <li><i class="material-icons">wifi</i></li>
        </ul>
    </div>

</div>
