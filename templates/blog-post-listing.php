<div class="col-md-6 col-12">
    <div class="blog-post">
        <div class="col-md-6 bp-left">
            <div class="bp-image">
                <img src="img/blogpost-image-small.jpg" alt="Post Title">
            </div>
            <div class="bp-title">Belgrade in food Best places in town</div>
            <div class="bp-btns">
                <a href="#" class="btn-link">Share</a>
                <a href="single-blog-post" class="btn-link">Read more</a>
            </div>
        </div>
        <div class="col-md-6 bp-right">
            <div class="bp-content">
                <p>Odvaj kada je druženje bilo beogradski. Od vremena kafana i kaldrma do danas, suština ljubavi prema
                    dobrom društvu i priči se nije promenila niti malo. Danas su beogradski koktel barovi upravo ono što
                    oslikava tu kulturu...</p>
                <p>Odvaj kada je druženje bilo beogradski. Od vremena kafana i kaldrma do danas, suština ljubavi prema
                    dobrom društvu i priči se nije promenila niti malo. </p>
            </div>
        </div>
    </div>
</div>