
    <?php
        $page_title = 'Belano.rs - Single apartman';
        include('head.php');
    ?>

    <body id="page-single-apartment" onload="initMap()">

    <?php include('header.php'); ?>

    <?php include('templates/page-preloader.php'); ?>
    
    <div class="main-container" data-spy="scroll" data-target="#list-example" data-offset="0">

        <div id="overlay" class="search-overlay"></div>

    	<div class="container-fluid">
    		<div class="row" id="inner-container-wrapper">
                <div class="col-md-6 col-12" id="container-left">

                    <div id="booking-top-sticky">

                        <!-- Anchor Links -->
                        <ul id="left-submenu">
                            <li><a href="#apart-info">Info</a></li>
                            <li><a href="#apart-step-pricelist">Pricelist</a></li>
                            <li><a href="#apart-step-structure">Structure</a></li>
                            <li><a href="#apart-step-location">Location</a></li>
                            <li><a href="#apart-step-reviews">Reviews</a></li>
                            <li><a href="#booking-form" class="btn-booking">Booking</a></li>
                        </ul>

                        <!-- Sticky info -->
                        <div id="booking-top-info">
                            <div class="bt-name">Frida</div>
                            <div class="bt-info">2-soban stan, Beograd 65 M2</div>
                            <div class="bt-rating ml-auto">
                                <div class="bt-price">
                                    <span>$34</span> / nocu
                                </div>
                                <span>4.5</span>
                                <ul class="rate-stars">
                                    <li class="rated-star"><i class="material-icons">grade</i></li>
                                    <li class="rated-star"><i class="material-icons">grade</i></li>
                                    <li class="rated-star"><i class="material-icons">grade</i></li>
                                    <li><i class="material-icons">grade</i></li>
                                    <li><i class="material-icons">grade</i></li>
                                </ul>
                            </div>
                            <a href="#booking-form" class="bt-cart">
                                Booking now <span class="ml-3"><img src="img/icon-bookingcart.svg" alt=""></span>
                            </a>
                        </div>

                    </div>

                    <!-- Main apartment content -->
                    <div class="inner-scrollable">

                        <div id="left-content">

                            <div class="inner-content py-5">

                                <div class="d-flex flex-row w-100 justify-content-between">

                                    <div class="d-flex flex-column" id="apart-info">
                                        <h4 class="title-big mb-3">Frida</h4>
                                        <p class="text-medium">
                                            Slavija Mekenzija,<br>
                                            2-Soban stan, Beograd 65 M2
                                        </p>
                                    </div>

                                    <div class="d-flex flex-column justify-content-start text-right">
                                        <div class="total-rate sa-rate mb-4">4.5</div>
                                        <ul class="rate-stars apart-stars">
                                            <li class="rated-star"><i class="material-icons">grade</i></li>
                                            <li class="rated-star"><i class="material-icons">grade</i></li>
                                            <li class="rated-star"><i class="material-icons">grade</i></li>
                                            <li><i class="material-icons">grade</i></li>
                                            <li><i class="material-icons">grade</i></li>
                                        </ul>
                                    </div>

                                </div>

                                <!-- Feature Icons -->
                                <ul class="feature-icons mt-3 mb-5">
                                    <li><i class="material-icons">local_parking</i></li>
                                    <li><i class="material-icons">navigation</i></li>
                                    <li><i class="material-icons">phone</i></li>
                                    <li><i class="material-icons">pool</i></li>
                                    <li><i class="material-icons">wifi</i></li>
                                </ul>

                                <div class="d-block text-lightblue">
                                    <p class="text-medium">Stan se nalazi u Makenzijevoj ulici u istoj zgradi i na istom
                                        spratu kao i studio Gentleman. Ispred zgrade postoji mini trg uredjen zelenilom,
                                        kao
                                        i autobuska stanica sa svim linijama koja vode u samo srce Beograda. Stan Frida
                                        je
                                        dvorisno orijentisan pa Vam je obezbedjen potpuni mir. U neposrednoj blizini se
                                        nalazi Kalenic pijaca i Cuburski park, a Hram Svetog Save je udaljen 10 min
                                        lagane
                                        setnje.</p>
                                    <p class="text-medium">Vrlo lep stan, moderno opremljen sa dve spavace sobe, u kojem
                                        jedna soba ima prostran bracni krevet, a druga bracni i solo krevet. Osim
                                        potpuno
                                        opremljene kuhinje i kupatila, stan ima udoban trpezarijski prostor, kao i
                                        dnevni
                                        boravak i terasu.</p>
                                    <p class="text-medium">Frida pruza mogucnost veoma komotnog smestaja 5 osoba.
                                        Gostima su
                                        na raspolaganju cista posteljina i peskiri, kao i redovno ciscenje i odrzavanje
                                        u
                                        slucaju boravka duzeg od 7 dana.</p>
                                    <p class="text-medium">Stan se nalazi u Makenzijevoj ulici u istoj zgradi i na istom
                                        spratu kao i studio Gentleman. Ispred zgrade postoji mini trg uredjen zelenilom,
                                        kao
                                        i autobuska stanica sa svim linijama koja vode u samo srce Beograda. Stan Frida
                                        je
                                        dvorisno orijentisan pa Vam je obezbedjen potpuni mir. U neposrednoj blizini se
                                        nalazi Kalenic pijaca i Cuburski park, a Hram Svetog Save je udaljen 10 min
                                        lagane
                                        setnje.</p>
                                    <p class="text-medium">
                                        Vrlo lep stan, moderno opremljen sa dve spavace sobe, u kojem jedna soba ima
                                        prostran bracni krevet, a druga bracni i solo krevet. Osim potpuno opremljene
                                        kuhinje i kupatila, stan ima udoban trpezarijski prostor, kao i dnevni boravak i
                                        terasu. </p>
                                    <p class="text-medium">Frida pruza mogucnost veoma komotnog smestaja 5 osoba.
                                        Gostima su
                                        na raspolaganju cista posteljina i peskiri, kao i redovno ciscenje i odrzavanje
                                        u
                                        slucaju boravka duzeg od 7 dana.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Apartmant details -->
                        <div id="apart-block-info" class="container bg-grey p-0" data-spy="scroll" data-target="#left-submenu" data-offset="0">

                            <div class="inner-content position-relative py-5">

                                <div class="d-flex flex-column mb-3">
                                    <h4>Do you want to book apartman?</h4>
                                    <p class="text-medium">"Neque porro quisquam est qui dolorem ipsum quia dolor sit
                                        amet, consectetur, adipisci velit..."</p>
                                </div>

                                <!-- Apartment feature list -->
                                <ul class="apart-feature-list mb-5">

                                    <li class="col-md-3">
                                       <div class="apart-fe-single">
                                           <i class="material-icons">phone</i>
                                           <span>Telefon</span>
                                       </div>
                                   </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single">
                                            <i class="material-icons">ac_unit</i>
                                            <span>Klima uređaj</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single">
                                            <i class="material-icons">router</i>
                                            <span>Ugradjen router</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single active">
                                            <i class="material-icons">tv</i>
                                            <span>Smart TV Android</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single">
                                            <i class="material-icons">phone</i>
                                            <span>Telefon</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single">
                                            <i class="material-icons">ac_unit</i>
                                            <span>Klima uređaj</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single">
                                            <i class="material-icons">router</i>
                                            <span>Ugradjen router</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single active">
                                            <i class="material-icons">tv</i>
                                            <span>Smart TV Android</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single">
                                            <i class="material-icons">phone</i>
                                            <span>Telefon</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single">
                                            <i class="material-icons">ac_unit</i>
                                            <span>Klima uređaj</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single">
                                            <i class="material-icons">router</i>
                                            <span>Ugradjen router</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single active">
                                            <i class="material-icons">tv</i>
                                            <span>Smart TV Android</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single">
                                            <i class="material-icons">phone</i>
                                            <span>Telefon</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single">
                                            <i class="material-icons">ac_unit</i>
                                            <span>Klima uređaj</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single">
                                            <i class="material-icons">router</i>
                                            <span>Ugradjen router</span>
                                        </div>
                                    </li>
                                    <li class="col-md-3">
                                        <div class="apart-fe-single active">
                                            <i class="material-icons">tv</i>
                                            <span>Smart TV Android</span>
                                        </div>
                                    </li>

                                </ul>

                                <!-- Pricelist -->
                                <div class="d-flex flex-column" id="apart-step-pricelist">
                                    <button class="dropdown-button w-100" data-toggle="collapse" aria-controls="apart-pricelist" href="#apart-pricelist" type="button">Cene
                                        <span>*Broj osoba se naplacuje</span>
                                    </button>

                                    <div class="collapse dropdown-block w-100" id="apart-pricelist">

                                        <table class="table table-bordered table-fixed table-highlight w-100 bg-white table-pricelist text-center">
                                            <thead>
                                            <tr>
                                                <th scope="col">Broj ljudi</th>
                                                <th scope="col">1 - 3 <span>dana</span></th>
                                                <th scope="col">4 - 7 <span>dana</span></th>
                                                <th scope="col">8 - 15 <span>dana</span></th>
                                                <th scope="col">16 - 30 <span>dana</span></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td scope="row">1 - 2</td>
                                                <td>55</td>
                                                <td>53</td>
                                                <td>56</td>
                                                <td>53</td>
                                            </tr>
                                            <tr>
                                                <td scope="row">1 - 2</td>
                                                <td>55</td>
                                                <td>53</td>
                                                <td>56</td>
                                                <td>53</td>
                                            </tr>
                                            <tr>
                                                <td scope="row">1 - 2</td>
                                                <td>55</td>
                                                <td>53</td>
                                                <td>56</td>
                                                <td>53</td>
                                            </tr>
                                            <tr>
                                                <td scope="row">1 - 2</td>
                                                <td>55</td>
                                                <td>53</td>
                                                <td>56</td>
                                                <td>53</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <p class="medium-text small text-grey my-4 ml-4">* Lorem Ipsum is simply dummy
                                            text of the printing and typesetting industry. Lore Ipsum has been the
                                            industry's standard dummy text ever since the 1500s, when an unknown printer
                                            took a galley of type and scrambled it to make</p>

                                    </div>
                                </div>

                                <div class="row my-5 px-4">
                                    <p class="text-medium text-lightblue">Stan se nalazi u Makenzijevoj ulici u istoj
                                        zgradi i
                                        na istom spratu kao i studio Gentleman. Ispred zgrade postoji mini trg uredjen
                                        zelenilom, kao i autobuska stanica sa svim linijama koja vode u samo srce
                                        Beograda.
                                        Stan Frida je dvorisno orijentisan pa Vam je obezbedjen potpuni mir. U
                                        neposrednoj
                                        blizini se nalazi Kalenic pijaca i Cuburski park, a Hram Svetog Save je udaljen
                                        10
                                        min lagane setnje.
                                    </p>
                                    <p class="text-medium text-lightblue"> Vrlo lep stan, moderno opremljen sa dve
                                        spavace sobe,
                                        u kojem jedna soba ima prostran bracni krevet, a druga bracni i solo krevet.
                                        Osim
                                        potpuno opremljene kuhinje i kupatila, stan ima udoban trpezarijski prostor, kao
                                        i
                                        dnevni boravak i terasu.
                                    </p>

                                    <p class="text-medium text-lightblue"> Frida pruza mogucnost veoma komotnog smestaja
                                        5
                                        osoba. Gostima su na raspolaganju cista posteljina i peskiri, kao i redovno
                                        ciscenje
                                        i odrzavanje u slucaju boravka duzeg od 7 dana.
                                    </p>
                                </div>

                                <!-- Calendar -->
                                <div class="row mb-4">
                                    <button class="dropdown-button w-100" data-toggle="collapse"
                                            aria-controls="apart-calendar" href="#apart-calendar" type="button">Kalendar
                                        <span>* Broj osoba se naplacuje</span>
                                    </button>

                                    <div class="collapse dropdown-block w-100 range-preselected" id="apart-calendar">
                                        <input id="apart-calendar-input" readonly="readonly" type="text"
                                               value="03/22/2019 - 03/29/2019"/>
                                    </div>

                                    <script>
                                        $(document).ready(function () {

                                            // init apartment calendar
                                            $("#apart-calendar-input").caleran({
                                                startEmpty: false,
                                                inline: true,
                                                singleDate: false,
                                                startOnMonday: true,
                                                calendarCount: 1,
                                                showHeader: false,
                                                showFooter: false,
                                                enableKeybard: false,
                                                enableMonthSwitcher: false,
                                                disableOnlyStart: true,
                                                disableOnlyEnd: true,
                                                legendFirst: "Free",
                                                legendSecond: "Busy",
                                                oninit: function (caleran) {
                                                    $('.caleran-day, .caleran-year-switch, .caleran-ms-month').unbind(); // disable clicking on date
                                                }
                                            });

                                        });
                                    </script>
                                </div>

                                <!-- Stucture -->
                                <div class="row mb-4" id="apart-step-structure">
                                    <button class="dropdown-button w-100" data-toggle="collapse"
                                            aria-controls="apart-structure" href="#apart-structure" type="button">
                                        Struktura
                                        <span>* Dodatni tekst</span>
                                    </button>
                                    <div class="collapse dropdown-block w-100" id="apart-structure">
                                        <div class="w-100 bg-white pb-5">
                                            <img src="img/apart-structure.jpg" alt="Apartmant Structure"
                                                 style="display:block; margin: 0 auto">
                                        </div>
                                    </div>
                                </div>

                                <!-- Location -->
                                <div class="row mb-4" id="apart-step-location">
                                    <button class="dropdown-button w-100" data-toggle="collapse"
                                            aria-controls="apart-location" href="#apart-location" type="button">Lokacija
                                        <span>Kratak tekst</span>
                                    </button>
                                    <div class="collapse dropdown-block w-100" id="apart-location">

                                        <div id="apart-gmaps"></div>

                                    </div>
                                </div>

                                <!-- Restorani -->
                                <div class="row mb-4">
                                    <button class="dropdown-button w-100" data-toggle="collapse"
                                            aria-controls="apart-nearby" href="#apart-nearby" type="button">Restorani
                                        <span>Kratak tekst</span>
                                    </button>
                                    <div class="collapse dropdown-block w-100" id="apart-nearby">
                                        <div class="w-100">
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="apart-restaurant">
                                                        <img src="img/apart-restaurant-img.jpg" alt=""
                                                             class="apart-restaurant-img">
                                                        <div class="apart-restaurant-info">
                                                            <span>Fine American</span>
                                                            <h3>The Wooly Public</h3>
                                                            <p>Pub fare at an eclectic spot with new and classic
                                                                cocktails</p>
                                                            <h5>About $30 per person</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="apart-restaurant">
                                                        <img src="img/apart-restaurant-img.jpg" alt=""
                                                             class="apart-restaurant-img">
                                                        <div class="apart-restaurant-info">
                                                            <span>Fine American</span>
                                                            <h3>The Wooly Public</h3>
                                                            <p>Pub fare at an eclectic spot with new and classic
                                                                cocktails</p>
                                                            <h5>About $30 per person</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="apart-restaurant">
                                                        <img src="img/apart-restaurant-img.jpg" alt=""
                                                             class="apart-restaurant-img">
                                                        <div class="apart-restaurant-info">
                                                            <span>Fine American</span>
                                                            <h3>The Wooly Public</h3>
                                                            <p>Pub fare at an eclectic spot with new and classic
                                                                cocktails</p>
                                                            <h5>About $30 per person</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="apart-restaurant">
                                                        <img src="img/apart-restaurant-img.jpg" alt=""
                                                             class="apart-restaurant-img">
                                                        <div class="apart-restaurant-info">
                                                            <span>Fine American</span>
                                                            <h3>The Wooly Public</h3>
                                                            <p>Pub fare at an eclectic spot with new and classic
                                                                cocktails</p>
                                                            <h5>About $30 per person</h5>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <!-- Reviews -->
                                <div class="row mb-4" id="apart-step-reviews">
                                    <button class="dropdown-button w-100" data-toggle="collapse"
                                            aria-controls="apart-reviews" href="#apart-reviews" type="button">Ocene
                                        gostiju
                                        <span>Kratak tekst</span>
                                    </button>
                                    <div class="collapse dropdown-block w-100" id="apart-reviews">
                                        <div class="w-100 d-flex apart-reviews bg-white">
                                            <div class="d-flex apart-review-row">
                                                <div class="col-3">Lokacija</div>
                                                <div class="col-4 pl-0">
                                                    <ul class="review-stars rate-stars">
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li><i class="material-icons">grade</i></li>
                                                    </ul>
                                                </div>
                                                <div class="col-3">Vrlo dobro</div>
                                                <div class="col-2">8.3 / 10</div>
                                            </div>
                                            <div class="d-flex apart-review-row">
                                                <div class="col-3">Sobe</div>
                                                <div class="col-4 pl-0">
                                                    <ul class="review-stars rate-stars">
                                                        <ul class="review-stars rate-stars">
                                                            <li class="rated-star"><i class="material-icons">grade</i></li>
                                                            <li class="rated-star"><i class="material-icons">grade</i></li>
                                                            <li class="rated-star"><i class="material-icons">grade</i></li>
                                                            <li class="rated-star"><i class="material-icons">grade</i></li>
                                                            <li><i class="material-icons">grade</i></li>
                                                            <li><i class="material-icons">grade</i></li>
                                                        </ul>
                                                    </ul>
                                                </div>
                                                <div class="col-3">Pristojno</div>
                                                <div class="col-2">7 / 10</div>
                                            </div>
                                            <div class="d-flex apart-review-row">
                                                <div class="col-3">Usluge</div>
                                                <div class="col-4 pl-0">
                                                    <ul class="review-stars rate-stars">
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li><i class="material-icons">grade</i></li>
                                                    </ul>
                                                </div>
                                                <div class="col-3">Vrlo dobro</div>
                                                <div class="col-2">8.3 / 10</div>
                                            </div>
                                            <div class="d-flex apart-review-row">
                                                <div class="col-3">Cistoća</div>
                                                <div class="col-4 pl-0">
                                                    <ul class="review-stars rate-stars">
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li><i class="material-icons">grade</i></li>
                                                    </ul>
                                                </div>
                                                <div class="col-3">Pristojno</div>
                                                <div class="col-2">8.3 / 10</div>
                                            </div>
                                            <div class="d-flex apart-review-row">
                                                <div class="col-3">Vrednost novca</div>
                                                <div class="col-4 pl-0">
                                                    <ul class="review-stars rate-stars">
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li><i class="material-icons">grade</i></li>
                                                    </ul>
                                                </div>
                                                <div class="col-3">Vrlo dobro</div>
                                                <div class="col-2">7 / 10</div>
                                            </div>
                                            <div class="d-flex apart-review-row">
                                                <div class="col-3">Komfor</div>
                                                <div class="col-4 pl-0">
                                                    <ul class="review-stars rate-stars">
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li><i class="material-icons">grade</i></li>
                                                    </ul>
                                                </div>
                                                <div class="col-3">Vrlo dobro</div>
                                                <div class="col-2">8.3 / 10</div>
                                            </div>
                                            <div class="d-flex apart-review-row">
                                                <div class="col-3">Gradjevina</div>
                                                <div class="col-4 pl-0">
                                                    <ul class="review-stars rate-stars">
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li><i class="material-icons">grade</i></li>
                                                    </ul>
                                                </div>
                                                <div class="col-3">Vrlo dobro</div>
                                                <div class="col-2">8.3 / 10</div>
                                            </div>
                                            <div class="d-flex apart-review-row">
                                                <div class="col-3">Okolina</div>
                                                <div class="col-4 pl-0">
                                                    <ul class="review-stars rate-stars">
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li class="rated-star"><i class="material-icons">grade</i></li>
                                                        <li><i class="material-icons">grade</i></li>
                                                    </ul>
                                                </div>
                                                <div class="col-3">Pristojno</div>
                                                <div class="col-2">8.3 / 10</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Comments -->
                                <div class="row mb-4">

                                    <button class="dropdown-button w-100" data-toggle="collapse" aria-controls="apart-comments" href="#apart-comments" type="button">
                                        Komentari
                                        <span>Kratak tekst</span>
                                    </button>

                                    <div class="collapse dropdown-block w-100 fade" id="apart-comments">

                                        <!-- Comment list -->
                                        <div class="apart-comments">
                                            <div class="ap-comment">
                                                <div class="avatar-wrap">
                                                    <div class="comment-avatar">M</div>
                                                </div>
                                                <div><p>“Lorem Ipsum is simply dummy text of the printing and ypesetting
                                                        industry. Lorem Ipsum has been the industry's standard dummy
                                                        text ever since the 1500s, “</p>
                                                </div>
                                            </div>
                                            <div class="ap-comment">
                                                <div class="avatar-wrap">
                                                    <div class="comment-avatar">M</div>
                                                </div>
                                                <div><p>“Lorem Ipsum is simply dummy text of the printing and ypesetting
                                                        industry. Lorem Ipsum has been the industry's standard dummy
                                                        text ever since the 1500s, “</p>
                                                </div>
                                            </div>
                                            <div class="ap-comment">
                                                <div class="avatar-wrap">
                                                    <div class="comment-avatar">M</div>
                                                </div>
                                                <div><p>“Lorem Ipsum is simply dummy text of the printing and ypesetting
                                                        industry. Lorem Ipsum has been the industry's standard dummy
                                                        text ever since the 1500s, “</p>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Form: Add comment -->
                                        <form action="#" id="add-comment" class="write-comment">
                                            <div class="col-1 avatar-wrap m-0 p-0">
                                                <div class="comment-avatar">M</div>
                                            </div>
                                            <div class="col-8 pr-0">
                                                <input type="text" class="comment-input" id="comment-input" placeholder="Reply">
                                            </div>
                                            <div class="col-3 text-right pr-0">
                                                <button class="btn btn-primary" type="submit">Send</button>
                                            </div>
                                        </form>

                                        <!-- Comment success block -->
                                        <div class="comment-success fade" id="comment-success">
                                            <div class="c-success-inner">
                                                <div class="comment-icon"><i class="fas fa-check"></i></div>
                                                <h5 class="text-lightblue mb-3">Hvala</h5>
                                                <h3 class="mb-3">Vas komentar je prosledjen</h3>
                                                <p class="mb-5">Vas komentar ce biti pusten</p>

                                                <button type="button" class="btn btn-white rounded" id="btn-close-comment">Back to Page</button>
                                                <p class="text-lightblue small my-3">if you don't go to the page press button</p>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <form action="" id="form-booking" name="form-booking">

                            <!-- Booking Form -->
                            <div id="booking-form" class="bg-white">

                                <div class="inner-content py-5">

                                    <h4 class="booking-title">Do you want to book apartman?</h4>
                                    <p class="text-medium mb-4">"Neque porro quisquam est qui dolorem ipsum quia
                                        dolor sit amet, consectetur, adipisci velit..."
                                    </p>

                                    <!-- User type switch -->
                                    <div class="d-flex justify-content-start py-4">
                                        <div class="col-3 p-0">
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input" id="booking-usertype-user" name="booking-usertype" checked="checked">
                                                <label class="form-check-label" for="booking-usertype-user">Fizičko lice</label>
                                            </div>
                                        </div>
                                        <div class="col-3 p-0">
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input" id="booking-usertype-company" name="booking-usertype">
                                                <label class="form-check-label" for="booking-usertype-company">Pravno lice</label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- User informations -->
                                    <div class="form-label-group mb-0">
                                        <input type="text" name="booking-fullname" id="booking-fullname" class="form-control with-shadow" required="required" placeholder="Fullname *">
                                        <label for="booking-name">Fullname *</label>
                                    </div>

                                    <div class="form-label-group mb-0">
                                        <input type="text" name="booking-email" id="booking-email" class="form-control with-shadow" required="required" placeholder="Email *">
                                        <label for="booking-email">Email *</label>
                                    </div>

                                    <div class="form-label-group mb-0">
                                        <input type="text" name="booking-mobile" id="booking-mobile" class="form-control with-shadow" required="required" placeholder="Mobile *">
                                        <label for="booking-mobile">Mobile *</label>
                                    </div>

                                    <!-- Company informations -->
                                    <div id="booking-co-info" class="mt-4 mb-5">
                                        <label>Company Informations</label>
                                        <div class="d-flex">
                                            <div class="col p-0">
                                                <div class="form-label-group mb-0">
                                                    <input type="text" name="booking-co-name" id="booking-co-name" class="form-control with-shadow border-right-0" required="required" placeholder="Company name *">
                                                    <label for="booking-name">Company name *</label>
                                                </div>
                                            </div>
                                            <div class="col p-0">
                                                <div class="form-label-group mb-0">
                                                    <input type="text" name="booking-co-number" id="booking-co-number" class="form-control with-shadow" required="required" placeholder="PIB / VAT Number *">
                                                    <label for="booking-name">PIB / VAT Number *</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex mt-4 mb-5">

                                        <div class="col-3 p-0">
                                            <label for="booking-peoples">Broj osoba</label>
                                            <select name="booking-peoples" id="booking-peoples" class="default-select custom-select" data-width="fit">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <div class="col-3 p-0">
                                            <label for="booking-peoples">Dece</label>
                                            <select name="booking-childs" id="booking-childs" class="default-select custom-select" data-width="fit">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>
                                        <div class="col-3 p-0">
                                            <label for="booking-peoples">Pet</label>
                                            <select name="booking-pet" id="booking-pet" class="default-select custom-select" data-width="fit">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                            </select>
                                        </div>

                                    </div>

                                    <!-- Arrival & Going Information -->
                                    <button class="dropdown-button w-100 red-border mt-4" data-toggle="collapse" aria-controls="booking-arrival" href="#booking-arrival">Arrival & Going Information</button>

                                    <div class="dropdown-block w-100 collapse mb-4" id="booking-arrival">

                                        <div id="booking-daypicker" class="cal-custom-head mb-4">
                                            <input type="text" class="form-control" id="form-booking-day"
                                                   name="form-booking-day">
                                        </div>
                                        <script>
                                            $(document).ready(function () {
                                                $("#form-booking-day").caleran({
                                                    inline: true,
                                                    startEmpty: true,
                                                    startDate: null,
                                                    singleDate: false,
                                                    startOnMonday: true,
                                                    calendarCount: 1,
                                                    showHeader: true,
                                                    showFooter: false,
                                                    startEmpty: true,
                                                    legendFirst: "Check-in day",
                                                    legendSecond: "Check-out day",
                                                    dateUpdateContainer: "form-booking-day",
                                                    enableKeybard: false,
                                                    enableMonthSwitcher: true,
                                                    container: ".calendar-ui",
                                                });
                                            });
                                        </script>

                                    </div>

                                    <!-- Time Information -->
                                    <button class="dropdown-button w-100 red-border mt-4" data-toggle="collapse"
                                            aria-controls="booking-time" href="#booking-time">Time Information
                                    </button>

                                    <div class="dropdown-block w-100 collapse" id="booking-time">

                                        <!-- Time picker -->
                                        <div id="booking-timepicker">

                                            <div class="bi-box d-flex border justify-content-between p-4 mb-4">
                                                <div class="booking-info-time">
                                                    <p>Time from</p>
                                                    <div class="d-flex flex-row align-items-end">
                                                        <div class="bi-time"><span id="time-picker-checkin-update">00</span>:00</div>
                                                        <div class="bi-time-label" id="time-picker-checkin-ampm">AM</div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <i class="icon-svg">
                                                        <img src="img/icon-arrow-right.svg" alt="">
                                                    </i>
                                                </div>
                                                <div class="booking-info-time">
                                                    <p>Time to</p>
                                                    <div class="d-flex flex-row align-items-end">
                                                        <div class="bi-time"><span id="time-picker-checkout-update">00</span>:00</div>
                                                        <div class="bi-time-label" id="time-picker-checkout-ampm">AM</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="time-picker-holder">
                                                <input type="text" id="booking-time-checkin" class="timepicker">
                                                <input type="text" id="booking-time-checkout" class="timepicker">

                                                <div id="clockpick-checkin"></div>
                                                <div id="clockpick-checkout"></div>

                                            </div>

                                        </div>

                                    </div>


                                    <!-- Payment -->
                                    <button class="dropdown-button w-100 mt-4" data-toggle="collapse"
                                            aria-controls="apart-payment" href="#apart-payment" type="button">Card - Information
                                    </button>

                                    <div class="collapse dropdown-block w-100 fade" id="apart-payment">

                                        <!-- Payment switcher -->
                                        <div class="payment-switcher block-row block-shadow mb-3 p-0">
                                            <div class="col-4 p-0">
                                                <div class="form-check">
                                                    <input type="radio" class="form-check-input" id="booking-payment_paypal"
                                                           name="booking-payment_method">
                                                    <label class="form-check-label" for="booking-payment_paypal"><img
                                                                src="img/icon-paypal.png" alt="PayPal"></label>
                                                </div>
                                            </div>
                                            <div class="col-4 p-0">
                                                <div class="form-check">
                                                    <input type="radio" class="form-check-input" id="booking-payment_money"
                                                           name="booking-payment_method">
                                                    <label class="form-check-label"
                                                           for="booking-payment_money">Money</label>
                                                </div>
                                            </div>
                                            <div class="col-4 ml-auto p-0 text-right">
                                                <div class="form-check">
                                                    <input type="radio" class="form-check-input"
                                                           id="booking-payment_deposit" name="booking-payment_method">
                                                    <label class="form-check-label" for="booking-payment_deposit">Depozit:
                                                        20%</label>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Creditcard Inputs -->
                                        <div class="d-flex w-100 flex-column">

                                            <div class="d-flex justify-content-between align-items-center my-5 cc-graphic">
                                                <div class="col-md-5 text-center">
                                                    <img src="img/image-mastercard.svg" alt="MasterCard">
                                                </div>
                                                <div class="col-md-7 p-0">
                                                    <img src="img/image-creditcard.png" alt="CreditCard">
                                                </div>
                                            </div>

                                            <form action="" class="w-100" id="input-creditcard">
                                                <div class="form-label-group mb-0 input-group-prepend">
                                                    <i class="far fa-credit-card"></i>
                                                    <input type="number" id="booking-cc-numb" name="booking-cc-numb"
                                                           class="form-control w-100 with-shadow border-bottom-0"
                                                           placeholder="Card Number">
                                                    <label for="booking-cc-numb">Card Information</label>
                                                </div>
                                                <div class="d-flex">
                                                    <div class="col-md-6 col-12 p-0">
                                                        <div class="form-label-group">
                                                            <input type="text"
                                                                   class="form-control with-shadow border-right-0"
                                                                   id="booking-cc-name" placeholder="Card Holder">
                                                            <label for="booking-cc-name">Card Holder</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-6 p-0">
                                                        <div class="form-label-group">
                                                            <input type="text"
                                                                   class="form-control with-shadow border-right-0"
                                                                   id="booking-cc-date" placeholder="Exp. Date">
                                                            <label for="booking-cc-date">Exp. Date</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-6 p-0">
                                                        <div class="form-label-group">
                                                            <input type="text" class="form-control with-shadow"
                                                                   id="booking-cc-date" placeholder="CVC">
                                                            <label for="booking-cc-date">CVC</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>

                                        <!-- Creditcard Confirmation block -->
                                        <div class="big-checkbox-wrapper">
                                            <div class="col-md-9 col-9 p-0">
                                                <div class="custom-control big-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="booking-cc-confirm" data-btn="#btn-cc-confirm">
                                                    <label class="custom-control-label" for="booking-cc-confirm">Do you agree with your information</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3 p-0">
                                                <button type="submit" class="btn btn-lg btn-secondary m-0 w-100 h-100 shadow-none" id="btn-cc-confirm" disabled="disabled">All in</button>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>

                            <!-- Rend-a-car form -->
                            <div id="rent-a-car-form" class="bg-grey">

                                <div class="inner-content py-5">

                                    <div class="d-flex flex-column mb-3">
                                        <h4>Do you want to reserve the car?</h4>
                                        <p class="text-medium">"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."</p>
                                    </div>

                                    <button class="dropdown-button w-100 mt-4 dropdown-black" data-toggle="collapse" aria-controls="booking-rentacar" href="#booking-rentacar" type="button">Rent-a-car information
                                    </button>

                                    <div class="collapse dropdown-block w-100 fade" id="booking-rentacar">

                                        <button class="block-row block-button w-100 mb-4" id="btn-car-frame">
                                            <h6>Izaberi vozilo</h6>
                                            <i class="fas fa-external-link-alt"></i>
                                        </button>

                                        <!-- Selected rent-a-car -->
                                        <div class="" id="selected-car-block">

                                            <div class="selected-car border">

                                                <button id="btn-remove-selected-car" class="">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>

                                                <!-- Car info -->
                                                <div class="col-md-6 col-12 cd-info">
                                                    <div class="text-left">
                                                        <h3>Volvo 985</h3>
                                                        <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus veritus ut eos, amet natum salutandi ad vis.</p>
                                                    </div>
                                                </div>

                                                <!-- Selected car image -->
                                                <div class="col-md-6 col-12">
                                                    <img class="d-block w-100" src="img/cars-volvo2.jpg" alt="">
                                                </div>

                                            </div>

                                            <div class="d-flex selcar-bottom mb-4">
                                                <button class="btn btn-lg btn-primary col-md-6 text-center">
                                                    Pay for day <h6>25€</h6>
                                                </button>
                                                <button class="btn btn-lg btn-secondary col-md-6 m-0">Vidi vise</button>
                                            </div>

                                        </div>

                                        <!-- Car Time/Date -->
                                        <div id="booking-car-date" class="cal-custom-head mb-4">

                                            <div id="booking-car-daypicker" class="cal-custom-head mb-4">
                                                <input type="text" class="form-control" id="form-booking-car-day" name="form-booking-car-day">
                                            </div>
                                            <script>
                                                $(document).ready(function () {
                                                    $("#form-booking-car-day").caleran({
                                                        inline: true,
                                                        startEmpty: true,
                                                        startDate: null,
                                                        singleDate: false,
                                                        startOnMonday: true,
                                                        calendarCount: 1,
                                                        showHeader: true,
                                                        showFooter: false,
                                                        startEmpty: true,
                                                        legendFirst: "Check-in day",
                                                        legendSecond: "Check-out day",
                                                        dateUpdateContainer: "form-booking-car-day",
                                                        enableKeybard: false,
                                                        enableMonthSwitcher: true,
                                                        container: ".calendar-ui",
                                                    });
                                                });
                                            </script>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <!-- Booking confirmation -->
                            <div id="booking-confirmation">

                                <div class="inner-content py-5">

                                    <div class="d-block">
                                        <h3>Now you can reserve it</h3>
                                        <p class="text-medium mb-5">If you have entered your information<br>now you can reyervisete a eented apartment with a car</p>
                                    </div>

                                    <div class="big-checkbox-wrapper">
                                        <div class="col-md-9 col-9 p-0">
                                            <div class="custom-control big-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="checkbox-book-confirm" data-btn="#btn-book-now">
                                                <label class="custom-control-label" for="checkbox-book-confirm">Do you agree with your information</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 p-0">
                                            <button type="submit" class="btn btn-lg btn-secondary m-0 w-100 h-100 shadow-none" id="btn-book-now" disabled="disabled">Book now</button>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </form>

                        <!-- Booking waiting window -->
                        <div id="booking-waiting" class="booking-overlay fade">
                            <div id="booking-confirm-window" class="booking-overlay-inner fade">

                                <div class="d-flex justify-content-center align-items-center w-100 flex-column">

                                    <div id="booking-confirm-loader" class="loading-dots mb-5">
                                        <span class="dot one"></span>
                                        <span class="dot two"></span>
                                        <span class="dot three"></span>
                                    </div>

                                    <h3>Waiting</h3>
                                    <p>Name users</p>
                                    <h4 class="mb-5">Name of apartmans</h4>

                                    <div class="d-flex mt-5 w-50">
                                        <div class="col pr-2">
                                            <button class="btn btn-lg btn-white btn-close w-100 rounded with-shadow"
                                                    data-container="#booking-waiting"
                                                    id="btn-booking-confirm-back">
                                                Back
                                            </button>
                                        </div>
                                        <div class="col pl-2">
                                            <button class="btn btn-lg btn-secondary w-100 rounded with-shadow"
                                                    id="btn-booking-confirm">
                                                Confirm booking
                                            </button>
                                        </div>
                                    </div>

                                    <p class="text-small text-grey mt-3">if you don't go to the page press button</p>
                                </div>

                            </div>
                            <div id="booking-confirmed" class="booking-overlay-inner fade">

                                <div class="d-flex justify-content-center align-items-center w-100 flex-column">

                                    <div class="circle-icon"><i class="fas fa-check"></i></div>

                                    <h5 class="text-lightblue mb-2">Thank you</h5>
                                    <h4 class="mb-3">You booked</h4>
                                    <h2 class="mb-4">Frida</h2>

                                    <div class="mt-5 w-50 text-center">
                                        <a href="view-booking.php" class="btn btn-lg btn-white with-shadow rounded text-secondary">View Checkout payment</a>
                                    </div>

                                    <p class="text-small text-lightblue mt-3">if you don't go to the page press button</p>
                                </div>

                            </div>
                        </div>

                    </div>

    			</div>
    			<div class="col-md-6 col-12 sticky" id="container-right">
                    <?php include('templates/single-apartment-inner.php'); ?>
    			</div>
    		</div>
    	</div>

    </div>

    <!-- Car Gallery Frame -->
    <div id="cars-frame">

        <div class="cars-frame-title">
            <h6>Slobodno vozilo</h6>
            <button type="button" id="btn-close-cars" class="close" aria-label="Close">
                Close <i class="icon-svg secondary-color icon-close"></i>
            </button>
        </div>

        <div class="cars-content w-100 h-100 bg-white align-content-center justify-content-center">

            <!-- Cars brand filter -->
            <ul id="cars-filter">
                <li><a href="#" data-category="mercedes">Mercedes</a></li>
                <li><a href="#" data-category="audi">Audi</a></li>
                <li><a href="#" data-category="bmw">BMW</a></li>
                <li><a href="#" data-category="volvo">Volvo</a></li>
                <li><a href="#" data-category="Opel">Opel</a></li>
                <li><a href="#" data-category="Reno">Reno</a></li>
            </ul>

            <!-- Cars catalog -->
            <div id="cars-catalog" class="d-flex justify-content-between">
                <div id="cars-loader">
                    <div class="preloader-wrapper small active">
                        <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="gap-patch">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="single-car">
                        <div class="sc-price">
                            <p>Pay for day</p>
                            <span>25€</span>
                        </div>
                        <div class="sc-image">
                            <a href="#" class="" data-toggle="modal" data-target="#modal-car-details">
                                <img src="img/cars-volvo.jpg" alt="Volvo">
                            </a>
                        </div>
                        <div class="sc-info">
                            <h4>Volvo 985</h4>
                            <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                veritus ut eos, amet natum salutandi ad vis.</p>
                        </div>
                        <div class="sc-bottom d-flex justify-content-between border-top">
                            <div>
                                <i class="fas fa-gas-pump"></i>
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <a class="btn btn-primary btn-reserve-car" data-car-id="47" href="#">Rezervisi <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="single-car active">
                        <div class="sc-price">
                            <p>Pay for day</p>
                            <span>25€</span>
                        </div>
                        <div class="sc-image">
                            <a href="#" class="btn-car-details" data-toggle="modal" data-target="#modal-car-details">
                                <img src="img/cars-volvo2.jpg" alt="Volvo">
                            </a>
                        </div>
                        <div class="sc-info">
                            <h4>Volvo 985</h4>
                            <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                veritus ut eos, amet natum salutandi ad vis.</p>
                        </div>
                        <div class="sc-bottom d-flex justify-content-between border-top">
                            <div>
                                <i class="fas fa-gas-pump"></i>
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <a class="btn btn-primary btn-reserve-car" data-car-id="32" href="#">U toku ... <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="single-car">
                        <div class="sc-price">
                            <p>Pay for day</p>
                            <span>25€</span>
                        </div>
                        <div class="sc-image">
                            <a href="#" class="" data-toggle="modal" data-target="#modal-car-details">
                                <img src="img/cars-volvo.jpg" alt="Volvo">
                            </a>
                        </div>
                        <div class="sc-info">
                            <h4>Volvo 985</h4>
                            <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                veritus ut eos, amet natum salutandi ad vis.</p>
                        </div>
                        <div class="sc-bottom d-flex justify-content-between border-top">
                            <div>
                                <i class="fas fa-gas-pump"></i>
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <a class="btn btn-primary btn-reserve-car" data-car-id="11" href="#">Rezervisi <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="single-car">
                        <div class="sc-price">
                            <p>Pay for day</p>
                            <span>25€</span>
                        </div>
                        <div class="sc-image">
                            <a href="#" class="" data-toggle="modal" data-target="#modal-car-details">
                                <img src="img/cars-volvo.jpg" alt="Volvo">
                            </a>
                        </div>
                        <div class="sc-info">
                            <h4>Volvo 985</h4>
                            <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                veritus ut eos, amet natum salutandi ad vis.</p>
                        </div>
                        <div class="sc-bottom d-flex justify-content-between border-top">
                            <div>
                                <i class="fas fa-gas-pump"></i>
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <a class="btn btn-primary btn-reserve-car" data-car-id="47" href="#">Rezervisi <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="single-car active">
                        <div class="sc-price">
                            <p>Pay for day</p>
                            <span>25€</span>
                        </div>
                        <div class="sc-image">
                            <a href="#" class="btn-car-details" data-toggle="modal" data-target="#modal-car-details">
                                <img src="img/cars-volvo2.jpg" alt="Volvo">
                            </a>
                        </div>
                        <div class="sc-info">
                            <h4>Volvo 985</h4>
                            <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                veritus ut eos, amet natum salutandi ad vis.</p>
                        </div>
                        <div class="sc-bottom d-flex justify-content-between border-top">
                            <div>
                                <i class="fas fa-gas-pump"></i>
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <a class="btn btn-primary btn-reserve-car" data-car-id="32" href="#">U toku ... <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="single-car">
                        <div class="sc-price">
                            <p>Pay for day</p>
                            <span>25€</span>
                        </div>
                        <div class="sc-image">
                            <a href="#" class="" data-toggle="modal" data-target="#modal-car-details">
                                <img src="img/cars-volvo.jpg" alt="Volvo">
                            </a>
                        </div>
                        <div class="sc-info">
                            <h4>Volvo 985</h4>
                            <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                veritus ut eos, amet natum salutandi ad vis.</p>
                        </div>
                        <div class="sc-bottom d-flex justify-content-between border-top">
                            <div>
                                <i class="fas fa-gas-pump"></i>
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <a class="btn btn-primary btn-reserve-car" data-car-id="11" href="#">Rezervisi <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal car details -->
    <div class="modal fade center align-items-center center-modal" id="modal-car-details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <!-- Add class .modal-side and then add class .modal-top-right (or other classes from list above) to set a position to the modal -->
        <div class="modal-dialog modal-lg car-details" role="document">

            <div class="modal-content">

                <div class="modal-body bg-white p-0">

                    <!-- Car Info + Image -->
                    <div class="car-details-head border-bottom d-flex">

                        <div class="col-md-2 cd-price">
                            <div>
                                <p>Pay for day</p>
                                <h4>25E</h4>
                            </div>
                        </div>

                        <div class="col-md-10 d-flex">

                            <!-- Car info -->
                            <div class="col-md-6 cd-info">
                                <div class="text-left">
                                    <h3>Volvo 985</h3>
                                    <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus veritus ut eos, amet natum salutandi ad vis.</p>
                                </div>
                            </div>

                            <!-- Image slider -->
                            <div class="col-md-6">

                                <!-- Carousel Wrapper -->
                                <div id="carousel-example-1z" class="car-details-slider carousel slide carousel-fade" data-ride="carousel">

                                    <ol class="carousel-indicators">
                                        <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
                                        <li data-target="#carousel-example-1z" data-slide-to="1"></li>
                                        <li data-target="#carousel-example-1z" data-slide-to="2"></li>
                                    </ol>

                                    <!--Slides-->
                                    <div class="carousel-inner" role="listbox">

                                        <div class="carousel-item active">
                                            <img class="d-block w-100" src="img/cars-volvo.jpg" alt="">
                                        </div>

                                        <div class="carousel-item">
                                            <img class="d-block w-100" src="img/cars-volvo2.jpg" alt="">
                                        </div>

                                        <div class="carousel-item">
                                            <img class="d-block w-100" src="img/cars-volvo3.jpg" alt="">
                                        </div>
                                    </div>

                                    <!-- Controls -->
                                    <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="car-details-wrapper tab-content p-0">

                        <!-- Car details table -->
                        <div class="tab-pane fade show active" role="tabpanel" id="cd-details-specs">
                            <div class="car-details-info">
                                <div class="col-md-6 p-0">
                                    <table class="table car-table">
                                        <tr>
                                            <td>
                                                Klima
                                                <i class="fas fa-user"></i>
                                            </td>
                                            <td>Da</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Abs
                                            </td>
                                            <td>Da</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                EPS
                                                <i class="fas fa-car"></i>
                                            </td>
                                            <td>Da</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Radio
                                                <i class="fas fa-gas-pump"></i>
                                            </td>
                                            <td>Da</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Navigacija
                                                <i class="fas fa-car"></i>
                                            </td>
                                            <td>Da</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-6 p-0">
                                    <table class="table car-table">
                                        <tr>
                                            <td>
                                                Klima
                                                <i class="fas fa-user"></i>
                                            </td>
                                            <td>Da</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Abs
                                            </td>
                                            <td>Da</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                EPS
                                                <i class="fas fa-car"></i>
                                            </td>
                                            <td>Da</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Radio
                                                <i class="fas fa-gas-pump"></i>
                                            </td>
                                            <td>Da</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Navigacija
                                                <i class="fas fa-car"></i>
                                            </td>
                                            <td>Da</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!-- Standard features -->
                        <div class="car-details-standard tab-pane fade" role="tabpanel" id="cd-details-standard">Drugi tab</div>

                    </div>

                    <ul class="d-flex cd-tabs m-0 nav border-top" role="tablist">
                        <li class="col">
                            <a class="nav-link cd-block active" data-toggle="tab" id="btn-cd-specs" href="#cd-details-specs" role="tab">
                                <h6>Karakteristike vozila</h6>
                            </a>
                        </li>
                        <li class="col">
                            <a class="nav-link cd-block" data-toggle="tab" id="btn-cd-standard" href="#cd-details-standard" role="tab">
                                <h6>Standardna oprema</h6>
                            </a>
                        </li>
                    </ul>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-lg btn-white" data-dismiss="modal">Belano rent-a-car</button>
                    <button type="button" class="btn btn-lg btn-secondary btn-reserve-car">Rezerviši</button>
                </div>

            </div>
        </div>
    </div>
    <!-- Side Modal Top Right -->

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfSPBFW9HWAOyKbdXE7TLkiXa-honwMsc&callback=initMap" async></script>
    <script>

        function gmapsFullscreen(container) {
            $(container).addClass('gmaps-fullscreen');
            google.maps.event.trigger(map, "resize");
        }

        // Google Maps API
        var map;

        var apartLatLng = { lat: 44.81927132, lng: 20.46501841 };

        function initMap() {

            var markerImage = {
                url: 'img/map-marker.svg',
                size: new google.maps.Size(30, 30),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(10, 30)
            };

            map = new google.maps.Map(document.getElementById('apart-gmaps'), {
                center: apartLatLng,
                zoom: 14,
                fullscreenControl: false,
                disableDefaultUI: true
            });

            // Setup markers
            var marker = new google.maps.Marker({
                position: apartLatLng,
                map: map,
                title: 'Hello World!',
                icon: markerImage
            });

            // marker radius
            var circle = new google.maps.Circle({
                map: map,
                radius: 500,
                center: apartLatLng,
                fillColor: '#AA0000',
                fillOpacity: 0.1,
                strokeColor: '#FF0000',
                strokeOpacity: 0.5,
                strokeWeight: 1,
                fillColor: 'red',
            });
            circle.bindTo('center', marker, 'position');

        }

        $(document).ready(function(){

            preloader();

            // Time pickers
            $('#booking-time-checkin').clockpicker({
                donetext: 'Done',
                autoclose: false,
                wrapper: "#clockpick-checkin",
                twelvehour: true,
                hoursOnly: true,
                autoShow: true,
                clockUpdateDiv: "#time-picker-checkin-update",
                clockUpdateAmPm: "#time-picker-checkin-ampm"
            });

            $('#booking-time-checkin').clockpicker('show');

            $('#booking-time-checkout').clockpicker({
                donetext: 'Done',
                autoclose: false,
                wrapper: "#clockpick-checkout",
                twelvehour: true,
                hoursOnly: true,
                autoShow: true,
                showDone: false,
                clockUpdateDiv: "#time-picker-checkout-update",
                clockUpdateAmPm: "#time-picker-checkout-ampm",
                addClass: 'clockpick-checkout'
            });

            $('#booking-time-checkout').clockpicker('show');

            // BigCheckbox - Booking agreement
            $('.big-checkbox input[type="checkbox"]').change(function () {
                var check_button = $(this, 'input').data('btn');
                if( $(this).prop('checked') == true ) {
                    $(this).parent().addClass('checked');
                    $(check_button).attr('disabled', false).addClass('enabled');
                }
                else {
                    $(this).parent().removeClass('checked');
                    $(check_button).attr('disabled', true);
                }
            });

            $('.inner-scrollable').scroll(function(){
                if ($('.inner-scrollable').scrollTop() >= 150) {
                    $('#booking-top-info').addClass('sticky');
                }
                else {
                    $('#booking-top-info').removeClass('sticky');
                }
            });

            // Usertype switcher ( User / Company )
            $('input[name="booking-usertype"]').change(function () {
                var selectedType = $('input[name="booking-usertype"]:checked').attr('id');
                console.log("Booking: Usertype changed ")
                if( selectedType == "booking-usertype-company" ) {
                    $('#booking-co-info').addClass('active');
                }
                else {
                    $('#booking-co-info').removeClass('active');
                }
            });

            // Payment type switcher
            $('input[name="booking-payment_method"]').change(function () {
                $('.form-check.payment-selected').removeClass('payment-selected');
                $(this).prop("checked", true).parent().addClass('payment-selected');
            });

            // Unselect payment methods if user start using CC inputs
            $('#booking-cc-numb, #booking-cc-holder').keypress(function() {
                $('input[name="booking-payment_method"]').prop('checked', false);
                $('.payment-switcher .form-check').removeClass('payment-selected');
            });

            // Comments
            $('#add-comment').submit(function (e) {
                e.preventDefault();
                // TODO send comment to server
                $('#comment-success').addClass('show');
                $('#comment-input').val(''); // clear comment field
            });

            $('#btn-close-comment').click(function () {
                $('#comment-success').removeClass('show');
            });

            // Show/Hide Cars frame
            $('#btn-car-frame').click(function(e){
                e.preventDefault();
                $('body').toggleClass('cars-frame-open');
            });

            $('#btn-close-cars').click(function(){
                $('body').removeClass('cars-frame-open');
            });

            // Rent-a-car stuff
            $('#cars-filter li a').click(function(e){
                e.preventDefault();

                $('#cars-filter li a').removeClass('active');
                $(this).addClass('active');

                var selected_car_brand = $(this).data('category');

                // open loader
                $('#cars-loader').addClass('open');
                console.log('selected car brand: ' + selected_car_brand);

                // simulate loaded content
                setTimeout(function(){
                    $('#cars-loader').removeClass('open');
                }, 2000);

            });

            // Make car reservation
            $('.btn-reserve-car').click(function(e){
                e.preventDefault();

                $('#modal-car-details').modal('hide');
                $('body').removeClass('cars-frame-open');

                var carID = $(this).data('car-id');
                console.log('Selected car ID: ' + carID);

                $('#selected-car-block').addClass('active'); // show selected car in form
            });

            // Clear selected car
            $('#btn-remove-selected-car').click(function(){
                $('#selected-car-block').removeClass('active');
                console.log('Booking: Selected car removal');
            });

            // Car Details Modal
            $("#modal-car-details").on('show.bs.modal', function(){
                console.log('Load Car details content');
            });

            $('#btn-book-now').click(function(){

                console.log('booking: open confirm window');

                $('body').toggleClass('fixed-window-open');
                $('#booking-waiting').addClass('show');
                $('#booking-confirm-window').addClass('show');
            });

            // open confirmation window
            $('#btn-booking-confirm').click(function(){
                $('#booking-confirmed').addClass('show');
                $('#booking-confirm-window').removeClass('show');
            });

        });

        // ScrollSpy
        // Cache selectors

        // Bind click handler to menu items
        // so we can get a fancy scroll animation

        var lastId,
            topMenu = $("#left-submenu")
            topMenuHeight = topMenu.outerHeight();
            // All list items
            menuItems = topMenu.find("a"),
            // Anchors corresponding to menu items
            scrollItems = menuItems.map(function(){
                var item = $($(this).attr("href"));
                if (item.length) { return item; }
            });

        var topOffset = 300;

        $(document).on('click', '#left-submenu a[href^="#"]', function (e) {
            e.preventDefault();

            $('.inner-scrollable').animate({
                scrollTop: $($.attr(this, 'href')).offset().top - topOffset
            }, 500);

        });

        // Bind to scroll
        $('.inner-scrollable').scroll(function(){

            // Get id of current scroll item
            var cur = scrollItems.map(function(){
                if ($(this).offset().top < topOffset)
                    return this;
            });

            // Get the id of the current element
            cur = cur[cur.length-1];
            var id = cur && cur.length ? cur[0].id : "";

            if (lastId !== id) {
                lastId = id;
                // Set/remove active class
                menuItems
                    .parent().removeClass("active")
                    .end().filter("[href='#"+id+"']").parent().addClass("active");
            }
        });

    </script>

<?php include('footer.php'); ?>