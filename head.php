<!DOCTYPE html>
<html>
  <head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- TODO: Ogg for social networks -->
    <!-- TODO: FavIcons -->

    <title><?php echo $page_title ?></title>

      <link rel="stylesheet" href="fonts/productsans.css">
      <link rel="stylesheet" href="fonts/helvetica-neue.css">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/bootstrap-reboot.css">
      <link rel="stylesheet" href="css/bootstrap-grid.min.css">
      <link rel="stylesheet" href="css/slick.css">

      <!-- Material Design for Bootstrap -->
      <link rel="stylesheet" href="css/mdb.min.css">
      <link rel="stylesheet" href="css/styles.css">
      <link rel="stylesheet" href="css/responsive.css">

      <script src="js/jquery.min.js"></script>

      <!-- Material design Icons -->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <!-- FontAwesome Icons -->
      <link rel="stylesheet" href="css/fontawesome-all.min.css">

      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- Leave those next 4 lines if you care about users using IE8 -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
