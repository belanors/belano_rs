<?php
	$page_title = 'Belano.rs - Contact us';
	include('head.php');
?>

<body id="page-contact">

    <?php include('header.php'); ?>

    <?php include('templates/page-preloader.php'); ?>
    
    <div id="main-container">

    	<div class="container-fluid">

            <div class="row">

                <div class="col-6" id="container-left">

                    <div class="inner-content overflow-y-auto py-5 mt-0 h-100">

                        <!-- Contact form -->
                        <h4 class="page-title">Get in touch</h4>
                        <p class="medium-text text-lightblue">Let's talk</p>

                        <form action="#" id="form-contact" class="needs-validation" novalidate>

                            <div class="form-label-group mb-0">
                                <input type="text" name="contact-fullname" id="contact-fullname"
                                       class="form-control with-shadow" placeholder="Fullname *" required>
                                <label for="contact-name" class="active">Fullname *</label>
                            </div>

                            <div class="form-label-group mb-4">
                                <input type="text" name="contact-email" id="contact-email"
                                       class="form-control with-shadow"
                                       required="required" placeholder="Email *">
                                <label for="contact-email" class="active">Email *</label>
                            </div>

                            <div class="form-label-group mb-0">
                                <input type="text" name="contact-subject" id="contact-subject"
                                       class="form-control with-shadow" required="required" placeholder="Subject">
                                <label for="contact-subject" class="active">Subject</label>
                            </div>

                            <div class="form-label-group mb-2">
                                <textarea type="textarea" name="contact-message" rows="30" id="contact-message"
                                                  class="form-control with-shadow" required="required"
                                                  placeholder="Message"></textarea>
                                <label for="contact-message" class="active">Message</label>
                            </div>

                            <div class="text-right">
                                <button class="btn btn-primary">Send message</button>
                            </div>

                        </form>
                    </div>

                </div>

                <div class="col-6" id="container-right">
                    <div class="contact-img-holder bg-darkblue d-flex align-items-center justify-content-center">
                        <i class="fas fa-image"></i>
                    </div>

                    <div class="py-5">

                        <ul class="contact-info">
                            <li>
                                <span><i class="fas fa-marker"></i></span>
                                <div>
                                    <p>Birčaninova, 11000 Beograd,Srbija</p>
                                    <p>office@belano.rs</p>
                                </div>
                            </li>
                            <li>
                                <span><i class="fas fa-phone"></i></span>
                                <div>
                                    <p>+381 60 55 66 509 , +381 66 55 66 509</p>
                                    <p>+381 11 36 77 773</p>
                                </div>
                            </li>
                            <li>
                                <span><i class="fas fa-globe"></i></span>
                                <div>
                                    <p>http://www.belano.rs/</p>
                                </div>
                            </li>
                        </ul>

                        <p>
                            Tell me about your company<br>
                            In which role do you see me?<br>
                            Do you require me to work locally or remote?
                        </p>

                    </div>
                </div>
            </div>

        </div>

    </div>

    <script>

        // Contact Form validation
        (function() {
            'use strict';
            window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                        $(form).find('input').parent().addClass('invalid-input');
                    }, false);
                });
            }, false);
        })();

    </script>

<?php include('bottom-includes.php'); ?>