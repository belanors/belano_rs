<?php
	$page_title = 'Belano.rs - Single apartman';
	include('head.php');
?>

    <body id="page-single-apartment">

    <?php include('header.php'); ?>
    
    <div class="main-container" data-spy="scroll" data-target="#list-example" data-offset="0">

    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-6" id="container-left">

                    <div id="booking-top-sticky">

                        <!-- Sticky info -->
                        <div id="booking-top-info">
                            <div>

                                <i class="far fa-check-circle"></i>

                                You booked

                                Frida

                                <div>
                                    <i class="fas fa-map-marker-alt"></i>
                                    Slavija, Mekenzijeva
                                </div>

                            </div>
                            <div class="bt-name">Frida</div>
                            <div class="bt-info">2-soban stan, Beograd 65 M2</div>

                        </div>

                    </div>

                    <div class="inner-scrollable">

                        <div id="left-content">

                            <div class="inner-content py-5">

                                <div class="block-row bg-primary">
                                    <h6>Payment & Billing information</h6>
                                </div>

                                <!-- Booking info: Price + Date -->
                                <div class="d-flex bg-primary">

                                    <div class="bookinfo-price"></div>

                                    <div class="bookinfo-date"></div>

                                </div>

                                <!-- Booking info: Map -->
                                <div class="bookinfo-map">

                                </div>

                                <!-- Booking info: Features -->
                                <ul class="feature-icons mt-3 mb-5">
                                    <li class="fe-parking"></li>
                                    <li class="fe-location"></li>
                                    <li class="fe-phone"></li>
                                    <li class="fe-bookmark"></li>
                                    <li class="fe-pool"></li>
                                    <li class="fe-wash"></li>
                                </ul>

                                <p class="text-medium">
                                    Stan se nalazi u Makenzijevoj ulici u istoj zgradi i na istom spratu kao i studio Gentleman. Ispred zgrade postoji mini trg uredjen zelenilom, kao i autobuska stanica sa svim linijama koja vode u samo srce Beograda. Stan Frida je dvorisno orijentisan pa Vam je obezbedjen potpuni mir. U neposrednoj blizini se nalazi Kalenic pijaca i Cuburski park, a Hram Svetog Save je udaljen 10 min lagane setnje.
                                </p>

                                <div id="bookinfo-contact">

                                    <button class="dropdown-button w-100" data-toggle="collapse"
                                            aria-controls="bookinfo-details" href="#bookinfo-details" type="button">
                                        Contact information
                                    </button>

                                    <div class="collapse dropdown-block w-100" id="bookinfo-details">
                                        Details
                                    </div>

                                </div>

                                <!-- Arrival & Going Information -->
                                <button class="dropdown-button w-100 red-border mt-4" data-toggle="collapse"
                                        aria-controls="bookinfo-arrival" href="#bookinfo-arrival">Arrival & Going Information
                                </button>

                                <div class="bi-box d-flex border justify-content-between p-4 my-4">
                                    <div class="booking-info-time">
                                        <p>Check - in</p>
                                        <div class="d-flex flex-row align-items-end">
                                            <div class="bi-time">18</div>
                                            <div class="bi-time-label">
                                                <span>March</span><span>Wednsday</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <i class="icon-svg">
                                            <img src="img/icon-arrow-right.svg" alt="">
                                        </i>
                                    </div>
                                    <div class="booking-info-time">
                                        <p>Check - out</p>
                                        <div class="d-flex flex-row align-items-end">
                                            <div class="bi-time">21</div>
                                            <div class="bi-time-label">
                                                <span>March</span><span>Wednsday</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="dropdown-block w-100 collapse my-4 cal-custom-head" id="bookinfo-arrival">
                                    <input id="bookinfo-date" readonly="readonly" type="text" value="03/22/2019 - 03/29/2019"/>
                                </div>

                                <script>
                                    $(document).ready(function () {

                                        // init apartment calendar
                                        $("#bookinfo-date").caleran({
                                            startEmpty: false,
                                            inline: true,
                                            singleDate: false,
                                            startOnMonday: true,
                                            calendarCount: 1,
                                            showHeader: false,
                                            showFooter: false,
                                            enableKeybard: false,
                                            enableMonthSwitcher: false,
                                            disableOnlyStart: true,
                                            disableOnlyEnd: true,
                                            legendFirst: "Check - in",
                                            legendSecond: "Check - out",
                                            oninit: function (caleran) {
                                                $('.caleran-day, .caleran-year-switch, .caleran-ms-month').unbind(); // disable clicking on date
                                            }
                                        });

                                    });
                                </script>

                                <!-- Time Information -->
                                <button class="dropdown-button w-100 red-border mt-4" data-toggle="collapse"
                                        aria-controls="bookinfo-time" href="#bookinfo-time">Time Information
                                </button>

                                <div class="dropdown-block w-100 collapse my-4" id="bookinfo-time">
                                    <div class="bi-box d-flex border justify-content-between p-4 mb-4">
                                        <div class="booking-info-time">
                                            <p>Time from</p>
                                            <div class="d-flex flex-row align-items-end">
                                                <div class="bi-time">13:00</div>
                                                <div class="bi-time-label">AM</div>
                                            </div>
                                        </div>
                                        <div>
                                            <i class="icon-svg">
                                                <img src="img/icon-arrow-right.svg" alt="">
                                            </i>
                                        </div>
                                        <div class="booking-info-time">
                                            <p>Time to</p>
                                            <div class="d-flex flex-row align-items-end">
                                                <div class="bi-time">15:00</div>
                                                <div class="bi-time-label">PM</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- Apartmant details -->
                        <div id="apart-block-info" class="container bg-grey p-0" data-spy="scroll"
                             data-target="#left-submenu" data-offset="0">

                            <div class="inner-content position-relative py-5">

                                <div class="d-flex flex-column mb-3">
                                    <h4>Do you want to book apartman?</h4>
                                    <p class="text-medium">"Neque porro quisquam est qui dolorem ipsum quia dolor sit
                                        amet, consectetur, adipisci velit..."</p>
                                </div>

                                <div class="d-flex flex-column mb-5">
                                    <button class="dropdown-button w-100" data-toggle="collapse" aria-controls=""
                                            href="#" type="button">Grejanje
                                    </button>
                                    <button class="dropdown-button w-100" data-toggle="collapse" aria-controls=""
                                            href="#" type="button">Internet
                                    </button>
                                    <button class="dropdown-button w-100" data-toggle="collapse" aria-controls=""
                                            href="#" type="button">Parking
                                    </button>
                                    <button class="dropdown-button w-100" data-toggle="collapse" aria-controls=""
                                            href="#" type="button">Televizor
                                    </button>
                                </div>

                                <!-- Pricelist -->
                                <div class="d-flex flex-column" id="apart-step-pricelist">
                                    <button class="dropdown-button w-100" data-toggle="collapse"
                                            aria-controls="apart-pricelist" href="#apart-pricelist" type="button">Cene
                                        <span>*Broj osoba se naplacuje</span>
                                    </button>

                                    <div class="collapse dropdown-block w-100" id="apart-pricelist">

                                        <div class="table-pricelist table-flex bg-white text-center my-3">
                                            <div class="table-row">
                                                <div class="table-td td-header">Broj ljudi</div>
                                                <div class="table-td">1-2</div>
                                                <div class="table-td">1-2</div>
                                                <div class="table-td">1-2</div>
                                                <div class="table-td">1-2</div>
                                            </div>
                                            <div class="table-row">
                                                <div class="table-td td-header">1-3</div>
                                                <div class="table-td">55</div>
                                                <div class="table-td">55</div>
                                                <div class="table-td">55</div>
                                                <div class="table-td">55</div>
                                            </div>
                                            <div class="table-row">
                                                <div class="table-td td-header">4-7</div>
                                                <div class="table-td">53</div>
                                                <div class="table-td">53</div>
                                                <div class="table-td">53</div>
                                                <div class="table-td">53</div>
                                            </div>
                                            <div class="table-row">
                                                <div class="table-td td-header">8-15</div>
                                                <div class="table-td">56</div>
                                                <div class="table-td">56</div>
                                                <div class="table-td">56</div>
                                                <div class="table-td">56</div>
                                            </div>
                                            <div class="table-row">
                                                <div class="table-td td-header">16-30</div>
                                                <div class="table-td">53</div>
                                                <div class="table-td">53</div>
                                                <div class="table-td">53</div>
                                                <div class="table-td">53</div>
                                            </div>
                                        </div>
                                        <p class="medium-text small text-grey my-4 ml-5">* Lorem Ipsum is simply dummy
                                            text of the printing and typesetting industry. Lore Ipsum has been the
                                            industry's standard dummy text ever since the 1500s, when an unknown printer
                                            took a galley of type and scrambled it to make</p>

                                    </div>
                                </div>

                                <!-- Location -->
                                <div class="row mb-4" id="apart-step-location">
                                    <button class="dropdown-button w-100" data-toggle="collapse"
                                            aria-controls="apart-location" href="#apart-location" type="button">Lokacija
                                        <span>Kratak tekst</span>
                                    </button>
                                    <div class="collapse dropdown-block w-100" id="apart-location">
                                        <div class="w-100">
                                            <img src="img/apart-location.jpg" alt="Apartmant Location"
                                                 style="width: 100%; height: auto">
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>

                        <!-- Booking Form -->
                        <div id="booking-form" class="mt-5 bg-white">

                            <div class="inner-content">

                                <h4 class="booking-title">Do you want to book apartman?</h4>
                                <p class="text-medium mb-4">"Neque porro quisquam est qui dolorem ipsum quia
                                    dolor sit amet, consectetur, adipisci velit..."</p>

                                <div class="form-label-group mb-0">
                                    <input type="text" name="booking-fullname" id="booking-fullname"
                                           class="form-control with-shadow" required="required"
                                           placeholder="Fullname *">
                                    <label for="booking-name">Fullname *</label>
                                </div>

                                <div class="form-label-group mb-0">
                                    <input type="text" name="booking-email" id="booking-email"
                                           class="form-control with-shadow" required="required"
                                           placeholder="Email *">
                                    <label for="booking-email">Email *</label>
                                </div>

                                <div class="form-label-group mb-0">
                                    <input type="text" name="booking-mobile" id="booking-mobile"
                                           class="form-control with-shadow" required="required"
                                           placeholder="Mobile *">
                                    <label for="booking-mobile">Mobile *</label>
                                </div>

                                <div class="d-flex mt-4 mb-5">

                                    <div class="col-3 p-0">
                                        <label for="booking-peoples">Broj osoba</label>
                                        <select name="booking-peoples" id="booking-peoples"
                                                class="browser-default custom-select">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="col-3 p-0">
                                        <label for="booking-peoples">Dece</label>
                                        <select name="booking-childs" id="booking-childs"
                                                class="browser-default custom-select">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                    <div class="col-3 p-0">
                                        <label for="booking-peoples">Pet</label>
                                        <select name="booking-pet" id="booking-pet"
                                                class="browser-default custom-select">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>

                                </div>

                                <!-- Arrival & Going Information -->
                                <button class="dropdown-button w-100 red-border mt-4" data-toggle="collapse"
                                        aria-controls="booking-arrival" href="#booking-arrival">Arrival & Going
                                    Information
                                </button>


                                <div class="dropdown-block w-100 collapse mb-4" id="booking-arrival">

                                    <div id="booking-daypicker" class="cal-custom-head mb-4">
                                        <input type="text" class="form-control" id="form-booking-day"
                                               name="form-booking-day">
                                    </div>
                                    <script>
                                        $(document).ready(function () {
                                            $("#form-booking-day").caleran({
                                                inline: true,
                                                startEmpty: true,
                                                startDate: null,
                                                singleDate: false,
                                                startOnMonday: true,
                                                calendarCount: 1,
                                                showHeader: true,
                                                showFooter: false,
                                                startEmpty: true,
                                                legendFirst: "Check-in day",
                                                legendSecond: "Check-out day",
                                                dateUpdateContainer: "form-booking-day",
                                                enableKeybard: false,
                                                enableMonthSwitcher: true,
                                                container: ".calendar-ui",
                                            });
                                        });
                                    </script>

                                </div>

                                <!-- Time Information -->
                                <button class="dropdown-button w-100 red-border mt-4" data-toggle="collapse"
                                        aria-controls="booking-time" href="#booking-time">Time Information
                                </button>

                                <div class="dropdown-block w-100 collapse" id="booking-time">

                                    <!-- Time picker -->
                                    <div id="booking-timepicker">

                                        <div class="bi-box d-flex border justify-content-between p-4 mb-4">
                                            <div class="booking-info-time">
                                                <p>Time from</p>
                                                <div class="d-flex flex-row align-items-end">
                                                    <div class="bi-time">13:00</div>
                                                    <div class="bi-time-label">AM</div>
                                                </div>
                                            </div>
                                            <div>
                                                <i class="icon-svg">
                                                    <img src="img/icon-arrow-right.svg" alt="">
                                                </i>
                                            </div>
                                            <div class="booking-info-time">
                                                <p>Time to</p>
                                                <div class="d-flex flex-row align-items-end">
                                                    <div class="bi-time">15:00</div>
                                                    <div class="bi-time-label">PM</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <!-- Payment -->
                                <button class="dropdown-button w-100 mt-4" data-toggle="collapse"
                                        aria-controls="apart-payment" href="#apart-payment" type="button">Card -
                                    Information

                                </button>
                                <div class="collapse dropdown-block w-100 fade" id="apart-payment">

                                    <!-- Payment switcher -->
                                    <div class="payment-switcher block-row block-shadow my-3 p-0">
                                        <div class="col-4 p-0">
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input" id="booking-payment_paypal"
                                                       name="booking-payment_method">
                                                <label class="form-check-label" for="booking-payment_paypal"><img
                                                            src="img/icon-paypal.png" alt="PayPal"></label>
                                            </div>
                                        </div>
                                        <div class="col-4 p-0">
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input" id="booking-payment_money"
                                                       name="booking-payment_method">
                                                <label class="form-check-label"
                                                       for="booking-payment_money">Money</label>
                                            </div>
                                        </div>
                                        <div class="col-4 ml-auto p-0 text-right">
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input"
                                                       id="booking-payment_deposit" name="booking-payment_method">
                                                <label class="form-check-label" for="booking-payment_deposit">Depozit:
                                                    20%</label>
                                            </div>
                                        </div>
                                    </div>

                                    <script>
                                        $(document).ready(function () {
                                            $('input[name="booking-payment_method"]').change(function () {
                                                $('.form-check.payment-selected').removeClass('payment-selected');
                                                $(this).prop("checked", true).parent().addClass('payment-selected');
                                            })
                                        });
                                    </script>

                                    <!-- Creditcard Inputs -->
                                    <div class="d-flex w-100 flex-column">

                                        <div class="d-flex justify-content-between align-items-center my-5 cc-graphic">
                                            <div class="col-md-5 text-center">
                                                <img src="img/image-mastercard.svg" alt="MasterCard">
                                            </div>
                                            <div class="col-md-7 p-0">
                                                <img src="img/image-creditcard.png" alt="CreditCard">
                                            </div>
                                        </div>

                                        <form action="" class="w-100" id="input-creditcard">
                                            <div class="form-label-group mb-0 input-group-prepend">
                                                <i class="far fa-credit-card"></i>
                                                <input type="text" id="booking-cc-numb" name="booking-cc-numb"
                                                       class="form-control w-100 with-shadow border-bottom-0"
                                                       placeholder="Card Number">
                                                <label for="booking-cc-numb">Card Information</label>
                                            </div>
                                            <div class="d-flex">
                                                <div class="col-md-6 col-12 p-0">
                                                    <div class="form-label-group">
                                                        <input type="text"
                                                               class="form-control with-shadow border-right-0"
                                                               id="booking-cc-name" placeholder="Card Holder">
                                                        <label for="booking-cc-name">Card Holder</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-6 p-0">
                                                    <div class="form-label-group">
                                                        <input type="text"
                                                               class="form-control with-shadow border-right-0"
                                                               id="booking-cc-date" placeholder="Exp. Date">
                                                        <label for="booking-cc-date">Exp. Date</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-6 p-0">
                                                    <div class="form-label-group">
                                                        <input type="text" class="form-control with-shadow"
                                                               id="booking-cc-date" placeholder="CVC">
                                                        <label for="booking-cc-date">CVC</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </div>

                                    <div class="block-row block-shadow p-0">
                                        <div class="col-9 p-0">
                                            <div class="form-check p-3">
                                                <input type="checkbox" class="form-check-input" id="booking-agree">
                                                <label class="form-check-label" for="booking-agree">Do you agree with
                                                    your information</label>
                                            </div>
                                        </div>
                                        <div class="col-3 p-0 text-right border-left">
                                            <div class="form-check p-3 text-right">
                                                <input type="checkbox" class="form-check-input" id="booking-allin"
                                                       checked>
                                                <label class="form-check-label" for="booking-allin">All-in</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <!-- Rend-a-car form -->
                        <div id="rent-a-car-form">

                            <div class="inner-content pt-5">

                                <div class="d-flex flex-column mb-3">
                                    <h4>Do you want to reserve the car?</h4>
                                    <p class="text-medium">"Neque porro quisquam est qui dolorem ipsum quia dolor sit
                                        amet,
                                        consectetur, adipisci velit..."</p>
                                </div>

                                <button class="block-row block-button w-100" id="btn-car-frame">
                                    <h6>Izaberi vozilo</h6>
                                    <i class="fas fa-external-link-alt"></i>
                                </button>

                                <!-- Selected rent-a-car -->
                                <div class="" id="selected-car-block">

                                    <div class="selected-car border">

                                        <button id="btn-remove-selected-car" class="">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>

                                        <!-- Car info -->
                                        <div class="col-md-6 col-12 cd-info">
                                            <div class="text-left">
                                                <h3>Volvo 985</h3>
                                                <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                                    veritus ut eos, amet natum salutandi ad vis.</p></div>
                                        </div>

                                        <!-- Selected car image -->
                                        <div class="col-md-6 col-12">
                                            <img class="d-block w-100" src="img/cars-volvo2.jpg" alt="">
                                        </div>

                                    </div>

                                    <div class="d-flex selcar-bottom mb-4">
                                        <button class="btn btn-lg btn-primary col-md-6 text-center">
                                            Pay for day <h6>25€</h6>
                                        </button>
                                        <button class="btn btn-lg btn-secondary col-md-6 m-0">Vidi vise</button>
                                    </div>

                                    <!-- Car Time/Date -->
                                    <div id="booking-car-date" class="cal-custom-head mb-4">

                                        <div id="booking-car-daypicker" class="cal-custom-head mb-4">
                                            <input type="text" class="form-control" id="form-booking-car-day"
                                                   name="form-booking-car-day">
                                        </div>
                                        <script>
                                            $(document).ready(function () {
                                                $("#form-booking-car-day").caleran({
                                                    inline: true,
                                                    startEmpty: true,
                                                    startDate: null,
                                                    singleDate: false,
                                                    startOnMonday: true,
                                                    calendarCount: 1,
                                                    showHeader: true,
                                                    showFooter: false,
                                                    startEmpty: true,
                                                    legendFirst: "Check-in day",
                                                    legendSecond: "Check-out day",
                                                    dateUpdateContainer: "form-booking-car-day",
                                                    enableKeybard: false,
                                                    enableMonthSwitcher: true,
                                                    container: ".calendar-ui",
                                                });
                                            });
                                        </script>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <!-- Booking confirmation -->
                        <div id="booking-confirmation">

                            <div class="inner-content py-5">

                                <div class="d-block">
                                    <h3>Now you can reserve it</h3>
                                    <p class="text-medium mb-5">If you have entered your information<br>now you can
                                        reyervisete a eented apartment with a car</p>
                                </div>

                                <div class="d-flex p-0">
                                    <div class="col-md-9 col-9 p-0 bg-darkblue text-white align-items-center p-4">
                                        <i class="far fa-check-circle mr-2"></i> Do you agree with your information
                                    </div>
                                    <div class="col-md-3 p-0">
                                        <button class="btn btn-lg btn-secondary m-0 w-100 h-100 shadow-none"
                                                id="btn-book-now">Book now
                                        </button>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <!-- Booking waiting window -->
                        <div id="booking-waiting" class="booking-overlay fade">

                            <div id="booking-confirm-window" class="booking-overlay-inner fade">

                                <div class="d-flex justify-content-center align-items-center w-100 flex-column">

                                    <div id="booking-confirm-loader" class="loading-dots mb-5">
                                        <span class="dot one"></span>
                                        <span class="dot two"></span>
                                        <span class="dot three"></span>
                                    </div>

                                    <h3>Waiting</h3>
                                    <p>Name users</p>
                                    <h4 class="mb-5">Name of apartmans</h4>

                                    <div class="d-flex mt-5 w-50">
                                        <div class="col pr-2">
                                            <button class="btn btn-lg btn-white btn-close w-100 rounded with-shadow"
                                                    data-container="#booking-waiting"
                                                    id="btn-booking-confirm-back">
                                                Back
                                            </button>
                                        </div>
                                        <div class="col pl-2">
                                            <button class="btn btn-lg btn-secondary w-100 rounded with-shadow"
                                                    id="btn-booking-confirm">
                                                Confirm booking
                                            </button>
                                        </div>
                                    </div>

                                    <p class="text-small text-grey mt-3">if you don't go to the page press button</p>
                                </div>

                            </div>

                            <div id="booking-confirmed" class="booking-overlay-inner fade">

                                <div class="d-flex justify-content-center align-items-center w-100 flex-column">

                                    <div class="circle-icon"><i class="fas fa-check"></i></div>

                                    <h5 class="text-lightblue mb-2">Thank you</h5>
                                    <h4 class="mb-3">You booked</h4>
                                    <h2 class="mb-4">Frida</h2>

                                    <div class="mt-5 w-50 text-center">
                                        <a href="/view-booking.php" class="btn btn-lg btn-white with-shadow rounded text-secondary">View Checkout payment</a>
                                    </div>

                                    <p class="text-small text-lightblue mt-3">if you don't go to the page press button</p>
                                </div>

                            </div>

                        </div>

                    </div>

    			</div>

    			<div class="col-6 sticky" id="container-right">
                    <?php include('templates/single-apartment-inner.php'); ?>
    			</div>
    		</div>
    	</div>

    </div>

    <!-- Car Gallery Frame -->
    <div id="cars-frame">

        <div class="cars-frame-title bg-black w-100">
            <h6>Slobodno vozilo</h6>
            <button type="button" id="btn-close-cars" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="cars-content w-100 h-100 bg-white align-content-center justify-content-center">

            <ul id="cars-filter">
                <li><a href="#" data-category="mercedes">Mercedes</a></li>
                <li><a href="#" data-category="audi">Audi</a></li>
                <li><a href="#" data-category="bmw">BMW</a></li>
                <li><a href="#" data-category="volvo">Volvo</a></li>
                <li><a href="#" data-category="Opel">Opel</a></li>
                <li><a href="#" data-category="Reno">Reno</a></li>
            </ul>

            <div id="cars-catalog" class="d-flex justify-content-between">

                <div id="cars-loader">
                    <div class="preloader-wrapper small active">
                        <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="gap-patch">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-12">
                    <div class="single-car">
                        <div class="sc-price">
                            <p>Pay for day</p>
                            <span>25€</span>
                        </div>
                        <div class="sc-image">
                            <a href="#" class="" data-toggle="modal" data-target="#modal-car-details">
                                <img src="img/cars-volvo.jpg" alt="Volvo">
                            </a>
                        </div>
                        <div class="sc-info">
                            <h4>Volvo 985</h4>
                            <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                veritus ut eos, amet natum salutandi ad vis.</p>
                        </div>
                        <div class="sc-bottom d-flex justify-content-between border-top">
                            <div>
                                <i class="fas fa-gas-pump"></i>
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <a class="btn btn-primary btn-reserve-car" data-car-id="47" href="#">Rezervisi <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="single-car active">
                        <div class="sc-price">
                            <p>Pay for day</p>
                            <span>25€</span>
                        </div>
                        <div class="sc-image">
                            <a href="#" class="btn-car-details" data-toggle="modal" data-target="#modal-car-details">
                                <img src="img/cars-volvo2.jpg" alt="Volvo">
                            </a>
                        </div>
                        <div class="sc-info">
                            <h4>Volvo 985</h4>
                            <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                veritus ut eos, amet natum salutandi ad vis.</p>
                        </div>
                        <div class="sc-bottom d-flex justify-content-between border-top">
                            <div>
                                <i class="fas fa-gas-pump"></i>
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <a class="btn btn-primary btn-reserve-car" data-car-id="32" href="#">U toku ... <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="single-car">
                        <div class="sc-price">
                            <p>Pay for day</p>
                            <span>25€</span>
                        </div>
                        <div class="sc-image">
                            <a href="#" class="" data-toggle="modal" data-target="#modal-car-details">
                                <img src="img/cars-volvo.jpg" alt="Volvo">
                            </a>
                        </div>
                        <div class="sc-info">
                            <h4>Volvo 985</h4>
                            <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                veritus ut eos, amet natum salutandi ad vis.</p>
                        </div>
                        <div class="sc-bottom d-flex justify-content-between border-top">
                            <div>
                                <i class="fas fa-gas-pump"></i>
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <a class="btn btn-primary btn-reserve-car" data-car-id="11" href="#">Rezervisi <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="single-car">
                        <div class="sc-price">
                            <p>Pay for day</p>
                            <span>25€</span>
                        </div>
                        <div class="sc-image">
                            <a href="#" class="" data-toggle="modal" data-target="#modal-car-details">
                                <img src="img/cars-volvo.jpg" alt="Volvo">
                            </a>
                        </div>
                        <div class="sc-info">
                            <h4>Volvo 985</h4>
                            <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                veritus ut eos, amet natum salutandi ad vis.</p>
                        </div>
                        <div class="sc-bottom d-flex justify-content-between border-top">
                            <div>
                                <i class="fas fa-gas-pump"></i>
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <a class="btn btn-primary btn-reserve-car" data-car-id="47" href="#">Rezervisi <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="single-car active">
                        <div class="sc-price">
                            <p>Pay for day</p>
                            <span>25€</span>
                        </div>
                        <div class="sc-image">
                            <a href="#" class="btn-car-details" data-toggle="modal" data-target="#modal-car-details">
                                <img src="img/cars-volvo2.jpg" alt="Volvo">
                            </a>
                        </div>
                        <div class="sc-info">
                            <h4>Volvo 985</h4>
                            <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                veritus ut eos, amet natum salutandi ad vis.</p>
                        </div>
                        <div class="sc-bottom d-flex justify-content-between border-top">
                            <div>
                                <i class="fas fa-gas-pump"></i>
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <a class="btn btn-primary btn-reserve-car" data-car-id="32" href="#">U toku ... <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-12">
                    <div class="single-car">
                        <div class="sc-price">
                            <p>Pay for day</p>
                            <span>25€</span>
                        </div>
                        <div class="sc-image">
                            <a href="#" class="" data-toggle="modal" data-target="#modal-car-details">
                                <img src="img/cars-volvo.jpg" alt="Volvo">
                            </a>
                        </div>
                        <div class="sc-info">
                            <h4>Volvo 985</h4>
                            <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus
                                veritus ut eos, amet natum salutandi ad vis.</p>
                        </div>
                        <div class="sc-bottom d-flex justify-content-between border-top">
                            <div>
                                <i class="fas fa-gas-pump"></i>
                                <i class="fas fa-user-friends"></i>
                            </div>
                            <a class="btn btn-primary btn-reserve-car" data-car-id="11" href="#">Rezervisi <i class="fas fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <!-- Modal car details -->
    <div class="modal fade center align-items-center center-modal" id="modal-car-details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <!-- Add class .modal-side and then add class .modal-top-right (or other classes from list above) to set a position to the modal -->
        <div class="modal-dialog modal-lg car-details" role="document">

            <div class="modal-content">

                <div class="modal-body bg-white p-0">

                    <!-- Car Info + Image -->
                    <div class="car-details-head border-bottom d-flex">

                        <div class="col-md-2 cd-price">
                            <div>
                                <p>Pay for day</p>
                                <h4>25E</h4>
                            </div>
                        </div>

                        <div class="col-md-10 d-flex">

                            <!-- Car info -->
                            <div class="col-md-6 cd-info">
                                <div class="text-left">
                                    <h3>Volvo 985</h3>
                                    <p>Lorem ipsum dolor sit amet, id doctus feugait ius, movet ornatus veritus ut eos,
                                        amet natum salutandi ad vis.</p></div>
                            </div>

                            <!-- Image slider -->
                            <div class="col-md-6">

                                <!-- Carousel Wrapper -->
                                <div id="carousel-example-1z" class="car-details-slider carousel slide carousel-fade" data-ride="carousel">

                                    <ol class="carousel-indicators">
                                        <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
                                        <li data-target="#carousel-example-1z" data-slide-to="1"></li>
                                        <li data-target="#carousel-example-1z" data-slide-to="2"></li>
                                    </ol>

                                    <!--Slides-->
                                    <div class="carousel-inner" role="listbox">

                                        <div class="carousel-item active">
                                            <img class="d-block w-100" src="img/cars-volvo.jpg" alt="">
                                        </div>

                                        <div class="carousel-item">
                                            <img class="d-block w-100" src="img/cars-volvo2.jpg" alt="">
                                        </div>

                                        <div class="carousel-item">
                                            <img class="d-block w-100" src="img/cars-volvo3.jpg" alt="">
                                        </div>
                                    </div>

                                    <!-- Controls -->
                                    <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>

                            </div>

                        </div>
                    </div>

                    <!-- Car details table -->
                    <div class="car-details-info d-flex">
                        <div class="col-md-6 p-0">
                            <table class="table car-table">
                                <tr>
                                    <td>
                                        Klima
                                        <i class="fas fa-user"></i>
                                    </td>
                                    <td>Da</td>
                                </tr>
                                <tr>
                                    <td>
                                        Abs
                                    </td>
                                    <td>Da</td>
                                </tr>
                                <tr>
                                    <td>
                                        EPS
                                        <i class="fas fa-car"></i>
                                    </td>
                                    <td>Da</td>
                                </tr>
                                <tr>
                                    <td>
                                        Radio
                                        <i class="fas fa-gas-pump"></i>
                                    </td>
                                    <td>Da</td>
                                </tr>
                                <tr>
                                    <td>
                                        Navigacija
                                        <i class="fas fa-car"></i>
                                    </td>
                                    <td>Da</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6 p-0">
                            <table class="table car-table">
                                <tr>
                                    <td>
                                        Klima
                                        <i class="fas fa-user"></i>
                                    </td>
                                    <td>Da</td>
                                </tr>
                                <tr>
                                    <td>
                                        Abs
                                    </td>
                                    <td>Da</td>
                                </tr>
                                <tr>
                                    <td>
                                        EPS
                                        <i class="fas fa-car"></i>
                                    </td>
                                    <td>Da</td>
                                </tr>
                                <tr>
                                    <td>
                                        Radio
                                        <i class="fas fa-gas-pump"></i>
                                    </td>
                                    <td>Da</td>
                                </tr>
                                <tr>
                                    <td>
                                        Navigacija
                                        <i class="fas fa-car"></i>
                                    </td>
                                    <td>Da</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="col-md-6 bg-darkblue cd-block">
                            <h6>Karakteristike vozila</h6>
                        </div>
                        <div class="col-md-6 cd-block">
                            <h6>Standardna oprema</h6>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-lg btn-white" data-dismiss="modal">Belano rent-a-car</button>
                    <button type="button" class="btn btn-lg btn-secondary btn-reserve-car">Rezerviši</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Side Modal Top Right -->

    <script>

        $(document).ready(function(){

            $('.inner-scrollable').scroll(function(){
                console.log(
                    // $('.inner-scrollable').scrollTop();
                );

                if ($('.inner-scrollable').scrollTop() >= 150) {
                    $('#booking-top-info').addClass('sticky');
                }
                else {
                    $('#booking-top-info').removeClass('sticky');
                }
            });

            $('#btn-car-frame').click(function(e){
                e.preventDefault();
                $('body').toggleClass('cars-frame-open');
            });
            $('#btn-close-cars').click(function(){
                $('body').removeClass('cars-frame-open');
            });

            $("#booking-top-info").sticky({
                 stickyClass: 'sticky',
                 startScrolling: 'top'
            });

            // $("#container-right").sticky({
            //     stopper: "#sticky-stopper",
            //     stickyClass: "sticky"
            // });

            // Rent-a-car stuff
            $('#cars-filter li a').click(function(e){
                e.preventDefault();

                // $('#cars-filter li a').removeClass('acitve');
                $(this).addClass('active');

                var selected_car_brand = $(this).data('category');

                // open loader
                $('#cars-loader').addClass('open');

                console.log('selected car brand: ' + selected_car_brand);

                $('#cars-loader').removeClass('open');

            });

            // Make car reservation
            $('.btn-reserve-car').click(function(e){
                e.preventDefault();

                $('#modal-car-details').modal('hide');
                $('body').removeClass('cars-frame-open');

                var carID = $(this).data('car-id');
                console.log('Selected car ID: ' + carID);

                $('#selected-car-block').addClass('active'); // show selected car in form
            });

            // Clear selected car
            $('#btn-remove-selected-car').click(function(){

                alert('selected car removal');

            })

            // Car Details Modal
            $("#modal-car-details").on('show.bs.modal', function(){
                console.log('Load Car details content');
            });

            $('#btn-book-now').click(function(){

                console.log('booking: open confirm window');

                $('body').toggleClass('fixed-window-open');
                $('#booking-waiting').addClass('show');
                $('#booking-confirm-window').addClass('show');

            });

            $('#btn-booking-confirm').click(function(){

                // open confirmation window
                $('#booking-confirmed').addClass('show');
                $('#booking-confirm-window').removeClass('show');

            })

        });

        // ScrollSpy
        // Cache selectors
        var lastId,
            topMenu = $("#left-submenu"),
            topMenuHeight = topMenu.outerHeight()+15,
            // All list items
            menuItems = topMenu.find("a"),
            // Anchors corresponding to menu items
            scrollItems = menuItems.map(function(){
                var item = $($(this).attr("href"));
                if (item.length) { return item; }
            });

        // Bind click handler to menu items
        // so we can get a fancy scroll animation
        menuItems.click(function(e){
            var href = $(this).attr("href"),
                offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
            $('html, body').stop().animate({
                scrollTop: offsetTop - 185
            }, 300);
            e.preventDefault();
        });

        // Bind to scroll
        $(window).scroll(function(){
            // Get container scroll position
            var fromTop = $(this).scrollTop()+topMenuHeight;

            // Get id of current scroll item
            var cur = scrollItems.map(function(){
                if ($(this).offset().top < fromTop)
                    return this;
            });
            // Get the id of the current element
            cur = cur[cur.length-1];
            var id = cur && cur.length ? cur[0].id : "";

            if (lastId !== id) {
                lastId = id;
                // Set/remove active class
                menuItems
                    .parent().removeClass("active")
                    .end().filter("[href='#"+id+"']").parent().addClass("active");
            }
        });

    </script>

<?php include('footer.php'); ?>