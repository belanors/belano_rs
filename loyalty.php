
<?php
    $page_title = 'Belano.rs - Loyalty';
    include('head.php');
?>

<body id="page-loyalty">

<?php include('header.php'); ?>

<?php include('templates/page-preloader.php'); ?>


<div id="main-container">

    <section class="bg-white p-md-5 pb-5 p-0">

            <div class="container-fluid">

                <div class="d-flex py-md-5 pl-md-5 p-0 align-items-md-center flex-md-row flex-column-reverse">
                    <div class="col-md-6 col-12 pl-md-5">
                        <h1 class="mb-5 text-darkblue">Smooth out your day, <br> evreryday.</h1>
                        <p class="text-small text-lightblue">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                            irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                            pariatur. </p>
                        <p class="text-small text-lightblue">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                            irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                            pariatur. </p>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="img-max-width">
                            <img src="img/image-loyalty-image1.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>

    </section>

    <!-- 2 Loyalty box -->
    <section class="with-padding pt-0">

        <div class="container-fluid">

            <div class="d-flex loyalty-boxes flex-md-row flex-column">

                <div class="col-md-6 p-0 pr-md-1 p-0 mb-md-0 mb-3">
                    <div class="border loyalty-box">
                        <div class="img-max-width">
                            <img src="img/image-loyalty-box1.jpg" alt="Loyalty Box">
                        </div>
                    </div>
                </div>

                <div class="col-md-6 p-0 pl-md-1 p-0">
                    <div class="border loyalty-box p-5 d-flex align-items-start flex-column justify-content-between">
                        <h5 class="text-lightblue mb-4">01</h5>
                        <h4 class="text-darkblue">Join Belano With free card</h4>
                        <p class="loyalty-code text-medium mb-4">1Kod2</p>

                        <div class="mb-3">
                            <i class="fas fa-check-circle mr-2 text-lightblue"></i>
                            Registruj se kod nas
                        </div>
                        <p class="text-medium text-lightblue">Lorem ipsum dolor sit amet, consectetur <br>
                            adipiscing elit, sed do eiusmod tempor <br>
                            incididunt ut labore et dolore magna aliqua.
                        </p>
                        <a href="loyalty-payment" class="btn btn-sm btn-primary">Get Started</a>
                    </div>
                </div>

            </div>
        </div>

    </section>

    <section class="bg-grey with-padding">

        <div class="mb-5 flex-column">
            <h1>Smooth out your day, <br> evreryday.</h1>
            <p class="text-medium text-lightblue">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                sed do <br> eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
        </div>

        <div class="row">

            <div class="col-md-4 col-12">
                <div class="loyalty-pricebox">
                    <div class="loyalty-head first p-4">
                        <h5 class="text-lightblue mb-5">01</h5>
                        <h4>Join Belano with Basic Card</h4>
                        <p class="text-medium text-lightblue mb-1">1Kod & gratis dan</p>
                    </div>
                    <div class="loyalty-content p-5">
                        <h3 class="mb-4">$20/mo</h3>
                        <div class="mb-2">
                            <i class="fas fa-check-circle text-lightblue mr-2"></i>
                            Registruj se kod nas
                        </div>
                        <div>
                            <i class="fas fa-check-circle text-lightblue mr-2"></i>
                            Lajk - instagramu
                        </div>

                        <p class="text-medium text-lightblue font-italic mt-3 mb-5">Lorem ipsum dolor sit amet, consectetur <br>
                            adipiscing elit, sed do eiusmod tempor <br>
                            incididunt ut labore et dolore magna aliqua.
                        </p>

                        <a href="loyalty-login" class="btn btn-primary">Get started</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="loyalty-pricebox">
                    <div class="loyalty-head second p-4">
                        <h5 class="text-white mb-5">02</h5>
                        <h4>Join Belano with Basic Card</h4>
                        <p class="text-medium text-lightblue mb-1">1Kod & gratis dan</p>
                    </div>
                    <div class="loyalty-content p-5">
                        <h3 class="mb-4">$30/mo</h3>
                        <div class="mb-2">
                            <i class="fas fa-check-circle text-lightblue mr-2"></i>
                            Registruj se kod nas
                        </div>
                        <div>
                            <i class="fas fa-check-circle text-lightblue mr-2"></i>
                            Lajk - instagramu
                        </div>

                        <p class="text-medium text-lightblue font-italic mt-3 mb-5">Lorem ipsum dolor sit amet, consectetur <br>
                            adipiscing elit, sed do eiusmod tempor <br>
                            incididunt ut labore et dolore magna aliqua.
                        </p>

                        <a href="loyalty-login" class="btn btn-primary">Get started</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="loyalty-pricebox">
                    <div class="loyalty-head third p-4">
                        <h5 class="text-white mb-5">03</h5>
                        <h4>Join Belano with Basic Card</h4>
                        <p class="text-medium text-lightblue mb-1">1Kod & gratis dan</p>
                    </div>
                    <div class="loyalty-content p-5">
                        <h3 class="mb-4">$40/mo</h3>
                        <div class="mb-2">
                            <i class="fas fa-check-circle text-lightblue mr-2"></i>
                            Registruj se kod nas
                        </div>
                        <div>
                            <i class="fas fa-check-circle text-lightblue mr-2"></i>
                            Lajk - instagramu
                        </div>

                        <p class="text-medium text-lightblue font-italic mt-3 mb-5">Lorem ipsum dolor sit amet, consectetur <br>
                            adipiscing elit, sed do eiusmod tempor <br>
                            incididunt ut labore et dolore magna aliqua.
                        </p>

                        <a href="loyalty-login" class="btn btn-primary">Get started</a>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="bg-white p-md-5 p-0">

        <div class="container-fluid">

            <div class="row py-5 pl-md-5 align-items-center">
                <div class="col-md-6 col-12 pl-5">
                    <h1 class="mb-5">Smooth out your day, <br> evreryday.</h1>
                    <p class="text-small text-lightblue">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                        irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur. </p>
                    <p class="text-small text-lightblue">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                        irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                        pariatur. </p>
                </div>
                <div class="col-md-6 col-12">
                    <div class="img-max-width">
                        <img src="img/image-loyalty-image1.jpg" alt="">
                    </div>
                </div>

            </div>

        </div>

    </section>

</div>


<?php include('footer.php'); ?>