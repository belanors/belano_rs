! function(a, b, c, d) {
    var e = function(b, c) {
        this.elem = b, this.$elem = a(b), this.options = c, this.metadata = this.$elem.data("plugin-options")
    };
    e.prototype = {
        public: {
            startDate: moment().startOf("day"),
            endDate: moment().startOf("day"),
            format: "L",
            dateSeparator: " - ",
            calendarCount: 2,
            inline: !1,
            minDate: null,
            maxDate: null,
            legendFirst: "",
            legendSecond: "",
            dateUpdateContainer: null,
            showHeader: !0,
            showFooter: !0,
            startOnMonday: !1,
            container: "body",
            oneCalendarWidth: 230,
            showOn: "bottom",
            locale: moment.locale(),
            singleDate: !1,
            target: null,
            autoCloseOnSelect: !1,
            ranges: [{
                title: "Today",
                startDate: moment(),
                endDate: moment()
            }, {
                title: "3 Days",
                startDate: moment(),
                endDate: moment().add(2, "days")
            }, {
                title: "5 Days",
                startDate: moment(),
                endDate: moment().add(4, "days")
            }, {
                title: "1 Week",
                startDate: moment(),
                endDate: moment().add(6, "days")
            }, {
                title: "Till Next Week",
                startDate: moment(),
                endDate: moment().endOf("week")
            }, {
                title: "Till Next Month",
                startDate: moment(),
                endDate: moment().endOf("month")
            }],
            rangeLabel: "Ranges: ",
            cancelLabel: "Cancel",
            applyLabel: "Apply",
            onbeforeselect: function() {
                return !0
            },
            onafterselect: function() {},
            onbeforeshow: function() {},
            onbeforehide: function() {},
            onaftershow: function() {},
            onafterhide: function() {},
            onfirstselect: function() {},
            onrangeselect: function() {},
            onbeforemonthchange: function() {
                return !0
            },
            onaftermonthchange: function() {},
            ondraw: function() {

            },
            oninit: function() {},
            disableDays: function() {
                return !1
            },
            disabledRanges: [],
            continuous: !1,
            enableMonthSwitcher: !0,
            enableYearSwitcher: !0
        },
        private: {
            startSelected: !1,
            currentDate: moment().startOf("day"),
            endSelected: !0,
            hoverDate: null,
            headerStartDay: null,
            headerStartDate: null,
            headerStartWeekday: null,
            headerEndDay: null,
            headerEndDate: null,
            headerEndWeekday: null,
            swipeTimeout: null,
            isMobile: !1,
            valElements: ["BUTTON", "OPTION", "INPUT", "LI", "METER", "PROGRESS", "PARAM"],
            dontHideOnce: !1,
            initiator: null,
            initComplete: !1
        },
        init: function() {
            return this.config = a.extend({}, this.public, this.options, this.metadata), this.globals = a.extend({}, this.private), this.globals.isMobile = this.checkMobile(), this.applyConfig(), this.fetchInputs(), this.drawUserInterface(), this.$elem.data("caleran", this), this.config.oninit(this), this.globals.initComplete = !0, this
        },
        validateDates: function() {
            var a;
            moment(this.config.startDate).locale(this.config.locale).isValid() && moment(this.config.endDate).locale(this.config.locale).isValid() ? (this.config.startDate = moment(this.config.startDate).locale(this.config.locale), this.config.endDate = moment(this.config.endDate).locale(this.config.locale), this.config.startDate.isAfter(this.config.endDate) && (a = this.config.startDate.clone(), this.config.startDate = this.config.endDate.clone(), this.config.endDate = a.clone(), a = null)) : (this.config.startDate = moment(), this.config.endDate = moment()), this.globals.currentDate = moment(this.config.startDate), null !== this.config.minDate && moment(this.config.minDate).locale(this.config.locale).isValid() ? this.config.minDate = moment(this.config.minDate).locale(this.config.locale) : this.config.minDate = null, null !== this.config.maxDate && moment(this.config.maxDate).locale(this.config.locale).isValid() ? this.config.maxDate = moment(this.config.maxDate).locale(this.config.locale) : this.config.maxDate = null, null !== this.config.minDate && null !== this.config.maxDate && this.config.minDate.isAfter(this.config.maxDate) && (a = this.config.minDate.clone(), this.config.minDate = this.config.maxDate.clone(), this.config.maxDate = a.clone(), a = null)
        },
        applyConfig: function() {
            moment.locale(this.config.locale), null === this.config.target && (this.config.target = this.$elem), !1 === this.globals.isMobile ? (!0 === this.config.inline ? (this.container = this.$elem.wrapAll("<div class='caleran-container'></div>").parent(), this.input = a("<div class='caleran-input'></div>").appendTo(this.container), this.elem.type = "hidden") : (this.container = a("<div class='caleran-container caleran-popup'><div class='caleran-box-arrow-top'></div></div>").appendTo(this.config.container), this.input = a("<div class='caleran-input'></div>").appendTo(this.container), this.container.hide()), this.input.css("width", this.config.calendarCount * this.config.oneCalendarWidth + "px"), !1 === this.config.inline && this.setViewport()) : (this.container = a("<div class='caleran-container-mobile'></div>").appendTo(this.config.container), this.input = a("<div class='caleran-input'></div>").appendTo(this.container), this.input.hide(), this.$elem.on("focus", function() {
                a(this).blur()
            }))
        },
        fetchInputs: function() {
            moment.locale(this.config.locale);
            var b = null;
            if (b = -1 !== a.inArray(this.elem.tagName, this.globals.valElements) ? this.$elem.val() : this.$elem.text(), !1 === this.config.singleDate && b.indexOf(this.config.dateSeparator) > 0) {
                var c = b.split(this.config.dateSeparator);
                2 == c.length && moment(c[0], this.config.format).isValid() && moment(c[1], this.config.format).isValid() && (this.config.startDate = moment(c[0], this.config.format), this.config.endDate = moment(c[1], this.config.format))
            } else if (!0 === this.config.singleDate) {
                var d = b;
                moment(d, this.config.format).isValid() && (this.config.startDate = moment(d, this.config.format), this.config.endDate = moment(d, this.config.format))
            }
            this.validateDates()
        },
        drawUserInterface: function() {
            console.log('calendar draw init');

            this.drawHeader(), this.calendars = this.input.find(".caleran-calendars").first();
            for ( var a = this.globals.currentDate.clone(), b = 0; b < this.config.calendarCount; b++)
                this.drawCalendarOfMonth(a),
                a = a.month(a.month() + 1);
                this.calendars.find(".caleran-calendar").last().addClass("no-border-right"),
                this.drawArrows(), this.drawFooter(), this.reDrawCells(), this.attachEvents(), this.updateInput(!1), !1 === this.config.inline && this.setViewport();

            this.initMonthSwitch();

        },
        drawYearSelector: function() {

        },
        drawHeader: function() {
            var a = "<div class='caleran-header'><div class='caleran-header-start'><p>Check - In</p><div class='caleran-header-start-day'></div><div class='bi-day-name'><div class='caleran-header-start-date'></div><div class='caleran-header-start-weekday'></div></div></div>";
            !1 === this.config.singleDate && (a += "<div class='caleran-header-separator'><i class='icon-svg right-arrow'></i></div><div class='caleran-header-end'><p>Check - Out</p><div class='caleran-header-end-day'></div><div class='bi-day-name'><div class='caleran-header-end-date'></div><div class='caleran-header-end-weekday'></div></div></div>"), a += "</div><div class='caleran-calendars'></div>", this.input.append(a), !1 === this.config.showHeader && this.input.find(".caleran-header").hide(), this.globals.headerStartDay = this.input.find(".caleran-header-start-day"), this.globals.headerStartDate = this.input.find(".caleran-header-start-date"), this.globals.headerStartWeekday = this.input.find(".caleran-header-start-weekday"), this.globals.headerEndDay = this.input.find(".caleran-header-end-day"), this.globals.headerEndDate = this.input.find(".caleran-header-end-date"), this.globals.headerEndWeekday = this.input.find(".caleran-header-end-weekday"), this.updateHeader()
        },
        updateHeader: function() {
            null !== this.config.startDate ? (this.globals.headerStartDay.text(this.config.startDate.date()), this.globals.isMobile ? this.globals.headerStartDate.text(moment.monthsShort(this.config.startDate.month()) + " " + this.config.startDate.year()) : this.globals.headerStartDate.text(moment.months(this.config.startDate.month()) + " " + this.config.startDate.year()), this.globals.headerStartWeekday.text(moment.weekdays(this.config.startDate.day()))) : (this.globals.headerStartDay.text(""), this.globals.headerStartDate.text(""), this.globals.headerStartWeekday.text("")), !1 === this.config.singleDate && (null !== this.config.endDate ? (this.globals.headerEndDay.text(this.config.endDate.date()), this.globals.isMobile ? this.globals.headerEndDate.text(moment.monthsShort(this.config.endDate.month()) + " " + this.config.endDate.year()) : this.globals.headerEndDate.text(moment.months(this.config.endDate.month()) + " " + this.config.endDate.year()), this.globals.headerEndWeekday.text(moment.weekdays(this.config.endDate.day()))) : (this.globals.headerEndDay.text(""), this.globals.headerEndDate.text(""), this.globals.headerEndWeekday.text("")))
        
        },
        updateInput: function(b) {
            if (!0 === this.config.singleDate) {
                if (null === this.config.startDate) return
            } else if (null === this.config.startDate || null === this.config.endDate) return; - 1 !== a.inArray(this.elem.tagName, this.globals.valElements) ? !1 === this.config.singleDate ? this.config.target.val(this.config.startDate.locale(this.config.locale).format(this.config.format) + this.config.dateSeparator + this.config.endDate.locale(this.config.locale).format(this.config.format)) : this.config.target.val(this.config.startDate.locale(this.config.locale).format(this.config.format)) : !1 === this.config.singleDate ? this.config.target.text(this.config.startDate.locale(this.config.locale).format(this.config.format) + this.config.dateSeparator + this.config.endDate.locale(this.config.locale).format(this.config.format)) : this.config.target.text(this.config.startDate.locale(this.config.locale).format(this.config.format)), !0 === this.globals.initComplete && !0 === b && this.config.onafterselect(this, this.config.startDate, this.config.endDate)
            var selected_date = this.config.startDate.locale(this.config.locale).format(this.config.format);

            $("#" + this.config.dateUpdateContainer).html(selected_date);

        },

        drawArrows: function() {
            //this.container.find(".caleran-title").length > 0 && (this.container.find(".caleran-title").first().prepend("<div class='caleran-prev'><i class='fa fa-arrow-left'></i></div>"), this.container.find(".caleran-title").last().append("<div class='caleran-next'><i class='fa fa-arrow-right'></i></div>"))
            // this.container.find(".caleran-title").length > 0 && (this.container.find(".caleran-title").first().prepend("<div class='caleran-prev'><i class='fa fa-arrow-left'></i></div>"),
                // this.container.find(".caleran-title").last().append("<div class='caleran-next'><i class='fa fa-arrow-right'></i></div>"))
        },

        drawCalendarOfMonth: function(a) {
            moment.locale(this.config.locale);
            var b = moment.localeData(this.config.locale).firstDayOfWeek(),
                c = moment(a).startOf("month").subtract(1, "day").endOf("month").startOf("week");
            1 == b && !1 === this.config.startOnMonday ? (c.subtract(1, "days"), b = 0) : 0 === b && !0 === this.config.startOnMonday && (c.add(1, "days"), b = 1);
            var d = "<div class='caleran-calendar' data-month='" + a.month() + "'>",
                e = 0,
                f = "",
                g = "";
            this.config.enableMonthSwitcher && (f = " class='caleran-month-switch'"), this.config.enableYearSwitcher && (g = " class='caleran-year-switch'"),
            d += "<div class='caleran-title'><div" + f + ">" + moment.months(a.month()) + "</div>&nbsp;<span" + g + ">" + a.year() + "</span></div>",
            d += "<div class='caleran-days-container day-labels'>";
            
            // print day labels
            for (var h = b; h < b + 7; h++) d += "<div class='caleran-dayofweek'>" + moment.weekdaysShort(h % 7) + "</div>";

            d += "</div>";
            d += "<div class='caleran-days-container'>";

            // print days
            for (; e < 42;) cellDate = c.startOf("day").unix(), cellStyle = a.month() == c.month() ? "caleran-day" : "caleran-disabled",
                d += "<div class='" + cellStyle + "' data-value='" + cellDate + "'><span>" + c.date() + "</span></div>", e++, c.add(1, "days");
            d += "</div>", d += "<div class='caleran-legend'><span class='legend-free'>"+ this.config.legendFirst +"</span><span class='legend-busy'>"+ this.config.legendSecond +"</span></div></div></div>", this.calendars.append(d);
        },
        drawFooter: function() {
            if (!1 === this.config.singleDate && !0 === this.config.showFooter) {
                this.input.append("<div class='caleran-ranges'></div>");
                var a = this.input.find(".caleran-ranges");

                // this.append("<div class='calendar-label'>Check-in Day</div>");

                a.append("<i class='fa fa-retweet'></i><div class='caleran-range-header'>" + this.config.rangeLabel + "</div>");
                for (var b in this.config.ranges) a.append("<div class='caleran-range' data-id='" + b + "'>" + this.config.ranges[b].title + "</div>")
            }
            this.globals.isMobile && (!0 !== this.config.singleDate && !1 !== this.config.showFooter || this.input.append("<div class='caleran-filler'></div>"), this.input.append("<div class='caleran-footer'></div>"), this.footer = this.input.find(".caleran-footer"), this.footer.append("<button class='caleran-cancel'>" + this.config.cancelLabel + "</button>"), this.footer.append("<button class='caleran-apply'>" + this.config.applyLabel + "</button>"))
        },
        drawNextMonth: function(a) {
            if (null === this.globals.swipeTimeout) {
                var b = this;
                this.globals.swipeTimeout = setTimeout(function() {
                    !0 === b.config.onbeforemonthchange(b, b.globals.currentDate.startOf("month"), "next") && (b.globals.currentDate.add(1, "month"), b.reDrawCalendars(), b.calendars.get(0).scrollTop = b.calendars.get(0).scrollHeight, b.config.onaftermonthchange(b, b.globals.currentDate.startOf("month"))), b.globals.swipeTimeout = null
                }, 100)
            }
            a && "function" == typeof a.stopPropagation && a.stopPropagation()
        },
        drawPrevMonth: function(a) {
            if (null === this.globals.swipeTimeout) {
                var b = this;
                this.globals.swipeTimeout = setTimeout(function() {
                    b.calendars.get(0).scrollTop = 0, !0 === b.config.onbeforemonthchange(b, b.globals.currentDate.startOf("month"), "prev") && (b.globals.currentDate.subtract(1, "month"), b.reDrawCalendars(), b.config.onaftermonthchange(b, b.globals.currentDate.startOf("month"))), b.globals.swipeTimeout = null
                }, 100)
            }
            a && "function" == typeof a.stopPropagation && a.stopPropagation()
        },
        cellClicked: function(b) {
            var c = a(b.target).data("value"),
                d = moment.unix(c);
            if (!1 === this.config.singleDate)
                if (!1 === this.globals.startSelected) this.globals.startDateBackup = this.config.startDate.clone(), this.config.startDate = d, this.config.endDate = null, this.globals.startSelected = !0, this.globals.endSelected = !1, void 0 !== this.footer && this.footer.find(".caleran-apply").attr("disabled", "disabled"), this.config.onfirstselect(this, this.config.startDate);
                else {
                    if (d.isBefore(this.config.startDate)) {
                        var e = this.config.startDate.clone();
                        this.config.startDate = d.clone(), d = e
                    }
                    this.globals.startDateBackup = null, this.config.endDate = d, this.globals.endSelected = !0, this.globals.startSelected = !1, this.globals.hoverDate = null, !1 === this.globals.isMobile ? (!0 === this.config.onbeforeselect(this, this.config.startDate, this.config.endDate) && !0 === this.checkRangeContinuity() ? this.updateInput(!0) : this.fetchInputs(), this.config.autoCloseOnSelect && !1 === this.config.inline && this.hideDropdown()) : !1 === this.checkRangeContinuity() && this.fetchInputs(), void 0 !== this.footer && this.footer.find(".caleran-apply").removeAttr("disabled")
                } else this.config.startDate = d, this.config.endDate = d, this.globals.endSelected = !0, this.globals.startSelected = !1, this.globals.hoverDate = null, !1 === this.globals.isMobile && (!0 === this.config.onbeforeselect(this, this.config.startDate, this.config.endDate) ? this.updateInput(!0) : this.fetchInputs(), this.config.autoCloseOnSelect && !1 === this.config.inline && this.hideDropdown());
            this.reDrawCells(), this.updateHeader(), b && "function" == typeof b.stopPropagation && b.stopPropagation()
        },
        checkRangeContinuity: function() {
            if (!1 === this.config.continuous) return !0;
            for (var b = this.config.endDate.diff(this.config.startDate, "days"), c = moment(this.config.startDate), d = 0; d <= b; d++) {
                if (a.grep(this.config.disabledRanges, function(a) {
                        return c.isBetween(a.start, a.end, "day", "[]")
                    }).length > 0 || !0 === this.config.disableDays(c)) return !1;
                c.add(1, "days")
            }
            return !0
        },
        cellHovered: function(b) {
            var c = a(b.target).data("value");
            this.globals.hoverDate = moment.unix(c), !0 === this.globals.startSelected && this.reDrawCells(), b && "function" == typeof b.stopPropagation && b.stopPropagation()
        },

        reDrawCalendars: function() {
            this.input.empty(), this.drawUserInterface()
        },

        initMonthSwitch: function() {
            var b = this;
            this.calendars.get(0).scrollTop = 0;
            for (
                var c = a("<div class='caleran-month-selector'></div>").appendTo(this.calendars),
                d = this.globals.currentDate.get("month"), e = 0; e < 12; e++)
                c.append("<div class='caleran-ms-month" + (d == e ? " current" : "") + "' data-month='" + e + "'>" + moment.months(e) + "</div>");

                // Ilija
                // c.find(".caleran-ms-month").off("click").on("click", function(c) {
                //     b.globals.currentDate.set("month", a(this).data("month")),
                //     b.reDrawCalendars(), c.stopPropagation();
                //
                //     console.log('month switch init');

                // })
        },
        monthSwitchClicked: function() {
            var b = this;
            this.calendars.get(0).scrollTop = 0;
            for (
                var c = a("<div class='caleran-month-selector'></div>").appendTo(this.calendars),
                d = this.globals.currentDate.get("month"), e = 0; e < 12; e++)
                c.append("<div class='caleran-ms-month" + (d == e ? " current" : "") + "' data-month='" + e + "'>" + moment.months(e) + "</div>");

                // Ilija
                c.find(".caleran-ms-month").off("click").on("click", function(c) {
                    b.globals.currentDate.set("month", a(this).data("month")),
                    // b.calendars.find(".caleran-month-selector").remove(),
                    b.reDrawCalendars(), c.stopPropagation();

            })
        },
        yearSwitchClicked: function() {
            var b = this;
            this.calendars.get(0).scrollTop = 0;
            for (var c = a("<div class='caleran-year-selector'></div>").appendTo(this.calendars), d = this.globals.currentDate.get("year"), e = d - 4; e < d + 2; e++) c.append("<div class='caleran-ys-year" + (d == e ? " current" : "") + "' data-year='" + e + "'>" + e + "</div>");
            c.fadeIn(100).css("display", "flex"), c.find(".caleran-ys-year").off("click").on("click", function(c) {
                b.globals.currentDate.set("year", a(this).data("year")), b.calendars.find(".caleran-year-selector").remove(), b.reDrawCalendars(), c.stopPropagation()
            })
        },
        hideDropdown: function(b) {
            if (!0 === this.globals.dontHideOnce) return void(this.hideDropdown.caller.arguments[0].target != this.globals.initiator && (this.globals.dontHideOnce = !1));
            this.input.is(":visible") && (this.config.onbeforehide(this), this.globals.isMobile ? (this.input.css({
                display: "none"
            }), a("body").removeClass("caleran-open")) : this.container.css({
                display: "none"
            }), this.globals.hoverDate = null, null !== this.globals.startDateBackup && (this.config.startDate = this.globals.startDateBackup, this.globals.startSelected = !1), this.config.onafterhide(this))
        },
        showDropdown: function(b) {
            this.input.is(":visible") || (void 0 === b && (this.globals.dontHideOnce = !0, this.globals.initiator = this.showDropdown.caller.arguments[0].target), this.fetchInputs(), this.reDrawCalendars(), this.config.onbeforeshow(this), this.globals.isMobile ? (this.input.css({
                display: "flex"
            }), a("body").addClass("caleran-open")) : this.container.css({
                display: "block"
            }), this.setViewport(), this.config.onaftershow(this))
        },
        reDrawCells: function() {
            var b = this;
            this.container.find(".caleran-day, .caleran-disabled").each(function() {
                var c = a(this).attr("data-value"),
                    d = moment.unix(c).locale(b.config.locale),
                    e = "caleran-day";
                6 != d.day() && 0 !== d.day() || (e += " caleran-weekend"), moment().isSame(d, "day") && (e += " caleran-today"), !1 === b.config.singleDate && null !== b.config.startDate && b.config.startDate.isSame(d, "day") && (e += " caleran-start"), !1 === b.config.singleDate && null !== b.config.endDate && b.config.endDate.isSame(d, "day") && (e += " caleran-end"), !1 === b.config.singleDate && null !== b.config.startDate && null !== b.config.endDate && d.isBetween(b.config.startDate, b.config.endDate, "day", "[]") && (e += " caleran-selected"), !0 === b.config.singleDate && null !== b.config.startDate && b.config.startDate.isSame(d, "day") && (e += " caleran-selected"), !0 === b.globals.startSelected && !1 === b.globals.endSelected && null !== b.globals.hoverDate && (d.isBetween(b.globals.hoverDate, b.config.startDate, "day", "[]") || d.isBetween(b.config.startDate, b.globals.hoverDate, "day", "[]")) && (e += " caleran-hovered");
                var f = a.grep(b.config.disabledRanges, function(a) {
                    return d.isBetween(a.start, a.end, "day", "[]")
                }).length > 0 || !0 === b.config.disableDays(d);
                (f || d.month() != a(this).closest(".caleran-calendar").data("month") || null !== b.config.maxDate && d.isAfter(b.config.maxDate, "day") || null !== b.config.minDate && d.isBefore(b.config.minDate, "day")) && (e = "caleran-disabled"), f && (e += " caleran-disabled-range"), a(this).attr("class", e)
            }), this.attachEvents(), this.config.ondraw(this)
        },
        rangeClicked: function(b) {
            if (b.target.hasAttribute("data-id")) {
                var c = a(b.target).attr("data-id");
                this.globals.currentDate = this.config.ranges[c].startDate.startOf("day").clone().locale(this.config.locale), this.config.startDate = this.config.ranges[c].startDate.startOf("day").clone().locale(this.config.locale), this.config.endDate = this.config.ranges[c].endDate.startOf("day").clone(), !1 === this.checkRangeContinuity() ? this.fetchInputs() : (this.config.onrangeselect(this, this.config.ranges[c]), this.reDrawCalendars()), b && "function" == typeof b.stopPropagation && b.stopPropagation()
            }
        },
        setViewport: function() {
            if (!0 === this.globals.isMobile);
            else {
                var a = {
                        top: b.scrollY,
                        left: b.scrollX,
                        bottom: b.scrollY + b.innerHeight,
                        right: b.scrollX + b.innerWidth
                    },
                    c = this.getDimensions(this.$elem, !0),
                    d = this.getDimensions(this.container, !0);
                "top" == this.config.showOn && c.offsetTop - d.height < a.top || "top" != this.config.showOn && c.offsetTop + c.height + d.height < a.bottom ? (this.container.css("top", c.offsetTop + c.height + 8), this.container.find(".caleran-box-arrow-bottom").removeClass("caleran-box-arrow-bottom").addClass("caleran-box-arrow-top")) : (this.container.css("top", c.offsetTop - d.height - 8), this.container.find(".caleran-box-arrow-top").removeClass("caleran-box-arrow-top").addClass("caleran-box-arrow-bottom")), c.offsetLeft + d.width > a.right ? this.container.css("left", a.right - d.width) : c.offsetLeft < a.left ? this.container.css("left", a.left) : this.container.css("left", c.offsetLeft - 10), this.container.find(".caleran-box-arrow-top, .caleran-box-arrow-bottom").css("margin-left", c.offsetLeft - parseInt(this.container.css("left"), 10))
            }
        },
        getDimensions: function(a, b) {
            return {
                width: b ? a.outerWidth() : a.innerWidth(),
                height: b ? a.outerHeight() : a.innerHeight(),
                offsetTop: a.offset().top,
                offsetLeft: a.offset().left
            }
        },
        attachEvents: function() {
            var b = a.proxy(this.drawNextMonth, this),
                d = a.proxy(this.drawPrevMonth, this),
                e = a.proxy(this.cellClicked, this),
                f = a.proxy(this.cellHovered, this),
                g = a.proxy(this.rangeClicked, this),
                h = a.proxy(this.monthSwitchClicked, this),
                i = a.proxy(this.yearSwitchClicked, this);
            if (this.container.find(".caleran-next").off("click").one("click", b), this.container.find(".caleran-prev").off("click").one("click", d), this.container.find(".caleran-day").off("click").on("click", e), this.container.find(".caleran-day").off("mouseover").on("mouseover", f), this.container.find(".caleran-disabled").off("click"), this.container.find(".caleran-range").off("click").on("click", g),
                
                // this.container.find(".caleran-month-switch ").off("click").on("click", h),

                this.container.find(".caleran-year-switch ").off("click").on("click", i), !1 === this.globals.isMobile && !1 === this.config.inline && (a(c).on("click", a.proxy(function(b) {
                    !this.input.is(a(b.target)) && 0 === a(this.input).find(a(b.target)).length && !1 === a(b.target).is(this.$elem) && a(this.input).is(":visible") && this.hideDropdown(b)
                }, this)), this.$elem.off("click").on("click", a.proxy(function(a) {
                    this.showDropdown(a)
                }, this))), !0 === this.globals.isMobile) {
                if ("function" == typeof a.fn.swiperight) this.input.find(".caleran-calendars").css("touch-action", "none"), this.input.find(".caleran-calendars").on("swipeleft", b), this.input.find(".caleran-calendars").on("swiperight", d);
                else {
                    var j = new Hammer(this.input.find(".caleran-calendars").get(0));
                    j.off("swipeleft").on("swipeleft", b), j.off("swiperight").on("swiperight", d)
                }
                this.$elem.off("click").on("click", a.proxy(function(a) {
                    this.showDropdown(a)
                }, this)), this.input.find(".caleran-cancel").off("click").on("click", a.proxy(function(a) {
                    this.hideDropdown(a)
                }, this)), this.input.find(".caleran-apply").off("click").on("click", a.proxy(function(a) {
                    !0 === this.config.onbeforeselect(this, this.config.startDate, this.config.endDate) && !0 === this.checkRangeContinuity() ? this.updateInput(!0) : this.fetchInputs(), this.hideDropdown(a)
                }, this))
            }
        },
        checkMobile: function() {
            return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(navigator.userAgent || navigator.vendor || b.opera) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test((navigator.userAgent || navigator.vendor || b.opera).substr(0, 4))
        }
    }, e.defaults = e.prototype.defaults, a.fn.caleran = function(a) {
        return this.each(function() {
            new e(this, a).init()
        })
    }
}(jQuery, window, document),
function(a, b, c, d) {
    "use strict";

    function e(a, b, c) {
        return setTimeout(j(a, c), b)
    }

    function f(a, b, c) {
        return !!Array.isArray(a) && (g(a, c[b], c), !0)
    }

    function g(a, b, c) {
        var e;
        if (a)
            if (a.forEach) a.forEach(b, c);
            else if (a.length !== d)
            for (e = 0; e < a.length;) b.call(c, a[e], e, a), e++;
        else
            for (e in a) a.hasOwnProperty(e) && b.call(c, a[e], e, a)
    }

    function h(b, c, d) {
        var e = "DEPRECATED METHOD: " + c + "\n" + d + " AT \n";
        return function() {
            var c = new Error("get-stack-trace"),
                d = c && c.stack ? c.stack.replace(/^[^\(]+?[\n$]/gm, "").replace(/^\s+at\s+/gm, "").replace(/^Object.<anonymous>\s*\(/gm, "{anonymous}()@") : "Unknown Stack Trace",
                f = a.console && (a.console.warn || a.console.log);
            return f && f.call(a.console, e, d), b.apply(this, arguments)
        }
    }

    function i(a, b, c) {
        var d, e = b.prototype;
        d = a.prototype = Object.create(e), d.constructor = a, d._super = e, c && la(d, c)
    }

    function j(a, b) {
        return function() {
            return a.apply(b, arguments)
        }
    }

    function k(a, b) {
        return typeof a == oa ? a.apply(b ? b[0] || d : d, b) : a
    }

    function l(a, b) {
        return a === d ? b : a
    }

    function m(a, b, c) {
        g(q(b), function(b) {
            a.addEventListener(b, c, !1)
        })
    }

    function n(a, b, c) {
        g(q(b), function(b) {
            a.removeEventListener(b, c, !1)
        })
    }

    function o(a, b) {
        for (; a;) {
            if (a == b) return !0;
            a = a.parentNode
        }
        return !1
    }

    function p(a, b) {
        return a.indexOf(b) > -1
    }

    function q(a) {
        return a.trim().split(/\s+/g)
    }

    function r(a, b, c) {
        if (a.indexOf && !c) return a.indexOf(b);
        for (var d = 0; d < a.length;) {
            if (c && a[d][c] == b || !c && a[d] === b) return d;
            d++
        }
        return -1
    }

    function s(a) {
        return Array.prototype.slice.call(a, 0)
    }

    function t(a, b, c) {
        for (var d = [], e = [], f = 0; f < a.length;) {
            var g = b ? a[f][b] : a[f];
            r(e, g) < 0 && d.push(a[f]), e[f] = g, f++
        }
        return c && (d = b ? d.sort(function(a, c) {
            return a[b] > c[b]
        }) : d.sort()), d
    }

    function u(a, b) {
        for (var c, e, f = b[0].toUpperCase() + b.slice(1), g = 0; g < ma.length;) {
            if (c = ma[g], (e = c ? c + f : b) in a) return e;
            g++
        }
        return d
    }

    function v() {
        return ua++
    }

    function w(b) {
        var c = b.ownerDocument || b;
        return c.defaultView || c.parentWindow || a
    }

    function x(a, b) {
        var c = this;
        this.manager = a, this.callback = b, this.element = a.element, this.target = a.options.inputTarget, this.domHandler = function(b) {
            k(a.options.enable, [a]) && c.handler(b)
        }, this.init()
    }

    function y(a) {
        var b = a.options.inputClass;
        return new(b || (xa ? M : ya ? P : wa ? R : L))(a, z)
    }

    function z(a, b, c) {
        var d = c.pointers.length,
            e = c.changedPointers.length,
            f = b & Ea && d - e == 0,
            g = b & (Ga | Ha) && d - e == 0;
        c.isFirst = !!f, c.isFinal = !!g, f && (a.session = {}), c.eventType = b, A(a, c), a.emit("hammer.input", c), a.recognize(c), a.session.prevInput = c
    }

    function A(a, b) {
        var c = a.session,
            d = b.pointers,
            e = d.length;
        c.firstInput || (c.firstInput = D(b)), e > 1 && !c.firstMultiple ? c.firstMultiple = D(b) : 1 === e && (c.firstMultiple = !1);
        var f = c.firstInput,
            g = c.firstMultiple,
            h = g ? g.center : f.center,
            i = b.center = E(d);
        b.timeStamp = ra(), b.deltaTime = b.timeStamp - f.timeStamp, b.angle = I(h, i), b.distance = H(h, i), B(c, b), b.offsetDirection = G(b.deltaX, b.deltaY);
        var j = F(b.deltaTime, b.deltaX, b.deltaY);
        b.overallVelocityX = j.x, b.overallVelocityY = j.y, b.overallVelocity = qa(j.x) > qa(j.y) ? j.x : j.y, b.scale = g ? K(g.pointers, d) : 1, b.rotation = g ? J(g.pointers, d) : 0, b.maxPointers = c.prevInput ? b.pointers.length > c.prevInput.maxPointers ? b.pointers.length : c.prevInput.maxPointers : b.pointers.length, C(c, b);
        var k = a.element;
        o(b.srcEvent.target, k) && (k = b.srcEvent.target), b.target = k
    }

    function B(a, b) {
        var c = b.center,
            d = a.offsetDelta || {},
            e = a.prevDelta || {},
            f = a.prevInput || {};
        b.eventType !== Ea && f.eventType !== Ga || (e = a.prevDelta = {
            x: f.deltaX || 0,
            y: f.deltaY || 0
        }, d = a.offsetDelta = {
            x: c.x,
            y: c.y
        }), b.deltaX = e.x + (c.x - d.x), b.deltaY = e.y + (c.y - d.y)
    }

    function C(a, b) {
        var c, e, f, g, h = a.lastInterval || b,
            i = b.timeStamp - h.timeStamp;
        if (b.eventType != Ha && (i > Da || h.velocity === d)) {
            var j = b.deltaX - h.deltaX,
                k = b.deltaY - h.deltaY,
                l = F(i, j, k);
            e = l.x, f = l.y, c = qa(l.x) > qa(l.y) ? l.x : l.y, g = G(j, k), a.lastInterval = b
        } else c = h.velocity, e = h.velocityX, f = h.velocityY, g = h.direction;
        b.velocity = c, b.velocityX = e, b.velocityY = f, b.direction = g
    }

    function D(a) {
        for (var b = [], c = 0; c < a.pointers.length;) b[c] = {
            clientX: pa(a.pointers[c].clientX),
            clientY: pa(a.pointers[c].clientY)
        }, c++;
        return {
            timeStamp: ra(),
            pointers: b,
            center: E(b),
            deltaX: a.deltaX,
            deltaY: a.deltaY
        }
    }

    function E(a) {
        var b = a.length;
        if (1 === b) return {
            x: pa(a[0].clientX),
            y: pa(a[0].clientY)
        };
        for (var c = 0, d = 0, e = 0; e < b;) c += a[e].clientX, d += a[e].clientY, e++;
        return {
            x: pa(c / b),
            y: pa(d / b)
        }
    }

    function F(a, b, c) {
        return {
            x: b / a || 0,
            y: c / a || 0
        }
    }

    function G(a, b) {
        return a === b ? Ia : qa(a) >= qa(b) ? a < 0 ? Ja : Ka : b < 0 ? La : Ma
    }

    function H(a, b, c) {
        c || (c = Qa);
        var d = b[c[0]] - a[c[0]],
            e = b[c[1]] - a[c[1]];
        return Math.sqrt(d * d + e * e)
    }

    function I(a, b, c) {
        c || (c = Qa);
        var d = b[c[0]] - a[c[0]],
            e = b[c[1]] - a[c[1]];
        return 180 * Math.atan2(e, d) / Math.PI
    }

    function J(a, b) {
        return I(b[1], b[0], Ra) + I(a[1], a[0], Ra)
    }

    function K(a, b) {
        return H(b[0], b[1], Ra) / H(a[0], a[1], Ra)
    }

    function L() {
        this.evEl = Ta, this.evWin = Ua, this.pressed = !1, x.apply(this, arguments)
    }

    function M() {
        this.evEl = Xa, this.evWin = Ya, x.apply(this, arguments), this.store = this.manager.session.pointerEvents = []
    }

    function N() {
        this.evTarget = $a, this.evWin = _a, this.started = !1, x.apply(this, arguments)
    }

    function O(a, b) {
        var c = s(a.touches),
            d = s(a.changedTouches);
        return b & (Ga | Ha) && (c = t(c.concat(d), "identifier", !0)), [c, d]
    }

    function P() {
        this.evTarget = bb, this.targetIds = {}, x.apply(this, arguments)
    }

    function Q(a, b) {
        var c = s(a.touches),
            d = this.targetIds;
        if (b & (Ea | Fa) && 1 === c.length) return d[c[0].identifier] = !0, [c, c];
        var e, f, g = s(a.changedTouches),
            h = [],
            i = this.target;
        if (f = c.filter(function(a) {
                return o(a.target, i)
            }), b === Ea)
            for (e = 0; e < f.length;) d[f[e].identifier] = !0, e++;
        for (e = 0; e < g.length;) d[g[e].identifier] && h.push(g[e]), b & (Ga | Ha) && delete d[g[e].identifier], e++;
        return h.length ? [t(f.concat(h), "identifier", !0), h] : void 0
    }

    function R() {
        x.apply(this, arguments);
        var a = j(this.handler, this);
        this.touch = new P(this.manager, a), this.mouse = new L(this.manager, a), this.primaryTouch = null, this.lastTouches = []
    }

    function S(a, b) {
        a & Ea ? (this.primaryTouch = b.changedPointers[0].identifier, T.call(this, b)) : a & (Ga | Ha) && T.call(this, b)
    }

    function T(a) {
        var b = a.changedPointers[0];
        if (b.identifier === this.primaryTouch) {
            var c = {
                x: b.clientX,
                y: b.clientY
            };
            this.lastTouches.push(c);
            var d = this.lastTouches,
                e = function() {
                    var a = d.indexOf(c);
                    a > -1 && d.splice(a, 1)
                };
            setTimeout(e, cb)
        }
    }

    function U(a) {
        for (var b = a.srcEvent.clientX, c = a.srcEvent.clientY, d = 0; d < this.lastTouches.length; d++) {
            var e = this.lastTouches[d],
                f = Math.abs(b - e.x),
                g = Math.abs(c - e.y);
            if (f <= db && g <= db) return !0
        }
        return !1
    }

    function V(a, b) {
        this.manager = a, this.set(b)
    }

    function W(a) {
        if (p(a, jb)) return jb;
        var b = p(a, kb),
            c = p(a, lb);
        return b && c ? jb : b || c ? b ? kb : lb : p(a, ib) ? ib : hb
    }

    function X() {
        if (!fb) return !1;
        var b = {},
            c = a.CSS && a.CSS.supports;
        return ["auto", "manipulation", "pan-y", "pan-x", "pan-x pan-y", "none"].forEach(function(d) {
            b[d] = !c || a.CSS.supports("touch-action", d)
        }), b
    }

    function Y(a) {
        this.options = la({}, this.defaults, a || {}), this.id = v(), this.manager = null, this.options.enable = l(this.options.enable, !0), this.state = nb, this.simultaneous = {}, this.requireFail = []
    }

    function Z(a) {
        return a & sb ? "cancel" : a & qb ? "end" : a & pb ? "move" : a & ob ? "start" : ""
    }

    function $(a) {
        return a == Ma ? "down" : a == La ? "up" : a == Ja ? "left" : a == Ka ? "right" : ""
    }

    function _(a, b) {
        var c = b.manager;
        return c ? c.get(a) : a
    }

    function aa() {
        Y.apply(this, arguments)
    }

    function ba() {
        aa.apply(this, arguments), this.pX = null, this.pY = null
    }

    function ca() {
        aa.apply(this, arguments)
    }

    function da() {
        Y.apply(this, arguments), this._timer = null, this._input = null
    }

    function ea() {
        aa.apply(this, arguments)
    }

    function fa() {
        aa.apply(this, arguments)
    }

    function ga() {
        Y.apply(this, arguments), this.pTime = !1, this.pCenter = !1, this._timer = null, this._input = null, this.count = 0
    }

    function ha(a, b) {
        return b = b || {}, b.recognizers = l(b.recognizers, ha.defaults.preset), new ia(a, b)
    }

    function ia(a, b) {
        this.options = la({}, ha.defaults, b || {}), this.options.inputTarget = this.options.inputTarget || a, this.handlers = {}, this.session = {}, this.recognizers = [], this.oldCssProps = {}, this.element = a, this.input = y(this), this.touchAction = new V(this, this.options.touchAction), ja(this, !0), g(this.options.recognizers, function(a) {
            var b = this.add(new a[0](a[1]));
            a[2] && b.recognizeWith(a[2]), a[3] && b.requireFailure(a[3])
        }, this)
    }

    function ja(a, b) {
        var c = a.element;
        if (c.style) {
            var d;
            g(a.options.cssProps, function(e, f) {
                d = u(c.style, f), b ? (a.oldCssProps[d] = c.style[d], c.style[d] = e) : c.style[d] = a.oldCssProps[d] || ""
            }), b || (a.oldCssProps = {})
        }
    }

    function ka(a, c) {
        var d = b.createEvent("Event");
        d.initEvent(a, !0, !0), d.gesture = c, c.target.dispatchEvent(d)
    }
    var la, ma = ["", "webkit", "Moz", "MS", "ms", "o"],
        na = b.createElement("div"),
        oa = "function",
        pa = Math.round,
        qa = Math.abs,
        ra = Date.now;
    la = "function" != typeof Object.assign ? function(a) {
        if (a === d || null === a) throw new TypeError("Cannot convert undefined or null to object");
        for (var b = Object(a), c = 1; c < arguments.length; c++) {
            var e = arguments[c];
            if (e !== d && null !== e)
                for (var f in e) e.hasOwnProperty(f) && (b[f] = e[f])
        }
        return b
    } : Object.assign;
    var sa = h(function(a, b, c) {
            for (var e = Object.keys(b), f = 0; f < e.length;)(!c || c && a[e[f]] === d) && (a[e[f]] = b[e[f]]), f++;
            return a
        }, "extend", "Use `assign`."),
        ta = h(function(a, b) {
            return sa(a, b, !0)
        }, "merge", "Use `assign`."),
        ua = 1,
        va = /mobile|tablet|ip(ad|hone|od)|android/i,
        wa = "ontouchstart" in a,
        xa = u(a, "PointerEvent") !== d,
        ya = wa && va.test(navigator.userAgent),
        za = "touch",
        Aa = "pen",
        Ba = "mouse",
        Ca = "kinect",
        Da = 25,
        Ea = 1,
        Fa = 2,
        Ga = 4,
        Ha = 8,
        Ia = 1,
        Ja = 2,
        Ka = 4,
        La = 8,
        Ma = 16,
        Na = Ja | Ka,
        Oa = La | Ma,
        Pa = Na | Oa,
        Qa = ["x", "y"],
        Ra = ["clientX", "clientY"];
    x.prototype = {
        handler: function() {},
        init: function() {
            this.evEl && m(this.element, this.evEl, this.domHandler), this.evTarget && m(this.target, this.evTarget, this.domHandler), this.evWin && m(w(this.element), this.evWin, this.domHandler)
        },
        destroy: function() {
            this.evEl && n(this.element, this.evEl, this.domHandler), this.evTarget && n(this.target, this.evTarget, this.domHandler), this.evWin && n(w(this.element), this.evWin, this.domHandler)
        }
    };
    var Sa = {
            mousedown: Ea,
            mousemove: Fa,
            mouseup: Ga
        },
        Ta = "mousedown",
        Ua = "mousemove mouseup";
    i(L, x, {
        handler: function(a) {
            var b = Sa[a.type];
            b & Ea && 0 === a.button && (this.pressed = !0), b & Fa && 1 !== a.which && (b = Ga), this.pressed && (b & Ga && (this.pressed = !1), this.callback(this.manager, b, {
                pointers: [a],
                changedPointers: [a],
                pointerType: Ba,
                srcEvent: a
            }))
        }
    });
    var Va = {
            pointerdown: Ea,
            pointermove: Fa,
            pointerup: Ga,
            pointercancel: Ha,
            pointerout: Ha
        },
        Wa = {
            2: za,
            3: Aa,
            4: Ba,
            5: Ca
        },
        Xa = "pointerdown",
        Ya = "pointermove pointerup pointercancel";
    a.MSPointerEvent && !a.PointerEvent && (Xa = "MSPointerDown", Ya = "MSPointerMove MSPointerUp MSPointerCancel"), i(M, x, {
        handler: function(a) {
            var b = this.store,
                c = !1,
                d = a.type.toLowerCase().replace("ms", ""),
                e = Va[d],
                f = Wa[a.pointerType] || a.pointerType,
                g = f == za,
                h = r(b, a.pointerId, "pointerId");
            e & Ea && (0 === a.button || g) ? h < 0 && (b.push(a), h = b.length - 1) : e & (Ga | Ha) && (c = !0), h < 0 || (b[h] = a, this.callback(this.manager, e, {
                pointers: b,
                changedPointers: [a],
                pointerType: f,
                srcEvent: a
            }), c && b.splice(h, 1))
        }
    });
    var Za = {
            touchstart: Ea,
            touchmove: Fa,
            touchend: Ga,
            touchcancel: Ha
        },
        $a = "touchstart",
        _a = "touchstart touchmove touchend touchcancel";
    i(N, x, {
        handler: function(a) {
            var b = Za[a.type];
            if (b === Ea && (this.started = !0), this.started) {
                var c = O.call(this, a, b);
                b & (Ga | Ha) && c[0].length - c[1].length == 0 && (this.started = !1), this.callback(this.manager, b, {
                    pointers: c[0],
                    changedPointers: c[1],
                    pointerType: za,
                    srcEvent: a
                })
            }
        }
    });
    var ab = {
            touchstart: Ea,
            touchmove: Fa,
            touchend: Ga,
            touchcancel: Ha
        },
        bb = "touchstart touchmove touchend touchcancel";
    i(P, x, {
        handler: function(a) {
            var b = ab[a.type],
                c = Q.call(this, a, b);
            c && this.callback(this.manager, b, {
                pointers: c[0],
                changedPointers: c[1],
                pointerType: za,
                srcEvent: a
            })
        }
    });
    var cb = 2500,
        db = 25;
    i(R, x, {
        handler: function(a, b, c) {
            var d = c.pointerType == za,
                e = c.pointerType == Ba;
            if (!(e && c.sourceCapabilities && c.sourceCapabilities.firesTouchEvents)) {
                if (d) S.call(this, b, c);
                else if (e && U.call(this, c)) return;
                this.callback(a, b, c)
            }
        },
        destroy: function() {
            this.touch.destroy(), this.mouse.destroy()
        }
    });
    var eb = u(na.style, "touchAction"),
        fb = eb !== d,
        gb = "compute",
        hb = "auto",
        ib = "manipulation",
        jb = "none",
        kb = "pan-x",
        lb = "pan-y",
        mb = X();
    V.prototype = {
        set: function(a) {
            a == gb && (a = this.compute()), fb && this.manager.element.style && mb[a] && (this.manager.element.style[eb] = a), this.actions = a.toLowerCase().trim()
        },
        update: function() {
            this.set(this.manager.options.touchAction)
        },
        compute: function() {
            var a = [];
            return g(this.manager.recognizers, function(b) {
                k(b.options.enable, [b]) && (a = a.concat(b.getTouchAction()))
            }), W(a.join(" "))
        },
        preventDefaults: function(a) {
            var b = a.srcEvent,
                c = a.offsetDirection;
            if (this.manager.session.prevented) return void b.preventDefault();
            var d = this.actions,
                e = p(d, jb) && !mb[jb],
                f = p(d, lb) && !mb[lb],
                g = p(d, kb) && !mb[kb];
            if (e) {
                var h = 1 === a.pointers.length,
                    i = a.distance < 2,
                    j = a.deltaTime < 250;
                if (h && i && j) return
            }
            return g && f ? void 0 : e || f && c & Na || g && c & Oa ? this.preventSrc(b) : void 0
        },
        preventSrc: function(a) {
            this.manager.session.prevented = !0, a.preventDefault()
        }
    };
    var nb = 1,
        ob = 2,
        pb = 4,
        qb = 8,
        rb = qb,
        sb = 16,
        tb = 32;
    Y.prototype = {
        defaults: {},
        set: function(a) {
            return la(this.options, a), this.manager && this.manager.touchAction.update(), this
        },
        recognizeWith: function(a) {
            if (f(a, "recognizeWith", this)) return this;
            var b = this.simultaneous;
            return a = _(a, this), b[a.id] || (b[a.id] = a, a.recognizeWith(this)), this
        },
        dropRecognizeWith: function(a) {
            return f(a, "dropRecognizeWith", this) ? this : (a = _(a, this), delete this.simultaneous[a.id], this)
        },
        requireFailure: function(a) {
            if (f(a, "requireFailure", this)) return this;
            var b = this.requireFail;
            return a = _(a, this), -1 === r(b, a) && (b.push(a), a.requireFailure(this)), this
        },
        dropRequireFailure: function(a) {
            if (f(a, "dropRequireFailure", this)) return this;
            a = _(a, this);
            var b = r(this.requireFail, a);
            return b > -1 && this.requireFail.splice(b, 1), this
        },
        hasRequireFailures: function() {
            return this.requireFail.length > 0
        },
        canRecognizeWith: function(a) {
            return !!this.simultaneous[a.id]
        },
        emit: function(a) {
            function b(b) {
                c.manager.emit(b, a)
            }
            var c = this,
                d = this.state;
            d < qb && b(c.options.event + Z(d)), b(c.options.event), a.additionalEvent && b(a.additionalEvent), d >= qb && b(c.options.event + Z(d))
        },
        tryEmit: function(a) {
            if (this.canEmit()) return this.emit(a);
            this.state = tb
        },
        canEmit: function() {
            for (var a = 0; a < this.requireFail.length;) {
                if (!(this.requireFail[a].state & (tb | nb))) return !1;
                a++
            }
            return !0
        },
        recognize: function(a) {
            var b = la({}, a);
            if (!k(this.options.enable, [this, b])) return this.reset(), void(this.state = tb);
            this.state & (rb | sb | tb) && (this.state = nb), this.state = this.process(b), this.state & (ob | pb | qb | sb) && this.tryEmit(b)
        },
        process: function(a) {},
        getTouchAction: function() {},
        reset: function() {}
    }, i(aa, Y, {
        defaults: {
            pointers: 1
        },
        attrTest: function(a) {
            var b = this.options.pointers;
            return 0 === b || a.pointers.length === b
        },
        process: function(a) {
            var b = this.state,
                c = a.eventType,
                d = b & (ob | pb),
                e = this.attrTest(a);
            return d && (c & Ha || !e) ? b | sb : d || e ? c & Ga ? b | qb : b & ob ? b | pb : ob : tb
        }
    }), i(ba, aa, {
        defaults: {
            event: "pan",
            threshold: 10,
            pointers: 1,
            direction: Pa
        },
        getTouchAction: function() {
            var a = this.options.direction,
                b = [];
            return a & Na && b.push(lb), a & Oa && b.push(kb), b
        },
        directionTest: function(a) {
            var b = this.options,
                c = !0,
                d = a.distance,
                e = a.direction,
                f = a.deltaX,
                g = a.deltaY;
            return e & b.direction || (b.direction & Na ? (e = 0 === f ? Ia : f < 0 ? Ja : Ka, c = f != this.pX, d = Math.abs(a.deltaX)) : (e = 0 === g ? Ia : g < 0 ? La : Ma, c = g != this.pY, d = Math.abs(a.deltaY))), a.direction = e, c && d > b.threshold && e & b.direction
        },
        attrTest: function(a) {
            return aa.prototype.attrTest.call(this, a) && (this.state & ob || !(this.state & ob) && this.directionTest(a))
        },
        emit: function(a) {
            this.pX = a.deltaX, this.pY = a.deltaY;
            var b = $(a.direction);
            b && (a.additionalEvent = this.options.event + b), this._super.emit.call(this, a)
        }
    }), i(ca, aa, {
        defaults: {
            event: "pinch",
            threshold: 0,
            pointers: 2
        },
        getTouchAction: function() {
            return [jb]
        },
        attrTest: function(a) {
            return this._super.attrTest.call(this, a) && (Math.abs(a.scale - 1) > this.options.threshold || this.state & ob)
        },
        emit: function(a) {
            if (1 !== a.scale) {
                var b = a.scale < 1 ? "in" : "out";
                a.additionalEvent = this.options.event + b
            }
            this._super.emit.call(this, a)
        }
    }), i(da, Y, {
        defaults: {
            event: "press",
            pointers: 1,
            time: 251,
            threshold: 9
        },
        getTouchAction: function() {
            return [hb]
        },
        process: function(a) {
            var b = this.options,
                c = a.pointers.length === b.pointers,
                d = a.distance < b.threshold,
                f = a.deltaTime > b.time;
            if (this._input = a, !d || !c || a.eventType & (Ga | Ha) && !f) this.reset();
            else if (a.eventType & Ea) this.reset(), this._timer = e(function() {
                this.state = rb, this.tryEmit()
            }, b.time, this);
            else if (a.eventType & Ga) return rb;
            return tb
        },
        reset: function() {
            clearTimeout(this._timer)
        },
        emit: function(a) {
            this.state === rb && (a && a.eventType & Ga ? this.manager.emit(this.options.event + "up", a) : (this._input.timeStamp = ra(), this.manager.emit(this.options.event, this._input)))
        }
    }), i(ea, aa, {
        defaults: {
            event: "rotate",
            threshold: 0,
            pointers: 2
        },
        getTouchAction: function() {
            return [jb]
        },
        attrTest: function(a) {
            return this._super.attrTest.call(this, a) && (Math.abs(a.rotation) > this.options.threshold || this.state & ob)
        }
    }), i(fa, aa, {
        defaults: {
            event: "swipe",
            threshold: 10,
            velocity: .3,
            direction: Na | Oa,
            pointers: 1
        },
        getTouchAction: function() {
            return ba.prototype.getTouchAction.call(this)
        },
        attrTest: function(a) {
            var b, c = this.options.direction;
            return c & (Na | Oa) ? b = a.overallVelocity : c & Na ? b = a.overallVelocityX : c & Oa && (b = a.overallVelocityY), this._super.attrTest.call(this, a) && c & a.offsetDirection && a.distance > this.options.threshold && a.maxPointers == this.options.pointers && qa(b) > this.options.velocity && a.eventType & Ga
        },
        emit: function(a) {
            var b = $(a.offsetDirection);
            b && this.manager.emit(this.options.event + b, a), this.manager.emit(this.options.event, a)
        }
    }), i(ga, Y, {
        defaults: {
            event: "tap",
            pointers: 1,
            taps: 1,
            interval: 300,
            time: 250,
            threshold: 9,
            posThreshold: 10
        },
        getTouchAction: function() {
            return [ib]
        },
        process: function(a) {
            var b = this.options,
                c = a.pointers.length === b.pointers,
                d = a.distance < b.threshold,
                f = a.deltaTime < b.time;
            if (this.reset(), a.eventType & Ea && 0 === this.count) return this.failTimeout();
            if (d && f && c) {
                if (a.eventType != Ga) return this.failTimeout();
                var g = !this.pTime || a.timeStamp - this.pTime < b.interval,
                    h = !this.pCenter || H(this.pCenter, a.center) < b.posThreshold;
                this.pTime = a.timeStamp, this.pCenter = a.center, h && g ? this.count += 1 : this.count = 1, this._input = a;
                if (0 === this.count % b.taps) return this.hasRequireFailures() ? (this._timer = e(function() {
                    this.state = rb, this.tryEmit()
                }, b.interval, this), ob) : rb
            }
            return tb
        },
        failTimeout: function() {
            return this._timer = e(function() {
                this.state = tb
            }, this.options.interval, this), tb
        },
        reset: function() {
            clearTimeout(this._timer)
        },
        emit: function() {
            this.state == rb && (this._input.tapCount = this.count, this.manager.emit(this.options.event, this._input))
        }
    }), ha.VERSION = "2.0.8", ha.defaults = {
        domEvents: !1,
        touchAction: gb,
        enable: !0,
        inputTarget: null,
        inputClass: null,
        preset: [
            [ea, {
                enable: !1
            }],
            [ca, {
                    enable: !1
                },
                ["rotate"]
            ],
            [fa, {
                direction: Na
            }],
            [ba, {
                    direction: Na
                },
                ["swipe"]
            ],
            [ga],
            [ga, {
                    event: "doubletap",
                    taps: 2
                },
                ["tap"]
            ],
            [da]
        ],
        cssProps: {
            userSelect: "none",
            touchSelect: "none",
            touchCallout: "none",
            contentZooming: "none",
            userDrag: "none",
            tapHighlightColor: "rgba(0,0,0,0)"
        }
    };
    var ub = 2;
    ia.prototype = {
        set: function(a) {
            return la(this.options, a), a.touchAction && this.touchAction.update(), a.inputTarget && (this.input.destroy(), this.input.target = a.inputTarget, this.input.init()), this
        },
        stop: function(a) {
            this.session.stopped = a ? ub : 1
        },
        recognize: function(a) {
            var b = this.session;
            if (!b.stopped) {
                this.touchAction.preventDefaults(a);
                var c, d = this.recognizers,
                    e = b.curRecognizer;
                (!e || e && e.state & rb) && (e = b.curRecognizer = null);
                for (var f = 0; f < d.length;) c = d[f], b.stopped === ub || e && c != e && !c.canRecognizeWith(e) ? c.reset() : c.recognize(a), !e && c.state & (ob | pb | qb) && (e = b.curRecognizer = c), f++
            }
        },
        get: function(a) {
            if (a instanceof Y) return a;
            for (var b = this.recognizers, c = 0; c < b.length; c++)
                if (b[c].options.event == a) return b[c];
            return null
        },
        add: function(a) {
            if (f(a, "add", this)) return this;
            var b = this.get(a.options.event);
            return b && this.remove(b), this.recognizers.push(a), a.manager = this, this.touchAction.update(), a
        },
        remove: function(a) {
            if (f(a, "remove", this)) return this;
            if (a = this.get(a)) {
                var b = this.recognizers,
                    c = r(b, a); - 1 !== c && (b.splice(c, 1), this.touchAction.update())
            }
            return this
        },
        on: function(a, b) {
            if (a !== d && b !== d) {
                var c = this.handlers;
                return g(q(a), function(a) {
                    c[a] = c[a] || [], c[a].push(b)
                }), this
            }
        },
        off: function(a, b) {
            if (a !== d) {
                var c = this.handlers;
                return g(q(a), function(a) {
                    b ? c[a] && c[a].splice(r(c[a], b), 1) : delete c[a]
                }), this
            }
        },
        emit: function(a, b) {
            this.options.domEvents && ka(a, b);
            var c = this.handlers[a] && this.handlers[a].slice();
            if (c && c.length) {
                b.type = a, b.preventDefault = function() {
                    b.srcEvent.preventDefault()
                };
                for (var d = 0; d < c.length;) c[d](b), d++
            }
        },
        destroy: function() {
            this.element && ja(this, !1), this.handlers = {}, this.session = {}, this.input.destroy(), this.element = null
        }
    }, la(ha, {
        INPUT_START: Ea,
        INPUT_MOVE: Fa,
        INPUT_END: Ga,
        INPUT_CANCEL: Ha,
        STATE_POSSIBLE: nb,
        STATE_BEGAN: ob,
        STATE_CHANGED: pb,
        STATE_ENDED: qb,
        STATE_RECOGNIZED: rb,
        STATE_CANCELLED: sb,
        STATE_FAILED: tb,
        DIRECTION_NONE: Ia,
        DIRECTION_LEFT: Ja,
        DIRECTION_RIGHT: Ka,
        DIRECTION_UP: La,
        DIRECTION_DOWN: Ma,
        DIRECTION_HORIZONTAL: Na,
        DIRECTION_VERTICAL: Oa,
        DIRECTION_ALL: Pa,
        Manager: ia,
        Input: x,
        TouchAction: V,
        TouchInput: P,
        MouseInput: L,
        PointerEventInput: M,
        TouchMouseInput: R,
        SingleTouchInput: N,
        Recognizer: Y,
        AttrRecognizer: aa,
        Tap: ga,
        Pan: ba,
        Swipe: fa,
        Pinch: ca,
        Rotate: ea,
        Press: da,
        on: m,
        off: n,
        each: g,
        merge: ta,
        extend: sa,
        assign: la,
        inherit: i,
        bindFn: j,
        prefixed: u
    }), (void 0 !== a ? a : "undefined" != typeof self ? self : {}).Hammer = ha, "function" == typeof define && define.amd ? define(function() {
        return ha
    }) : "undefined" != typeof module && module.exports ? module.exports = ha : a[c] = ha
}(window, document, "Hammer");