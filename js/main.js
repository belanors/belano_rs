function preloader() {
    $('#preloader').fadeOut(500);

    // if preloader dont get hidden for 10s > hide it
    setTimeout(function(){
        $('#preloader').hide();
    }, 5000 );
}

$("#calendar-checkin-input").caleran({
    startEmpty: true,
    inline: true,
    singleDate: true,
    startOnMonday: true,
    calendarCount: 1,
    showHeader: false,
    showFooter: false,
    dateUpdateContainer: "qs-label-checkin",
    enableKeybard: false,
    enableMonthSwitcher: true,
});

$("#calendar-checkout-input").caleran({
    inline: true,
    startEmpty: true,
    startDate: null,
    singleDate: true,
    startOnMonday: true,
    calendarCount: 1,
    showHeader: false,
    showFooter: false,
    startEmpty: true,
    dateUpdateContainer: "qs-label-checkout",
    enableKeybard: false,
    enableMonthSwitcher: true,
    container: ".calendar-ui",
    startDate: ""
});

<!-- Search autocomplete DB -->
var streetDB = ['Belgrade - Palilula - Bulevar Kralja Aleksandra 18 - Apartman - Belano', 'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
    'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
    'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
    'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
    'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
    'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
    'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
    'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
    'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
];

$(document).ready(function(){

    preloader();

    $('#apartments-nav-slider').slick({
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        nextArrow: '<button type="button" class="slick-next btn-floating"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
        prevArrow: '<button type="button" class="slick-prev btn-floating"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: false
                }
            }
        ]
    });

    $(".navbar-toggler").click(function() {
        $(this).toggleClass("open")
        // event.stopPropagation();
        // $("#hamburger-menu").toggleClass("open");
    });

    // Main slider
    $('#main-slider-holder').slick({
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true
        // nextArrow: '<div class="slider-gradient-right"><button type="button" class="slick-next minimal-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button></div>',
        // prevArrow: '<button type="button" class="slider-gradient-left slick-prev minimal-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
    });

    $('#main-slider-prev').click(function(e){
        e.preventDefault();
        $('#main-slider-holder').slick('slickPrev');
    });

    $('#main-slider-next').click(function(e){
        e.preventDefault();
        $('#main-slider-holder').slick('slickNext');
    });

    // Show/Hide Password
    $(".toggle-password").click(function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
    
    $.fn.selectpicker.Constructor.BootstrapVersion = '4';
    $('.custom-select').selectpicker({
        dropupAuto: false
    });

    // Search auto-complete
    var substringMatcher = function(strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };

    $('#search-input').typeahead({
        classNames: {
            wrapper: 'search-autocomplete-wrapper'
        },
            hint: false,
            highlight: true,
            minLength: 3
        },
        {
            name: 'streets',
            source: substringMatcher(streetDB)
        });

    $('#benefits-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        nextArrow: '<button type="button" class="slick-next btn-floating"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
        prevArrow: '<button type="button" class="slick-prev btn-floating"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
          responsive: [
              {
                  breakpoint: 1024,
                  settings: {
                      slidesToShow: 3,
                      slidesToScroll: 1
                  }
              },
              {
                  breakpoint: 600,
                  settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      dots: true,
                      arrows: false
                  }
              },
              {
                  breakpoint: 480,
                  settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      dots: true,
                      arrows: false
                  }
              }
          ]
    });

    $('#blog-post-slider').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        nextArrow: '<div class="slider-gradient-right"><button type="button" class="slick-next minimal-arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></button></div>',
        prevArrow: '<div class="slider-gradient-left"><button type="button" class="slider-gradient-left slick-prev minimal-arrow"><i class="fa fa-angle-left" aria-hidden="true"></i></button></div>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: false
                }
            }
        ]

    });
    
    var $sliders = $(".apart-slider");
    var $arrows = $(".slick-controls");

    $(".apart-image.with-slider").each(function(){

        var $this = $(this);
        var slick = $this.find( $sliders ).slick({
            dots:           true,
            speed:          300,
            fade:           false,
            infinite:       false,
            arrows:         true,
            dots:           true,
            slidesToShow:   1,
            mobileFirst:    true,
            appendArrows:   $this.find( $arrows ),
            appendDots:     $this.find( $arrows ),
            nextArrow: "<div class='slick-next'><i class='fa fa-angle-right' aria-hidden='true'></i></div>",
            prevArrow: "<div class='slick-prev'><i class='fa fa-angle-left' aria-hidden='true'></i></div>"
        });
    });

      var date = new Date();
      var dateNow = date.getFullYear() + "/" + (date.getMonth()+1) + "/" + date.getDate();
      console.log(dateNow);


	// Quick-search form
 	$('.qs-btn').click(function(e) {
 		e.preventDefault();

 		$('#quick-search').toggleClass('active');

 		// check if overlay is already active
 		// if( $('#overlay').hasClass('active') == false ) {
 			$('#overlay').toggleClass('active');
 		// }
 		
 		// TODO: detect if clicked outside search form > close overlay

 		$('.qs-expanded-block:visible').addClass('d-none'); // hide all blocks

 		var searchBlock = $(this).data('toggle');
 		$('#' + searchBlock).removeClass('d-none'); // show only clicked

    });

  	$('#btn-qs-search').click(function(){
  		$('#search-overlay').addClass('d-none'); // dev only
  		$('.qs-expanded-block').addClass('d-none');
  	});

    $('#btn-qs-search').click(function(){
        $('#overlay').removeClass('active');
        alert('search form submited');
    });

  	// Activate search form
  	$('#search-trigger, #search-trigger-mobile').click(function(){
  		$('body').toggleClass('search-active');
        $('#search-input').focus();
  	});

    // Load more apartments
    $('.btn-load-more').click(function(e){
        e.preventDefault();
        alert('load more items');
    });

    // Apartman grid tabs
    $('#apart-tabs .nav-link').click(function(){
        $('#apart-tabs .nav-link').removeClass('active');
        $(this).addClass('active');

        console.log('load apartments');
    });

    // close button - hide container
    $(document).on('click', '.btn-close', function() {
        var container = $(this).data('container');
        $(container).removeClass('show');
    });

    // open button - show container
    $(document).on('click', '.btn-open', function() {
        var container = $(this).data('container');
        $(container).addClass('show');
        $(container).find('.show').removeClass('show');
    });

    // Like apartment
    $('.btn-add-wishlist').click(function(e){

        e.preventDefault();
        var apartID = $(this).data('id');
        $(this).toggleClass('active');

        alert('Liked apartment ID: ' + apartID);

    });

    // Table row + column highlight
    var tableCells = $(".table-highlight td, .table-highlight th");
    var tableHover = $(".table-highlight");
    tableCells.on("mouseover", function() {
        tableHover.addClass('table-hovered');
            var el = $(this),
                pos = el.index();
            el.parent().find("th, td").addClass("cell-hover");
            tableCells.filter(":nth-child(" + (pos+1) + ")").addClass("cell-hover");
        })
        .on("mouseout", function() {
            tableHover.removeClass('table-hovered');
            tableCells.removeClass("cell-hover");
        });

    $('form.ajaxForm').submit(function(e){
        e.preventDefault();

        var formID = $(this).attr('id');
        var formAction = $(this).attr('action');
        var formMethod = $(this).attr('method');
        var formURL = $(this).data('url');
        var formData = $(this).serialize();
        var formDataArray = $(this).serializeArray();

        $.ajax({
            type: formMethod,
            url: formURL,
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function() {
                console.info('Form submit - ' + formID);
                console.log( formDataArray );
            },
            error: function(jqXHR, textStatus, errorMessage) {
                console.log(errorMessage); // Optional
                console.error('Form Error - (' + formID + '): ' + errorMessage);
            },
            success: function(data) {
                console.info('Form success - (' + formID + '): ' + data);
            }
        });


    })

});

function sticky(container, offset, element) {
    $(container).scroll(function(){
        if ($(container).scrollTop() >= offset) {
            $(element).addClass('sticky');
        }
        else {
            $(element).removeClass('sticky');
        }
    });
}

//DASHBOARD

$('.menu-trigger .menu-open').click(function (e) {
    e.preventDefault();
    $('#dash-sidebar').addClass('is-open');
});
$('.menu-trigger .menu-close').click(function (e) {
    e.preventDefault();
    $('#dash-sidebar').removeClass('is-open');
});