    <footer id="footer">
        <div id="sticky-stopper"></div>
    	<div class="container-fluid">
    		<div class="row justify-content-between">

    			<div class="col-12 col-md-3 text-md-left text-center" id="footer-left">
                    
                    <a href="#" class="footer-logo">
                        <img src="img/logo-header-light.png" alt="">
                    </a>

                    <div class="small">
                        <div class="nav-lang mb-3" class="mb-3"><i class="fa fa-globe mr-2"> </i>Eng / Srb</div>
                        <div class="nav-weather"><img src="img/icon-weather.png" alt=""> &nbsp; Belgrade 34F</div>
                    </div>
    				
                    <div class="social-links">
                        <a href="instagram.com"><i class="fab fa-instagram"></i></a>
                        <a href="facebook.com"><i class="fab fa-facebook"></i></a>
                        <a href="pinterest"><i class="fab fa-pinterest"></i></a>
                        <a href="linkedin"><i class="fab fa-linkedin"></i></a>
                        <a href="email"><i class="fa fa-envelope"></i></a>
                    </div>

    			</div>

    			<div class="col-12 col-md-9" id="footer-right">
    				<div class="row justify-content-between">
    					<ul class="footer-links">
        					<li><a href="#">Blog</a></li>
        					<li><a href="#">Apartmani</a></li>
        					<li><a href="#">Kontakt</a></li>
        					<li><a href="#">Korisne informacije</a></li>
        					<li><a href="#">Lojalti</a></li>
	    				</ul>

	    				<ul class="footer-links">
                            <li class="title">Lux apartments</li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
	    				</ul>

                        <ul class="footer-links">
                            <li class="title">Lux apartments</li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                        </ul>
                        
                        <ul class="footer-links">
                            <li class="title">Lux apartments</li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                            <li><a href="">Apartmans for day</a></li>
                        </ul>

    				</div>
    			</div>
    		</div>
    	</div>
    </footer>
    <section id="subfooter" class="p-4">
        <div class="d-flex justify-content-between align-items-center">
            <div class="col text-left">© Copyright</div>
            <div class="col text-right d-flex align-items-center justify-content-end">Power by <img src="img/logo-dimis.png" alt="Dimis" class="ml-2"></div>
        </div>
    </section>

    <?php include('bottom-includes.php'); ?>